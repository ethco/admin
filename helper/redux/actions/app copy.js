import AuthStorage from "../../utils/auth-storage";  

import { SINGLE_API /* , REQUEST_ERROR */ } from "./types";

export const actionDeletePartner = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
        "Content-Type": "multipart/form-data",
      })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};

export const actionUpdatePassword = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
        "Content-Type": "multipart/form-data",
      })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/users/update_password/${payload.data.user}`,
      options: {
        method: "PUT",
        headers,
      },
      payload: payload.data.values,
      next,
    },
  };
};

export const actionUpdateStatus = async (payload = {}, next = (f) => f) => {
  let headers = {};

  payload.type == "multipart"
    ? (headers = {
        "Content-Type": "multipart/form-data",
      })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${payload.payload.id}/status`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const actionUpdateRoleStatus = async (payload = {}, next = (f) => f) => {
  let headers = {};

  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
        "Content-Type": "multipart/form-data",
      })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/managers/promote`,
      options: {
        method: "PUT",
        headers,
      },
      payload: payload.payload,
      next,
    },
  };
};

export const actionRemovePrivilege = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload.payload);

  payload.type == "multipart"
    ? (headers = {
        "Content-Type": "multipart/form-data",
      })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/managers/remove`,
      options: {
        method: "PUT",
        headers,
      },
      payload: payload.payload,
      next,
    },
  };
};

export const actionAddLocation = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
        "Content-Type": "multipart/form-data",
      })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${payload.data.partner}/store`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.data.payload,
      next,
    },
  };
};

export const actionAddPartner = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
        "Content-Type": "multipart/form-data",
      })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: "admin/partners",
      options: {
        method: "POST",
        headers,
      },
      payload: payload.data,
      next,
    },
  };
};

export const getAllManagers = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/managers",
      next,
    },
  };
};

export const getAllPartners = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/partners",
      next,
    },
  };
};

export const getAllPartnerByName = async (name, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${name}`,
      next,
    },
  };
};

export const getPendingPartners = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/partners/pending",
      next,
    },
  };
};

export const getDeclinedPartners = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/partners/declined",
      next,
    },
  };
};

export const getUsers = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/users",
      next,
    },
  };
};


// COME BACK TO THIS

export const getAllOrders = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/orders",
      url: "admin/orders",
      next,
    },
  };
};
export const getAllOrdersById = async (id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${id}`,
      next,
    },
  };
};
export const actionUpdateOrder = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
        "Content-Type": "multipart/form-data",
      })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${payload.payload.id}/${payload.data.order}`,
      options: {
        method: "PUT",
        headers,
      },
      payload: payload.data.values,
      next,
    },
  };
};
export const actionDeleteOrder = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
        "Content-Type": "multipart/form-data",
      })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};