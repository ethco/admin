import AuthStorage from "../../utils/auth-storage";

import { SINGLE_API /* , REQUEST_ERROR */ } from "./types";

export const actionDeletePartner = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};

export const actionUpdatePassword = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/users/update_password/${payload.data.user}`,
      options: {
        method: "PUT",
        headers,
      },
      payload: payload.data.values,
      next,
    },
  };
};

export const actionUpdateStatus = async (payload = {}, next = (f) => f) => {
  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${payload.payload.id}/status`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const actionUpdateRoleStatus = async (payload = {}, next = (f) => f) => {
  let headers = {};

  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/managers/promote`,
      options: {
        method: "PUT",
        headers,
      },
      payload: payload.payload,
      next,
    },
  };
};

export const actionRemovePrivilege = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload.payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/managers/remove`,
      options: {
        method: "PUT",
        headers,
      },
      payload: payload.payload,
      next,
    },
  };
};

export const actionAddLocation = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${payload.data.partner}/store`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.data.payload,
      next,
    },
  };
};

export const getAllManagers = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/managers",
      next,
    },
  };
};

export const getUsers = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/users",
      next,
    },
  };
};




// ADDRESS
export const addAddress = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: "addresses",
      options: {
        method: "POST",
        headers,
      },
      payload: payload.data,
      next,
    },
  };
};

export const getAddresses = async (postcode) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `addresses/${postcode}`,
      next,
    },
  };
}

export const getAddress = async (postcode) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `addresses/${postcode}/expanded`,
      next,
    },
  };
}


// PARTNERS

export const actionAddPartner = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: "admin/partners",
      options: {
        method: "POST",
        headers,
      },
      payload: payload.data,
      next,
    },
  };
};

export const getAllPartners = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/partners",
      next,
    },
  };
};

export const getAllPartnerByName = async (partner_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${partner_id}`,
      next,
    },
  };
};

export const getPendingPartners = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/partners/pending",
      next,
    },
  };
};

export const getDeclinedPartners = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/partners/declined",
      next,
    },
  };
};

export const actionUpdatePartner = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${payload.payload.id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Update store Data 
export const updatePartnerLogo = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/partners/${payload.payload.id}/logo`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};


// STORE

// List of all stores 
export const getAllStores = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: "admin/stores",
      next,
    },
  };
};

export const addPartnerStore = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/stores/${payload.payload.partner_id}`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// list of stores under a partner
export const getAllPartnerStores = async (partner_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      // url: `admin/partners/${name}/${store_id}`,
      url: `admin/store/${partner_id}`,
      next,
    },
  };
};

// Get a single store Data
export const getAllPartnerStoreByName = async (store_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/stores/${store_id}/store`,
      next,
    },
  };
};

// Get a single store Orders
export const getStoreOrders = async (store_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/store/${store_id}`,
      next,
    },
  };
};

export const UpdateHours = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/stores/${payload.payload.store_id}/opening_hours`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Update store Data 
export const actionUpdateStore = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/stores/${payload.payload.store_id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Update store address
export const updateStoreAddress = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      // Get payload.payload.id data from function payload
      url: `admin/stores/${payload.payload.store_id}/address`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Update store address
export const storeConfiguration = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/stores/${payload.payload.store_id}/configuration`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Delete Store
export const deleteStore = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/stores/${payload.payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};



// ORDERS

// Get all Orders
export const getAllOrders = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: 'admin/orders',
      next,
    },
  };
};

// Get individual Orders list
export const getAllOrdersByName = async (order_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${order_id}`,
      next,
    },
  };
};

// Get product info under an order
export const getProductByName = async (order_item_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/item/${order_item_id}`,
      next,
    },
  };
};

// Push order to another store
export const pushToStore = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${payload.payload.order_id}/push`,
      options: {
        method: "PUT",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Cancel an order item / product in a order This will be ib the Product page
export const actionCancelOrder = async (payload = {}, next = (f) => f) => {
  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${payload.payload.order_item_id}/status`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Refund payment
export const actionRefundAmount = async (payload = {}, next = (f) => f) => {
  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${payload.payload.order_id}/payment/status`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Update order delivery adddress
export const updateDeliveryAddress = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${payload.payload.order_id}/delivery-detail`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Update orders status
export const updateOrderStat = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${payload.payload.order_id}/status`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const updateOrdersStatus = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/${payload.payload.order_id}/multiple/status`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Get order Status
export const getOrderStatus = async (status, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/status/${status}`,
      next,
    },
  };
};

// Track order 
export const trackOrder = async (tracker, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/orders/track/${tracker}`,
      next,
    },
  };
};



// CUSTOMER
export const getAllCustomers = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: 'admin/customers',
      next,
    },
  };
};

// Get a single customer
export const getCustomerData = async (customer_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/customers/${customer_id}`,
      next,
    },
  };
};

// Get a single customer
export const getCustomerAddress = async (customer_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/customers/address/${customer_id}`,
      next,
    },
  };
};

// Update order customer adddress
export const updateCustomerAddress = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/customers/address/${payload.payload.address_id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};
// Update Customer Data 
export const actionUpdateCustomer = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/customers/${payload.payload.user_id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Delete Customer 
export const deleteCustomer = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/customers/${payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};


// PRODUCTS

export const AddProductToStore = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/products/${payload.payload.store_id}/store`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const AddMultipleProducts = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/products/${payload.payload.store_id}/multiple`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const getProducts = async (store_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/products/${store_id}/store?type=published`,
      next,
    },
  };
};

export const getUnpublishedProducts = async (store_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/products/${store_id}/store?type=unpublished`,
      next,
    },
  };
};

export const getProduct = async (product_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/products/${product_id}`,
      next,
    },
  };
};

export const deleteProduct = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/products/${payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};

export const actionEditProduct = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/products/${payload.payload.product_id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const actionProductQuantity = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/products/${payload.payload.product_id}/quantity`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const updatePublishStatus = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/products/${payload.payload.id}/status`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};


// Discounts

export const AddDiscount = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/discounts/${payload.payload.store_id}?discount_type=product`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const getDiscounts = async (store_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/discounts/${store_id}`,
      next,
    },
  };
};

export const getDiscount = async (discount_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/discounts/${discount_id}/discount`,
      next,
    },
  };
};
export const getDiscountProducts = async (discount_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/discounts/${discount_id}/products`,
      next,
    },
  };
};

export const updateDiscount = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/discounts/${payload.payload.discount_id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const removeProductDiscount = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/discounts/${payload.payload.store_id}/remove`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const deleteDiscount = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/discounts/${payload.payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};




// Coupons

export const AddCoupon = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/coupons?all_products=${payload.payload.data.all_products}`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const getCoupons = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/coupons`,
      next,
    },
  };
};

export const getCoupon = async (discount_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/coupons/${discount_id}`,
      next,
    },
  };
};

export const updateCoupon = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/coupons/${payload.payload.id}?all_products=false`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const deleteCoupon = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/coupons/${payload.payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};


// Categories


export const getPartnerCategories = async (product_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `/partner/products/${product_id}/categories`,
      next,
    },
  };
};

export const getCategories = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/categories`,
      next,
    },
  };
};

export const getStoreCategory = async (category_id) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/categories/${category_id}`
    },
  };
};

export const AddCategory = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/categories`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const updateCategory = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/categories/${payload.payload.id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const updateCategoryStatus = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/categories/${payload.payload.id}/status`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const deleteCategory = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/categories/${payload.payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};


// Fees Delivery
export const getFees = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/fees`,
      next,
    },
  };
};

export const AddFee = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/fees`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const getSingleFee = async (fee_id) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/fees/${fee_id}`
    },
  };
};

export const updateFee = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/fees/${payload.payload.id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const deleteFee = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/fees/${payload.payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};


// Service Charge
export const getServiceCharges = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/charges`,
      next,
    },
  };
};

export const AddServiceCharge = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/charges`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const getSingleServiceCharge = async (charge_id) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/charges/${charge_id}`
    },
  };
};

export const updateServiceCharge = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/charges/${payload.payload.id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const deleteServiceCharge = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/charges/${payload.payload.id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};



// Configuration
export const getConfigurations = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/config`,
      next,
    },
  };
};

export const AddConfiguration = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/config`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const getSingleConfiguration = async (config_key) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/config/${config_key}`
    },
  };
};

export const updateConfiguration = async (payload = {}, next = (f) => f) => {

  let headers = {};

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/config/${payload.payload.key}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const deleteConfiguration = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/settings/config/${payload.payload.key}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};


// SUBSCRIPTIONS

export const subscribeStoreFree = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/subscriptions/${payload.payload.store_id}/free?duration=monthly`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

export const subscribeStorePayment = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      // url: `admin/subscriptions/${payload.payload.store_id}/pay?duration=${payload.payload.data.duration}`,
      url: `admin/subscriptions/${payload.payload.store_id}/pay?duration=monthly`,
      options: {
        method: "POST",
        // body: JSON.stringify({}),
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};
// Get Store Subscription
export const getStoreSubscription = async (store_id) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/subscriptions/${store_id}?status=all`
    },
  };
};

// Get Store Subscription
export const getASubscription = async (payload = {} ) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/subscriptions/${payload.store_id}/${payload.subscription_id}`
    },
  };
};

// Update store Data 
export const UpdateSubscription = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/subscriptions/${payload.payload.subscription_id}/upgrade`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Cancel store Data 
export const cancelSubscription = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/subscriptions/${payload.payload.subscription_id}/cancel`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};



// PLANS

export const AddPlan = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/plans`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Update Plan
export const UpdatePlan = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/plans/${payload.payload.plan_id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// GET ALL PLANS
export const getPlans = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/plans`,
      next,
    },
  };
}

// Get a store Plan
export const getStorePlan = async (plan_id) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/plans/${plan_id}`
    },
  };
};

export const deletePlan = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/plans/${payload.payload.plan_id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};



// DRIVER

export const AddDriver = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Update Driver 
export const UpdateDriver = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers/${payload.payload.driver_id}`,
      options: {
        method: "PATCH",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// GET ALL Drivers
export const getDrivers = async (next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers`,
      next,
    },
  };
}

// Get a store Driver 
export const getSingleDriver = async (driver_id) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers/${driver_id}`
    },
  };
};

export const deleteDriver = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers/${payload.payload.driver_id}`,
      options: {
        method: "DELETE",
        headers,
      },
      next,
    },
  };
};


// Driver Assignment
// GET ALL Driver Order
export const getDriverOrders = async (driver_id, next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers/${driver_id}/orders`,
      next,
    },
  };
};

export const getOrderHistory = async (payload={},  next = (f) => f) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers/orders/${payload.payload.order_id}`,
      next,
    },
  };
};


// export const getOrderHistory = async (payload = {} ) => {
//   return {
//     type: SINGLE_API,
//     payload: {
//       url: `admin/drivers/order/${payload.payload.order_id}}`
//     },
//   };
// };

// Get Driver Assignment
export const getDriverAssignment = async (order_id) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers/order/assignment/${order_id}`
    },
  };
};


//Assign Order to Driver 
export const assignDriverToOrder = async (payload = {}, next = (f) => f) => {
  let headers = {};
  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});
  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers/order/assign`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Re Assign Order to Driver 
export const changeDriver = async (payload = {}, next = (f) => f) => {
  let headers = {};
  console.log(payload);

  payload.type == "multipart"
    ? (headers = {
      "Content-Type": "multipart/form-data",
    })
    : (headers = {});

  return {
    type: SINGLE_API,
    payload: {
      url: `admin/drivers/order/reassign/${payload.payload.assignment_id}`,
      options: {
        method: "POST",
        headers,
      },
      payload: payload.payload.data,
      next,
    },
  };
};

// Get Driver Assignment
export const getPaymentKey = async (type) => {
  return {
    type: SINGLE_API,
    payload: {
      url: `settings/keys/public?type=${type}`
    },
  };
};

