const urls = {
  API_URL: "https://api.getethco.com/api/v1/",
  // API_URL: "https://ethco-dev.herokuapp.com/api/v1/",
  // API_URL: "http://127.0.0.1:4001/api/v1/",
  WEB_URL: process.env.NEXT_PUBLIC_WEB_URL,
};

export default urls;
// 
