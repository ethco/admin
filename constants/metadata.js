import LINK from "../constants/urls";

const metadata = {
  APP_NAME: "Ethco UK",
  APP_DESCRIPTION: "...",
  IMG_SHARE: LINK.WEB_URL + "/images/icons/favicon.svg",
  KEY_WORDS: "",
  WEB_URL: LINK.WEB_URL,
  PRIMARY_COLOR: "#654FF3",
};

export default metadata;
