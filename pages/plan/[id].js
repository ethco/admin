import { useState, useEffect } from "react";
import DashboardLayout from "../../layouts/DashboardLayout";
import Head from "../../components/Head";
import Link from "next/link";
import { Modal, Button, Input, Form, Select, Avatar } from "antd";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
  getStoreCategory,
} from "../../helper/redux/actions/app";
import { ExclamationCircleOutlined } from "@ant-design/icons";
const SingleCategory = ({ id }) => {
  const [loading, setLoading] = useState(true);
  const [deleteLoading, setDeleteLoading] = useState(false);
  const [category, setCategory] = useState({});
  const dispatch = useDispatch();

  
  const [ form ] = Form.useForm();
  const { Option } = Select;
  const { TextArea } = Input;
  const { confirm } = Modal;
 
  // console.log(storeList[0])
  
  const getACategory = async () =>{
    try {
      setLoading(true);
      await dispatch(await getStoreCategory(id))
        .then((response) => {
          setCategory(response.data);
          console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get Category, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  } 
   
  useAsync(async () =>{
   await getACategory()
  }, [])


 

    // Delete Coupon
    const confirmDelete = () => {
      confirm({
        title: "Confirm delete",
        icon: <ExclamationCircleOutlined />,
        content: "Are you sure to delete this coupon?",
        async onOk() {
          try {
            setDeleteLoading(true);
  
            await dispatch(await deleteCoupon(couponId))
              .then((response) => {
                Router.push(`/coupon`);
                  setDeleteLoading(false);
                notification.success({
                  message: "Coupon",
                  description: "Coupon deleted successfully",
                });
              })
              .catch((error) => {
                setDeleteLoading(false);
              });
          } finally {
            setLoading(false);
          }
        },
        onCancel() { },
      });
    };

  return (
    <>
      <Head title={"Coupon"} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader  title="Coupon details" page="coupon">
          <Button
              type="primary"
              className="mx-3"
              primary
              // onClick={() => setStoreInfo(true)}
            >
              Update Category
            </Button>
            <Button danger loading={deleteLoading} onClick={confirmDelete}>
              Delete Category
            </Button>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <div className="row">
            {category &&
              <div className="row">
                 <div className="col-md-3">
                <div className="card card-bordered">
                  <div className="card-body text-center">
                    <div className="mx-auto mb-4">
                      <Avatar
                        style={{
                          backgroundColor: "#e09335",
                          verticalAlign: "middle",
                        }}
                        src={category.banner}
                        size={80}
                      >
                        
                          <span>
                            {category.name.charAt(0).toUpperCase()}
                          </span>
                        
                      </Avatar>
                    </div>
                    <h5 className="font-size-15 mb-1 text-dark">
                      {category.name.toUpperCase()}
                    </h5>
                    

                  </div>
                </div>
              </div> 
              <div className="col-xl-6">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title mb-4">Customer activities</h4>

                    <div className="table-responsive">
                      <table className="table table-nowrap mb-0">
                        <tbody>
                          <tr>
                            <th scope="row">Registered date :</th>
                            <td>{moment(category.created_at).format("ddd, MMM Do YYYY")}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
          
              </div>
            }
            </div>
          )}
        </div>
      </DashboardLayout>
    </>
  );
};

SingleCategory.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default SingleCategory;
