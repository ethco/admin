import { useState, useEffect } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import ReactPaginate from 'react-paginate';
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  getPlans,
  AddPlan,
  UpdatePlan,
  deletePlan
} from "../../helper/redux/actions/app";
import {
  ExclamationCircleOutlined,
  UploadOutlined, CloseCircleOutlined
} from "@ant-design/icons";
import { useAsync } from "react-use";
import Router from "next/router";
import {
  notification, Button, Form, Badge,Select, Avatar, Input, InputNumber,
  Table, Switch, Spin, Modal, Tooltip
} from "antd";
import { render } from "react-dom";




export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  // const [deleteLoading, setDeleteLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);
  const [addPlan, setAddPlan] = useState(false);
  // const [editCategory, setEditCategory] = useState(false);
  const [plans, setPlans] = useState([]);
  const [offset, setOffset] = useState(0);
  const [perPage] = useState(10);
  const [pageCount, setPageCount] = useState(0)
  const [isEditing, setIsEditing] = useState(false)
  const [editPlan, setEditPlan] = useState(null)
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);

  const { confirm } = Modal;
  const [form] = Form.useForm();
  const { Option } = Select;
  const { TextArea } = Input;


   const columns = [
    {
      key: 1,
      title: 'Date added',
      dataIndex: 'updated_at',
      render: (updated_at) => (<span className="text-small">
        {moment(updated_at).format(
          "ddd, MMM Do YYYY"
        )}
      </span>)
    },
    {
      key: 2,
      title: 'Name',
      dataIndex: 'name',
      render:(name) => (
        <p>{name}{console.log(name)}</p>
        
      )
    },
    {
      title: "Edit plan",
      key: 3,
      dataSource: ['id'],
      render: (plan) => (
       <Button
                  type="primary"
                  onClick={() => EditPlan(plan)}
                >
                   Edit
                </Button>
      )
    },
    {
      title: "Remove plan",
      key: 6,
      dataSource: ['id'],
      render: (plan) => (
         <Tooltip title="Remove plan">
                  <Button
                    onClick={() => {
                      confirmDelete(plan.id);
                    }}
                    type="danger"
                    icon={<CloseCircleOutlined />}>
                    Remove
                  </Button>

                </Tooltip>
      )
    }
  ];

  // Same Function with getAllStoreData()
  const getAllPlans = async () => {

    try {
      setLoading(true);
      await dispatch(await getPlans())
        .then((response) => {
          const data = (response.data);
          setPlans(data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all Plans, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }
  useEffect(async () => {
    await getAllPlans()
  }, [offset])



  // Add product to a store
  const AddNewPlan = async (value) => {
    try {
      setLoading(true);

      const payload = {
        data: {
          name: value.name,
          description: value.description,
          duration: value.duration,
          cost_monthly: value.cost_monthly,
          cost_annually: value.cost_annually,
          published_products: value.published_products,
          epos_hardware: value.epos_hardware,
          epos_integrations: value.epos_integrations,
          internet_connection: value.internet_connection,
          onboarding: value.onboarding,
          product_photos: value.product_photos,
          automation: value.automation,
          supply_intergration: value.supply_intergration,
          custom_store: value.custom_store,
          support: value.support
        }
      };
      await dispatch(await AddPlan({ payload }))
        .then((response) => {
          setAddPlan(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
          getAllPlans()
          // setAddPlan(false)
          onReset()
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  // Update Plan
   const actionUpdatePlan = async (value) => {
    try {
      const payload = {
        plan_id: editPlan.id,
        data: {
          name: value.name ? value.name : editPlan.name,
          description: value.description ? value.description : editPlan.description,
          duration: value.duration ? value.duration : editPlan.duration,
          cost_monthly: value.cost_monthly ? value.cost_monthly : editPlan.cost_monthly,
          cost_annually: value.cost_annually ? value.cost_annually : editPlan.cost_annually,
          published_products: value.published_products ? value.published_products : editPlan.published_products,
          epos_hardware: value.epos_hardware ? value.epos_hardware : editPlan.epos_hardware,
          epos_integrations: value.epos_integrations ? value.epos_integrations : editPlan.epos_integrations,
          internet_connection: value.internet_connection ? value.internet_connection : editPlan.internet_connection,
          onboarding: value.onboarding ? value.onboarding :  editPlan.onboarding,
          product_photos: value.product_photos ? value.product_photos : editPlan.product_photos,
          automation: value.automation ? value.automation : editPlan.automation,
          supply_intergration: value.supply_intergration ? value.supply_intergration : editPlan.supply_intergration,
          custom_store: value.custom_store ? value.custom_store : editPlan.custom_store,
          support: value.support ? value.support : editPlan.support
        }
      };
      await dispatch(await UpdatePlan({ payload }))
        .then((response) => {
          setIsEditing(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
          getAllPlans()
          onReset()
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  const EditPlan = (plan) => {
    setEditPlan({...plan})
    setIsEditing(true)
  };

  const checkForm = (value) => {
    console.log(value)
    console.log(editPlan.description)
  }



  const confirmDelete = (id) => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to delete this plan?",
      async onOk() {
        try {
          setPageLoading(true);
          const payload = {
            plan_id: id
          };

          await dispatch(await deletePlan({ payload }))
            .then((response) => {
              setPageLoading(false);
              notification.success({
                message: "Delete Category",
                description: "Category deleted successfully",
              });
              getAllPlans();
            })
            .catch((error) => {
              setPageLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() { },
    });
  };


  const statusChange = async (id, status) => {

    try {
      setPageLoading(true);
      const payload = {
        id: id,
        data: {
          status: status ? "enabled" : "disabled"
        }
      };
      console.log(id, status)
      await dispatch(await UpdatePlanStatus({ payload }))
        .then((response) => {
          notification.success({
            message: "Category Status",
            description: "Status updated successfully",
          });
          getAllPlans();
          setPageLoading(false);
          console.log(response)
        })
        .catch((error) => {
          setPageLoading(false);
        });
    } finally {
      setLoading(false);
    }
  };
  const handlePageClick = (e) => {
    const selectedPage = e.selected;
    setOffset(selectedPage + 1)
  };

  const addPlanImage = (e) => {
    console.log('Upload event:', e);

    if (Array.isArray(e)) {
      return e;
    }

    return e?.fileList;
  };

  const onReset = () => {
    form.resetFields();
  };



  return (
    <>
      <Head title="Plans" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Plans" page="Plans">
            <Button type="primary" onClick={() => setAddPlan(true)}>
              Add Plan </Button>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
      
              
 <Table
            loading={loading}
            columns={columns}
            dataSource={plans}
            pagination={{
              current: page,
              pageSize: pageSize,
              total: plans.length,
              onChange: (page, pageSize)=>{
                setPage(page);
                setPageSize(pageSize);
              }
            }
            }
            />  

              {/* Add Plan */}
              <Modal
                title="Add Plan"
                centered
                visible={addPlan}
                width={1000}
                onCancel={() => {
                  setAddPlan(false)
                  onReset()
                }}
                footer={null}
              >
                <Form
                  form={form}
                  name="add_plan"
                  onFinish={AddNewPlan}
                  layout="vertical"
                  style={{
                    display: 'grid',
                    gridTemplateColumns: '1fr 1fr',
                    gridColumnGap: '24px'
                  }}
                >
                  <Form.Item name="name" label="Plan name"
                  rules={[
                    {
                      required: true,
                      message: "Please enter plan name!",
                    }]}>
                    <Input
                      placeholder="Plan name"
                      className="inputWidthFull"
                      label="Plan name"
                    />
                  </Form.Item>


                  <Form.Item name="duration" label="Duration" 
                   rules={[
                    {
                      required: true,
                      message: "Please choose duration!",
                    }]}>
                    <Select placeholder="Annually">
                      <Option value={'annually'} >Annually</Option>
                      <Option value={'monthly'}>Monthly</Option>
                    </Select>
                  </Form.Item>


                  <Form.Item name="cost_monthly" label="Monthly charge" rules={[
                    {
                      required: true,
                      message: "Please ente Monthly charge!",
                    },
                  ]}>
                    <InputNumber
                      placeholder="Monthly charge"
                      className="inputWidthFull"
                      label="Monthly charge"
                    />
                  </Form.Item>
                  <Form.Item name="cost_annually" label="Annual charge" rules={[
                    {
                      required: true,
                      message: "Please enter Annually charge!",
                    },
                  ]}>
                    <InputNumber
                      placeholder="Annually charge"
                      className="inputWidthFull"
                      label="Annual charge"
                    />
                  </Form.Item>

                  <Form.Item name="published_products" label="Number of products" rules={[
                    {
                      required: true,
                      message: "Please enter Number of products!",
                    },
                  ]}>
                    <InputNumber
                      placeholder="Number of products"
                      className="inputWidthFull"
                      label="Number of products"
                    />
                  </Form.Item>

                  <Form.Item name="epos_hardware" label="Epos hardware" 
                   rules={[
                    {
                      required: true,
                      message: "Is epos hardware supported?",
                    }]}
                    >
                    <Select placeholder="Yes">
                      <Option value={1} >Yes</Option>
                      <Option value={0}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="epos_integrations" label="Epos integrations"
                   rules={[
                    {
                      required: true,
                      message: "Is epos integrations supported?",
                    }]} >
                    <Select placeholder="Yes">
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="internet_connection" label="Internet connection" 
                  rules={[
                    {
                      required: true,
                      message: "Is internet connection enabled?",
                    }]} >
                    <Select placeholder="Yes">
                      <Option value={1} >Yes</Option>
                      <Option value={0}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="onboarding" label="Onboarding" 
                   rules={[
                    {
                      required: true,
                      message: "Onboarding?",
                    }]} 
                    >
                    <Select placeholder="one time">
                      <Option value={"one_time"} >one time</Option>
                      {/* <Option value={0}>No</Option> */}
                    </Select>
                  </Form.Item>

                  <Form.Item name="product_photos" label="Product photos"
                   rules={[
                    {
                      required: true,
                      message: "Product photos support?",
                    }]}  >
                    <Select placeholder="Yes">
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="automation" label="Automation" 
                  rules={[
                    {
                      required: true,
                      message: "Automation?",
                    }]} >
                    <Select placeholder="Yes">
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="supply_intergration" label="Supply intergration" 
                   rules={[
                    {
                      required: true,
                      message: "Supply intergration support?",
                    }]}>
                    <Select placeholder="Yes">
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="custom_store" label="Custom store" 
                  rules={[
                    {
                      required: true,
                      message: "Custom store?",
                    }]}>
                    <Select placeholder="Yes">
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="support" label="Support" 
                   rules={[
                    {
                      required: true,
                      message: "Support?",
                    }]}>
                    <Select placeholder="Yes">
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>
                  
                  <Form.Item name="description"
                   rules={[
                    {
                      required: true,
                      message: "Pease enter plan description?",
                    }]}>
                    <TextArea rows={4}
                      placeholder="Plan description"
                      label="Description"
                    />

                  </Form.Item>

                  <Form.Item
                   style={{
                    alignSelf: 'end',
                    justifySelf: 'end'
                  }}
                  >
                    <Button
                      loading={loading}
                      htmlType="submit"
                      className="mt-2"
                      type="primary"
                    >
                      Add Plan
                    </Button>
                    <Button
                      className="mt-2 mx-3"
                      type="primary"
                      onClick={() => {
                        setAddPlan(false)
                        onReset()
                      }}
                    >
                      Cancel
                    </Button>
                  </Form.Item >
                </Form>
              </Modal>


              {/* Edit Plan */}
              <Modal
                title="Edit Plan"
                centered
                visible={isEditing}
                width={1000}
                onCancel={() => {
                  setIsEditing(false)
                  onReset()
                }}

                onOk={() => {
                  setIsEditing(false)
                }}
                footer={null}
              >
                <Form
                  form={form}
                  name="add_plan"
                  onFinish={actionUpdatePlan}
                  style={{
                    display: 'grid',
                    gridTemplateColumns: '1fr 1fr',
                    gridColumnGap: '24px'
                  }}
                  layout="vertical"
                >
                  <Form.Item name="name" label="Plan name">
                    <Input
                      placeholder={editPlan?.name}
                      className="inputWidthFull"
                      label="Plan name"
                    />
                  </Form.Item>


                  <Form.Item name="duration" label="Duration" >
                    <Select placeholder={editPlan?.duration}>
                      <Option value={'annually'} >Annually</Option>
                      <Option value={'monthly'}>Monthly</Option>
                    </Select>
                  </Form.Item>


                  <Form.Item name="cost_monthly" label="Monthly charge">
                    <InputNumber
                      placeholder={editPlan?.cost_monthly}
                      className="inputWidthFull"
                      label="Monthly charge"
                    />
                  </Form.Item>
                  <Form.Item name="cost_annually" label="Annual charge">
                    <InputNumber
                      placeholder={editPlan?.cost_annually}
                      className="inputWidthFull"
                      label="Annual charge"
                    />
                  </Form.Item>

                  <Form.Item name="published_products" label="Number of products" >
                    <InputNumber
                      placeholder={editPlan?.published_products}
                      className="inputWidthFull"
                      label="Number of products"
                    />
                  </Form.Item>

                  <Form.Item name="epos_hardware" label="Epos hardware" >
                    <Select placeholder={editPlan?.epos_hardware}>
                      <Option value={1} >Yes</Option>
                      <Option value={0}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="epos_integrations" label="Epos integrations" >
                    <Select placeholder={editPlan?.epos_integrations}>
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="internet_connection" label="Internet connection" >
                    <Select placeholder={editPlan?.internet_connection}>
                      <Option value={1} >Yes</Option>
                      <Option value={0}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="onboarding" label="Onboarding" >
                    <Select placeholder={editPlan?.onboarding}>
                      <Option value={"one_time"} >one time</Option>
                      {/* <Option value={0}>No</Option> */}
                    </Select>
                  </Form.Item>

                  <Form.Item name="product_photos" label="Product photos" >
                    <Select placeholder={editPlan?.product_photos}>
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="automation" label="Automation" >
                    <Select placeholder={editPlan?.automation}>
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="supply_intergration" label="Supply intergration" >
                    <Select placeholder={editPlan?.supply_intergration}>
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="custom_store" label="Custom store" >
                    <Select placeholder={editPlan?.custom_store}>
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item name="support" label="Support" >
                    <Select placeholder={editPlan?.support}>
                      <Option value={"yes"} >Yes</Option>
                      <Option value={"no"}>No</Option>
                    </Select>
                  </Form.Item>

                  
                  <Form.Item name="description">
                    <TextArea rows={4}
                      placeholder={editPlan?.description}
                      label="Description"
                    />
                  </Form.Item>

                  <Form.Item
                     style={{
                      alignSelf: 'end',
                      justifySelf: 'end'
                    }}>
                    <Button
                      loading={loading}
                      htmlType="submit"
                      className="mt-2"
                      type="primary"
                    >
                      Update Plan
                    </Button>
                    <Button
                      className="mt-2 mx-3"
                      type="primary"
                      onClick={() => {
                        setIsEditing(false)
                        onReset()
                      }}
                    >
                      Cancel
                    </Button>
                  </Form.Item >
                </Form>
              </Modal>

            
            </Spin>
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
