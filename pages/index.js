import { useState, useEffect } from "react";
import Head from "../components/Head";
import DashboardLayout from "../layouts/DashboardLayout";
import PageHeader from "../components/PageHeader";
import PageLoading from "../components/PageLoading";
import AuthStorage from "../helper/utils/auth-storage";
import Link from "next/link";
import { useDispatch } from "react-redux";
import Router from "next/router";
import {Spin, Button} from "antd"
import moment from "moment";
import { useAsync } from "react-use";


import {
  getAllOrders,
  getAllStores,

} from "../helper/redux/actions/app"

export default function Home() {
  const [user, setUser] = useState(AuthStorage.user || {});
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [pageLoading, setPageLoading] = useState(false);

  const [stores, setStores] = useState([]);
  const [orders, setOrders] = useState([]);
  const [perPage] = useState(10);


  const getAllStoreData = async () => {
    try {
      setLoading(true);
      await dispatch(await getAllStores())
        .then((response) => {
          setStores(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
          console.log(stores)
        });
    } finally {
      setLoading(false);
    }
  }

  const getAllOrdersData = async () =>{
    try {
      setLoading(true);
      await dispatch(await getAllOrders())
        .then((response) => {
          setOrders(response.data);
          console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all orders, try again",
          });
          console.log(orders)
        });
    } finally {
      setLoading(false);
    }
  }

  useAsync(async () =>{
    await getAllOrdersData()
    await getAllStoreData()
    console.log(AuthStorage.user)
  },[])



  return (
    <>
      <Head title="Home" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Dashboard" page="Home" />
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
          <div className="row">
            <div className="col-xl-4">
              <div className="card overflow-hidden">
                <div className="bg-primary bg-soft">
                  <div className="row">
                    <div className="col-7">
                      <div className="text-primary p-3">
                        <h5 className="text-primary">Welcome Back !</h5>
                        <p>Ethco Dashboard</p>
                      </div>
                    </div>
                    <div className="col-5 align-self-end">
                      <img
                        src="/images/profile-img.png"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-sm-4">
                      <div className="avatar-md profile-user-wid mb-4">
                        <img
                          src={
                            user.picture ||
                            `https://ui-avatars.com/api/?name=${user.name}&background=3c29b3&color=fff&size=128`
                          }
                          alt={user.name}
                          className="img-thumbnail rounded-circle"
                        />
                      </div>
                      <h5 className="font-size-15 text-truncate">
                        {user && user.name}
                      </h5>
                      <p className="text-muted mb-0 text-truncate text-uppercase">
                        Admin
                      </p>
                    </div>

                    <div className="col-sm-8">
                      <div className="pt-4">
                        <div className="row">
                          <div className="col-12">
                            <h5 className="font-size-15">{stores.length}</h5>
                            <p className="text-muted mb-0">Vendors</p>
                          </div>
                         
                        </div>
                        <div className="mt-4">
                          <Link href="/profile">
                            <a className="btn btn-primary waves-effect waves-light btn-sm">
                              View Profile
                              <i className="mdi mdi-arrow-right ms-1"></i>
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* <div className="card">
                <div className="card-body">
                  <h4 className="card-title mb-4">Monthly Earning</h4>
                  <div className="row">
                    <div className="col-sm-6">
                      <p className="text-muted">This month</p>
                      <h3>$34,252</h3>
                      <p className="text-muted">
                        <span className="text-success me-2">
                          12% <i className="mdi mdi-arrow-up"></i>
                        </span>
                        From previous period
                      </p>

                      <div className="mt-4">
                        <a
                          href="javascript: void(0);"
                          className="btn btn-primary waves-effect waves-light btn-sm"
                        >
                          View More <i className="mdi mdi-arrow-right ms-1"></i>
                        </a>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="mt-4 mt-sm-0">
                        <div id="radialBar-chart" className="apex-charts"></div>
                      </div>
                    </div>
                  </div>
                  <p className="text-muted mb-0">
                    We craft digital, graphic and dimensional thinking.
                  </p>
                </div>
              </div> */}

            </div>
            <div className="col-xl-8">
              <div className="row">
                <div className="col-md-4">
                  <div className="card mini-stats-wid">
                    <div className="card-body">
                      <div className="d-flex">
                        <div className="flex-grow-1">
                          <p className="text-muted fw-medium">Orders</p>
                          <h4 className="mb-0">{orders.length}</h4>
                        </div>

                        <div className="flex-shrink-0 align-self-center">
                          <div className="mini-stat-icon avatar-sm rounded-circle bg-primary">
                            <span className="avatar-title">
                              <i className="bx bx-copy-alt font-size-24"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="card mini-stats-wid">
                    <div className="card-body">
                      <div className="d-flex">
                        <div className="flex-grow-1">
                          <p className="text-muted fw-medium">Revenue</p>
                          <h4 className="mb-0">£0</h4>
                        </div>

                        <div className="flex-shrink-0 align-self-center">
                          <div className="avatar-sm rounded-circle bg-primary mini-stat-icon">
                            <span className="avatar-title rounded-circle bg-primary">
                              <i className="bx bx-archive-in font-size-24"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="card mini-stats-wid">
                    <div className="card-body">
                      <div className="d-flex">
                        <div className="flex-grow-1">
                          <p className="text-muted fw-medium">Average Price</p>
                          <h4 className="mb-0">£0</h4>
                        </div>

                        <div className="flex-shrink-0 align-self-center">
                          <div className="avatar-sm rounded-circle bg-primary mini-stat-icon">
                            <span className="avatar-title rounded-circle bg-primary">
                              <i className="bx bx-purchase-tag-alt font-size-24"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-body">
                  <div className="d-sm-flex flex-wrap">
                    <h4 className="card-title mb-4">settings</h4>
                    <div className="ms-auto">
                            <ul className="nav nav-pills gap-3">
                              {/* <li className="nav-item">
                                <Button type="primary" onClick={() => Router.push(`/coupon`)}>
                                <i className="bx bxs-coupon"></i>
                                  <span className="ms-2">Coupons</span>
                                   </Button>
                              </li> */}
                               <li className="nav-item">
                              <Button type="primary" onClick={() => Router.push(`/fees/config`)}>
                              <i className="bx bx-tools"></i>
                              <span className="ms-2">Configuration</span>
                                   </Button>
                              </li>

                              <li className="nav-item">
                              <Button type="primary" onClick={() => Router.push(`/fees/delivery`)}>
                              <i className="bx bx-money"></i>
                                  <span className="ms-2">Delivery fee</span>
                                   </Button>
                              </li>

                              <li className="nav-item">
                              <Button type="primary" onClick={() => Router.push(`/fees/service`)}>
                              <i className="bx bx-money"></i>
                              <span className="ms-2">Service charges</span>
                                   </Button>
                              </li>

                            </ul>
                    </div>
                  </div>

                  <div
                    id="stacked-column-chart"
                    className="apex-charts"
                    dir="ltr"
                  ></div>
                </div>
              </div>
              {/* Useful for Analytics */}
              {/* <div className="card">
                <div className="card-body">
                  <div className="d-sm-flex flex-wrap">
                    <h4 className="card-title mb-4">Sales Report</h4>
                    <div className="ms-auto">
                      <ul className="nav nav-pills">
                        <li className="nav-item">
                          <a className="nav-link" href="#">
                            Week
                          </a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="#">
                            Month
                          </a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link active" href="#">
                            Year
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  <div
                    id="stacked-column-chart"
                    className="apex-charts"
                    dir="ltr"
                  ></div>
                </div>
              </div> */}

            </div>
          </div>
              {/* Useful for Later */}
          <div className="row">
            <div className="col-xl-4">
              {/* <div className="card">
                <div className="card-body">
                  <h4 className="card-title mb-4">Social Source</h4>
                  <div className="text-center">
                    <div className="avatar-sm mx-auto mb-4">
                      <span className="avatar-title rounded-circle bg-primary bg-soft font-size-24">
                        <i className="mdi mdi-facebook text-primary"></i>
                      </span>
                    </div>
                    <p className="font-16 text-muted mb-2"></p>
                    <h5>
                      <a href="javascript: void(0);" className="text-dark">
                        Facebook -
                        <span className="text-muted font-16">125 sales</span>
                      </a>
                    </h5>
                    <p className="text-muted">
                      Maecenas nec odio et ante tincidunt tempus. Donec vitae
                      sapien ut libero venenatis faucibus tincidunt.
                    </p>
                    <a
                      href="javascript: void(0);"
                      className="text-primary font-16"
                    >
                      Learn more <i className="mdi mdi-chevron-right"></i>
                    </a>
                  </div>
                  <div className="row mt-4">
                    <div className="col-4">
                      <div className="social-source text-center mt-3">
                        <div className="avatar-xs mx-auto mb-3">
                          <span className="avatar-title rounded-circle bg-primary font-size-16">
                            <i className="mdi mdi-facebook text-white"></i>
                          </span>
                        </div>
                        <h5 className="font-size-15">Facebook</h5>
                        <p className="text-muted mb-0">125 sales</p>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="social-source text-center mt-3">
                        <div className="avatar-xs mx-auto mb-3">
                          <span className="avatar-title rounded-circle bg-info font-size-16">
                            <i className="mdi mdi-twitter text-white"></i>
                          </span>
                        </div>
                        <h5 className="font-size-15">Twitter</h5>
                        <p className="text-muted mb-0">112 sales</p>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="social-source text-center mt-3">
                        <div className="avatar-xs mx-auto mb-3">
                          <span className="avatar-title rounded-circle bg-pink font-size-16">
                            <i className="mdi mdi-instagram text-white"></i>
                          </span>
                        </div>
                        <h5 className="font-size-15">Instagram</h5>
                        <p className="text-muted mb-0">104 sales</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
            <div className="col-xl-4">
              {/* <div className="card">
                <div className="card-body">
                  <h4 className="card-title mb-5">Activity</h4>
                  <ul className="verti-timeline list-unstyled">
                    <li className="event-list">
                      <div className="event-timeline-dot">
                        <i className="bx bx-right-arrow-circle font-size-18"></i>
                      </div>
                      <div className="d-flex">
                        <div className="flex-shrink-0 me-3">
                          <h5 className="font-size-14">
                            22 Nov
                            <i className="bx bx-right-arrow-alt font-size-16 text-primary align-middle ms-2"></i>
                          </h5>
                        </div>
                        <div className="flex-grow-1">
                          <div>Responded to need “Volunteer Activities</div>
                        </div>
                      </div>
                    </li>
                    <li className="event-list">
                      <div className="event-timeline-dot">
                        <i className="bx bx-right-arrow-circle font-size-18"></i>
                      </div>
                      <div className="d-flex">
                        <div className="flex-shrink-0 me-3">
                          <h5 className="font-size-14">
                            17 Nov
                            <i className="bx bx-right-arrow-alt font-size-16 text-primary align-middle ms-2"></i>
                          </h5>
                        </div>
                        <div className="flex-grow-1">
                          <div>
                            Everyone realizes why a new common language would be
                            desirable...
                            <a href="javascript: void(0);">Read more</a>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li className="event-list active">
                      <div className="event-timeline-dot">
                        <i className="bx bxs-right-arrow-circle font-size-18 bx-fade-right"></i>
                      </div>
                      <div className="d-flex">
                        <div className="flex-shrink-0 me-3">
                          <h5 className="font-size-14">
                            15 Nov
                            <i className="bx bx-right-arrow-alt font-size-16 text-primary align-middle ms-2"></i>
                          </h5>
                        </div>
                        <div className="flex-grow-1">
                          <div>Joined the group “Boardsmanship Forum”</div>
                        </div>
                      </div>
                    </li>
                    <li className="event-list">
                      <div className="event-timeline-dot">
                        <i className="bx bx-right-arrow-circle font-size-18"></i>
                      </div>
                      <div className="d-flex">
                        <div className="flex-shrink-0 me-3">
                          <h5 className="font-size-14">
                            12 Nov
                            <i className="bx bx-right-arrow-alt font-size-16 text-primary align-middle ms-2"></i>
                          </h5>
                        </div>
                        <div className="flex-grow-1">
                          <div>Responded to need “In-Kind Opportunity”</div>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <div className="text-center mt-4">
                    <a
                      href="javascript: void(0);"
                      className="btn btn-primary waves-effect waves-light btn-sm"
                    >
                      View More <i className="mdi mdi-arrow-right ms-1"></i>
                    </a>
                  </div>
                </div>
              </div> */}
            </div>

            <div className="col-xl-4">
              {/* <div className="card">
                <div className="card-body">
                  <h4 className="card-title mb-4">
                    Top Cities Selling Product
                  </h4>

                  <div className="text-center">
                    <div className="mb-4">
                      <i className="bx bx-map-pin text-primary display-4"></i>
                    </div>
                    <h3>1,456</h3>
                    <p>San Francisco</p>
                  </div>

                  <div className="table-responsive mt-4">
                    <table className="table align-middle table-nowrap">
                      <tbody>
                        <tr>
                          <td style={{ width: "30%" }}>
                            <p className="mb-0">San Francisco</p>
                          </td>
                          <td style={{ width: "25%" }}>
                            <h5 className="mb-0">1,456</h5>
                          </td>
                          <td>
                            <div className="progress bg-transparent progress-sm">
                              <div
                                className="progress-bar bg-primary rounded"
                                role="progressbar"
                                style={{ width: "94%" }}
                                aria-valuenow="94"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              ></div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <p className="mb-0">Los Angeles</p>
                          </td>
                          <td>
                            <h5 className="mb-0">1,123</h5>
                          </td>
                          <td>
                            <div className="progress bg-transparent progress-sm">
                              <div
                                className="progress-bar bg-success rounded"
                                role="progressbar"
                                style={{ width: "82%" }}
                                aria-valuenow="82"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              ></div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <p className="mb-0">San Diego</p>
                          </td>
                          <td>
                            <h5 className="mb-0">1,026</h5>
                          </td>
                          <td>
                            <div className="progress bg-transparent progress-sm">
                              <div
                                className="progress-bar bg-warning rounded"
                                role="progressbar"
                                style={{ width: "70%" }}
                                aria-valuenow="70"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              ></div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div> */}
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <div className="card">
                <div className="card-body">
                  <h4 className="card-title mb-4">Latest Transaction</h4>
                  <div className="table-responsive">
                    <table className="table align-middle table-nowrap mb-0">
                      <thead className="table-light">
                        <tr>
                          <th className="align-middle">Order ID</th>
                          <th className="align-middle">Billing Name</th>
                          <th className="align-middle">Date</th>
                          <th className="align-middle">Total</th>
                          <th className="align-middle">Payment Status</th>
                          <th className="align-middle">Payment Method</th>
                          <th className="align-middle">View Details</th>
                        </tr>
                      </thead>
                      <tbody>
                      {orders &&
                     orders.map((order, index) =>(
                        <tr key={index}>
                        <td>
                          <a
                            href="javascript: void(0);"
                            className="text-body fw-bold"
                          >
                            {order.reference}
                          </a>
                        </td>
                        <td>{order.pickup && order.pickup.name}</td>
                        <td><span className="text-small">
                        {moment(order.created_at).format(
                          "ddd, MMM Do YYYY"
                        )}
                      </span></td>
                        <td>{`£${order.payment_details.total_amount}`}
</td>
                        <td>
                       <span className={`badge badge-pill font-size-11
                              ${order.payment_details.status && order.payment_details.status
                                          ? "badge-soft-success"
                                          : "badge-soft-danger"
                                        } mx-3`}>
                                          {order.payment_details.status && order.payment_details.status
                                        ? "Paid"
                                        : "Not Paid"
                                      }
                          {/* {order.payment_details.status} */}
                          </span>
                        </td>
                        <td className="text-capitalize">
                          <i className="fab fa-cc-mastercard me-1"></i>
                          {order.payment_details.type}
                        </td>
                        <td>
                          <button
                            type="button"
                            className="btn btn-primary btn-sm btn-rounded waves-effect waves-light"
                            data-bs-toggle="modal"
                            data-bs-target=".transaction-detailModal"
                            onClick={() => Router.push(`/orders/${order._id}`)}
                          >
                            View Details
                          </button>
                        </td>
                      </tr>
                      )).filter((e,k) => k < 10)
                      }
                     

                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
</Spin> )}
        </div>
      </DashboardLayout>
    </>
  );
}
