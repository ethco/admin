import { useState, useEffect } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  getConfigurations,
  getSingleConfiguration,
  updateConfiguration,
  deleteConfiguration,
  AddConfiguration
} from "../../helper/redux/actions/app";
import { useAsync } from "react-use";
import Router from "next/router";
import {
  notification,
  Select,
  Button,
  Modal,
  Badge,
  Switch,
  Spin,
  Tooltip,
  Form,
  Input,
  InputNumber
} from "antd";
import { createGlobalStyle } from "styled-components";

export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  // const [deleteLoading, setDeleteLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);
  const [addConfiguration, setAddConfiguration] = useState(false);
  const [Configurations, setConfigurations] = useState([]);
  const [configurationValue, setconfigurationValue] = useState('');
  const [configurationDescription, setConfigurationDescription] = useState('');
  const [newConfiguration, setNewConfiguration] = useState('');

  const [isEditing, setIsEditing] = useState(false)
  const [editConfig, setEditConfig] = useState(null)

  // const [pageLoading, setPageLoading] = useState(false);

  
  const [form] = Form.useForm();
  const { confirm } = Modal;
  const { Option } = Select;
  const { TextArea } = Input;


  // Same Function with getAllStoreData()
  const getAllConfigurations = async () => {

    try {
      setLoading(true);
      await dispatch(await getConfigurations())
        .then((response) => {
          const data = response.data
          // sort data with start_radius
          // const postData = [...data].sort((a, b) =>
          //   a.end_radius > b.end_radius ? -1 : 1,
          // );
          setConfigurations(data)
          setLoading(false)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all Service Charges, try again",
          });
          // console.log(categories)
        });
    } finally {
      setLoading(false);
    }
  }

  const getSingleConfigurations = async () => {

    try {
      setLoading(true);
      await dispatch(await getSingleConfiguration())
        .then((response) => {
          const data = response.data
          console.log(data)
          // sort data with start_radius
          // const postData = [...data].sort((a, b) =>
          //   a.end_radius > b.end_radius ? -1 : 1,
          // );
          // setConfigurations(data)
          setLoading(false)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all Service Charges, try again",
          });
          // console.log(categories)
        });
    } finally {
      setLoading(false);
    }
  }

  useEffect(async () => {
    await getAllConfigurations()
    // await getSingleConfigurations()
  },[])

  
  const EditConfig = (configuration) => {
    setEditConfig({...configuration})
    setIsEditing(true)
  };

const EditConfiguration = async (value) =>{
      try {
        setLoading(true);     
        const payload = {
          key: editConfig.key,
          data: {
            key: editConfig.key,
           value: value.value ? value.value : editConfig.value,
            description: value.description ? value.description : editConfig.description
          }
        };
        await dispatch(await updateConfiguration({ payload }))
        .then((response) => {
          console.log("Update Success")
          notification.success({
            message: "Configuration",
            description: "Configuration updated successfully",
          });
          getAllConfigurations()
          setPageLoading(false);
          setIsEditing(false)
        })          
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
}


  // Add Service Charge to a store
  const AddNewConfiguration = async (value) => {
    try {
      const payload = {
            data: {
            key: value.key,
           value: value.value,
            description: value.description
      }}
      await dispatch(await AddConfiguration({ payload }))
        .then((response) => {
          setAddConfiguration(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
          getAllConfigurations()
          // setAddCategory(false)
          onReset()
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

 const confirmDelete = (key) => {
  confirm({
    title: "Confirm delete",
    icon: <ExclamationCircleOutlined />,
    content: "Are you sure to delete this Service Charge?",
    async onOk() {
      try {
        setPageLoading(true);
        const payload = { 
          key: key
         };

        await dispatch(await deleteConfiguration({ payload }))
          .then((response) => {
            setPageLoading(false);
            notification.success({
              message: "Delete Service Charge",
              description: "Configuration deleted successfully",
            });
            getAllConfigurations();
          })
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
    },
    onCancel() {},
  });
};




const onReset = () => {
  form.resetFields();
};



  return (
    <>
      <Head title="Configuration" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Configuration" page="Configuration">

          <Button type="primary" className="mx-3" onClick={() => setAddConfiguration(true)}>
          Add configuration </Button>
          <Button type="primary" className="mx-3" onClick={() => Router.push(`/fees/service`)}>
            <i className="bx bx-money"></i>
               <span className="ms-2">Service fee</span>
           </Button>
           <Button type="primary" className="mx-3" onClick={() => Router.push(`/fees/delivery`)}>
            <i className="bx bx-money"></i>
               <span className="ms-2">Delivery fee</span>
           </Button>
        
          
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
           <table className="table align-middle table-nowrap mb-0">
                <thead className="table-light">
                  <tr>
                    <th className="align-middle">Type</th>
                    <th className="align-middle">Value</th>
                    <th className="align-middle">Description</th>
                    <th className="align-middle">Edit</th>
                    <th className="align-middle">Delete</th>
                  </tr>
                </thead>
                <tbody>
                {Configurations &&
                     Configurations.map((configuration, index) =>(
                        <tr key={index}>
                      <td>{`${configuration.key}`}</td>
                      <td>{configuration.description === "amount" ? `£${configuration.value}` : `${configuration.value}`}</td>
                      <td>{`${configuration.description}`}</td>

                       {/* Edit */}
                    <td>
                      <Button type="primary" onClick={() => EditConfig(configuration)}
                      > Edit service charge
                      </Button>
                      </td>

                      {/* Delete */}
                          <td>
                      <Tooltip title="Remove privilege">
                        <Button onClick={() => confirmDelete(configuration.key)}
                          type="danger"
                          style={{alignItems: "center"}} 
                          icon={<CloseCircleOutlined />}>
                            Delete
                          </Button>
                        
                      </Tooltip>

                      
                    </td>
                      </tr>
                      ))
                      }
                </tbody>
              </table>
           
            {/* Add Categry */}
             <Modal
                  title="Add service charge"
                  centered
                  visible={addConfiguration}
                  width={500}
                  onCancel={() =>{ 
                    setAddConfiguration(false)
                    onReset()}}
                  footer={null}
                >
               <Form
               form={form}
               name="add_configuration"
               onFinish={AddNewConfiguration}
              
               layout="vertical"
             >
              <Form.Item  name="key">
        <Input
          className="inputWidthFull"
          label="Configuration"
          placeholder="Enter configuration name"
        />
        </Form.Item> 
              <Form.Item  name="value">
        <InputNumber
          className="inputWidthFull"
          label="Value"
          placeholder="Enter Value"
        />
        </Form.Item>
        <Form.Item  name="description" label="Description">

        <TextArea rows={4}
              placeholder="Configuration description"
              label="Description"
            />

        </Form.Item>

               <Form.Item>
                 <Button
                   loading={loading}
                   htmlType="submit"
                   className="mt-2"
                   type="primary"
                 >
                   Add configuration
                 </Button>
                 <Button
                   className="mt-2 mx-3"
                   type="primary"
                   onClick={() =>{
                     setAddConfiguration(false)
                     onReset()
                   }}
                 >
                   Cancel
                 </Button>
               </Form.Item >
             </Form>
             </Modal>

 {/* Edit Configuration */}
                <Modal
                  title="Edit Configuration"
                  centered
                  visible={isEditing}
                  width={1000}
                  onCancel={() => {
                    setIsEditing(false)
                    onReset()
                  }}
                  onOk={() => {
                    setIsEditing(false)
                  }}
                  footer={null}
                >
                  <Form
                    form={form}
                    name="Edit_Configuration"
                    onFinish={EditConfiguration}
                    layout="vertical"
                  >
                    <Form.Item name="value">
                      <InputNumber
                        className="inputWidthFull"
                        label="Value"
                        placeholder={editConfig?.value}
                        // initialValue={editConfig?.value}
                      />
                    </Form.Item>
                    <Form.Item name="description"

                    >
                      <Input
                        className="inputWidthFull"
                        label="Description"
                        placeholder={editConfig?.description}
                        // initialValue={editConfig?.description}
                      />
                    </Form.Item>

                    <Form.Item
                      style={{
                        alignSelf: 'end',
                        justifySelf: 'end'
                      }}>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      >
                        Update Configuration
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => {
                          setIsEditing(false)
                          onReset()
                        }}
                      >
                        Cancel
                      </Button>
                    </Form.Item >
                  </Form>
              </Modal>
                  
            </Spin> 
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
