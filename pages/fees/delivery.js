import { useState, useEffect } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  getFees,
  getSingleFee,
  updateFee,
  deleteFee,
  AddFee
} from "../../helper/redux/actions/app";
import { useAsync } from "react-use";
import Router from "next/router";
import {
  notification,
  Avatar,
  Button,
  Modal,
  Badge,
  Switch,
  Spin,
  Tooltip,
  Form,
  Input,
  InputNumber
} from "antd";

export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  // const [deleteLoading, setDeleteLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);
  const [addFee, setAddFee] = useState(false);
  const [fees, setFees] = useState([]);
  const [startRadius, setStartRadius] = useState('');
  const [endRadius, setEndRadius] = useState('');
  const [newFee, setNewFee] = useState('');

  // const [pageLoading, setPageLoading] = useState(false);

  
  const [form] = Form.useForm();
  const { confirm } = Modal;

  // Same Function with getAllStoreData()
  const getAllFees = async () => {

    try {
      setLoading(true);
      await dispatch(await getFees())
        .then((response) => {
          const data = response.data
          // sort data with start_radius
          const postData = [...data].sort((a, b) =>
            a.end_radius > b.end_radius ? -1 : 1,
          );
          setFees(postData)
          setLoading(false)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all Fees, try again",
          });
          // console.log(categories)
        });
    } finally {
      setLoading(false);
    }
  }

  useEffect(async () => {
    await getAllFees()
  },[])

const EditFee = (fee) =>{
  confirm({
    title: "Edit Fee",
    icon: <ExclamationCircleOutlined />,
    content: ( <Form>
      <Form.Item  name="start">
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="Start radius"
          placeholder={fee.start_radius}
          defaultValue={fee.start_radius}
          onChange={(text) => setStartRadius(text.target.value)}
        />
        </Form.Item>
        <Form.Item  name="end">
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="End radius"
          placeholder={fee.end_radius}
          defaultValue={fee.end_radius}
          onChange={(text) => setEndRadius(text.target.value)}
        />
        </Form.Item>
        <Form.Item  name="fee">
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="Set fee"
          placeholder={fee.fee}
          defaultValue={fee.fee}
          onChange={(text) => setNewFee(text.target.value)}
        />
        </Form.Item>
    </Form>),
    async onOk() {
      try {
        setLoading(true);     
        const payload = {
          id: fee.id,
          data: {
            start_radius: startRadius ? startRadius : fee.start_radius,
            end_radius: endRadius ? endRadius : fee.end_radius,
            fee: newFee ? newFee : fee.fee,
            status: "active"
          }
        };
        await dispatch(await updateFee({ payload }))
        .then((response) => {
          console.log("Update Success")
          notification.success({
            message: "Category",
            description: "Category updated successfully",
          });
          getAllFees()
          setPageLoading(false);
        })          
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
    },
    onCancel() {},
  }); 
}


  // Add Fee to a store
  const AddNewFee = async (value) => {
    try {
      const payload = {
          data: {
            start_radius: value.start,
            end_radius:value.end,
            fee: value.fee,
            status: "active"
      }}
      await dispatch(await AddFee({ payload }))
        .then((response) => {
          setAddFee(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
          getAllFees()
          // setAddCategory(false)
          onReset()
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

 const confirmDelete = (id) => {
  confirm({
    title: "Confirm delete",
    icon: <ExclamationCircleOutlined />,
    content: "Are you sure to delete this fee?",
    async onOk() {
      try {
        setPageLoading(true);
        const payload = { 
          id: id
         };

        await dispatch(await deleteFee({ payload }))
          .then((response) => {
            setPageLoading(false);
            notification.success({
              message: "Delete fee",
              description: "Fee deleted successfully",
            });
            getAllFees();
          })
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
    },
    onCancel() {},
  });
};




const onReset = () => {
  form.resetFields();
};



  return (
    <>
      <Head title="Delivery fees" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Delivery fees" page="fees">
          <Button type="primary" onClick={() => setAddFee(true)}>
          Add delivery fee </Button>

          <Button type="primary" className="mx-3" onClick={() => Router.push(`/fees/config`)}>
            <i className="bx bx-money"></i>
               <span className="ms-2">Fee configurations</span>
           </Button>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
           <table className="table align-middle table-nowrap mb-0">
                <thead className="table-light">
                  <tr>
                    <th className="align-middle">Date</th>
                    <th className="align-middle">Start radius</th>
                    <th className="align-middle">End Radius</th>
                    {/* <th className="align-middle">Update status</th> */}
                    <th className="align-middle">Fee</th>
                    <th className="align-middle">Edit</th>
                    <th className="align-middle">Delete</th>
                  </tr>
                </thead>
                <tbody>
                {fees &&
                     fees.reverse().map((fee, index) =>(
                        <tr key={index}>
                        <td><span className="text-small">
                        {moment(fee.created_at).format(
                          "ddd, MMM Do YYYY"
                        )}
                      </span></td>
                      <td>{fee.start_radius < 2 ? `${fee.start_radius} mile` : `${fee.start_radius} miles`}</td>
                      <td>{fee.end_radius < 2 ? `${fee.end_radius} mile` : `${fee.end_radius} miles`}</td>
                      <td>{`£${fee.fee}`}</td>

                       {/* Edit */}
                    <td>
                      <Button type="primary" onClick={() => EditFee(fee)}
                      > Edit fee
                      </Button>
                      </td>

                      {/* Delete */}
                          <td>
                      <Tooltip title="Remove privilege">
                        <Button onClick={() => confirmDelete(fee.id)}
                          type="danger"
                          style={{alignItems: "center"}} 
                          icon={<CloseCircleOutlined />}>
                            Delete
                          </Button>
                        
                      </Tooltip>

                      
                    </td>
                      </tr>
                      ))
                      }
                </tbody>
              </table>
           
            {/* Add Categry */}
             <Modal
                  title="Add Fee"
                  centered
                  visible={addFee}
                  width={500}
                  onCancel={() =>{ 
                    setAddFee(false)
                    onReset()}}
                  footer={null}
                >
               <Form
               form={form}
               name="add_product"
               onFinish={AddNewFee}
              
               layout="vertical"
             >
              
              <Form.Item  name="start">
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="Start radius"
          placeholder="Set start radius"
        />
        </Form.Item>
        <Form.Item  name="end">
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="End radius"
          placeholder="Set end radius"
        />
        </Form.Item>
        <Form.Item  name="fee">
        <InputNumber

        // placeholder="Category title"
          className="inputWidthFull"
          label="Set fee"
          placeholder="Set delivery fee"
        />
        </Form.Item>

               <Form.Item>
                 <Button
                   loading={loading}
                   htmlType="submit"
                   className="mt-2"
                   type="primary"
                 >
                   Add Fee
                 </Button>
                 <Button
                   className="mt-2 mx-3"
                   type="primary"
                   onClick={() =>{
                     setAddFee(false)
                     onReset()
                   }}
                 >
                   Cancel
                 </Button>
               </Form.Item >
             </Form>
             </Modal>

                  
            </Spin> 
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
