import { useState, useEffect } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  getServiceCharges,
  getSingleServiceCharge,
  updateServiceCharge,
  deleteServiceCharge,
  AddServiceCharge,
  getConfigurations,
  updateConfiguration,
} from "../../helper/redux/actions/app";
import { useAsync } from "react-use";
import Router from "next/router";
import {
  notification,
  Select,
  Button,
  Modal,
  Badge,
  Switch,
  Spin,
  Tooltip,
  Form,
  Input,
  InputNumber
} from "antd";

export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  // const [deleteLoading, setDeleteLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);
  const [addServiceCharge, setAddServiceCharge] = useState(false);
  const [ServiceCharges, setServiceCharges] = useState([]);
  const [Configurations, setConfigurations] = useState([]);
  const [chargeAmount, setChargeAmount] = useState('');
  const [chargeType, setChargeType] = useState('');
  const [chargeStatus, setChargeStatus] = useState('');
  const [chargeFee, setChargeFee] = useState('');

  // const [pageLoading, setPageLoading] = useState(false);

  
  const [form] = Form.useForm();
  const { confirm } = Modal;
  const { Option } = Select;
  // Same Function with getAllStoreData()
  const getAllServiceCharges = async () => {

    try {
      setLoading(true);
      await dispatch(await getServiceCharges())
        .then((response) => {
          const data = response.data
          // sort data with start_radius
          // const postData = [...data].sort((a, b) =>
          //   a.end_radius > b.end_radius ? -1 : 1,
          // );
          setServiceCharges(data)
          setLoading(false)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all Service Charges, try again",
          });
          // console.log(categories)
        });
    } finally {
      setLoading(false);
    }
  }

  const getSingleServiceCharges = async () => {

    try {
      setLoading(true);
      await dispatch(await getSingleServiceCharge("63596a0adfad576d1fc0a9eb"))
        .then((response) => {
          const data = response.data
          console.log(data)
          // sort data with start_radius
          // const postData = [...data].sort((a, b) =>
          //   a.end_radius > b.end_radius ? -1 : 1,
          // );
          // setServiceCharges(data)
          setLoading(false)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all Service Charges, try again",
          });
          // console.log(categories)
        });
    } finally {
      setLoading(false);
    }
  }

  useEffect(async () => {
    await getAllServiceCharges()
    await getAllConfigurations()
    // await getSingleServiceCharges()
  },[])

const EditServiceCharge = (ServiceCharge) =>{
  confirm({
    title: "Edit Service Charge",
    icon: <ExclamationCircleOutlined />,
    content: ( <Form
      layout="vertical"   >
      <Form.Item  name="amount"
        onChange={(text) => setChargeAmount(text.target.value)}
      label="Order amount">
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="Order amount"
          placeholder={ServiceCharge.max_order_amount}
          defaultValue={ServiceCharge.max_order_amount}
        />
        </Form.Item>
       <Form.Item  name="fee"
         onChange={(text) => setChargeFee(text.target.value)}
       label="Set fee">
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="Set fee"
          placeholder={ServiceCharge.fee}
          defaultValue={ServiceCharge.fee}
        />
        </Form.Item>  


      <Form.Item name="type"
      label="Charge type"
      >
        <Select
          placeholder={ServiceCharge.type}
          defaultValue={ServiceCharge.type}
          onChange={(value) => {
            setChargeType(value)
            console.log(value)
          }}
        >
          <Option value={'percentage'} >Percentage</Option>
          <Option value={'amount'}>Amount</Option>
        </Select>

      </Form.Item>
        <Form.Item  name="status"
         label="Charge status"
         
         >
        <Select
          placeholder={ServiceCharge.status}
          defaultValue={ServiceCharge.status}
          onChange={(value) => {
            setChargeStatus(value)
          console.log(value)}}
        >
          <Option value={'active'} >Activate</Option>
          <Option value={'disabled'}>Deactivate</Option>
        </Select>
        </Form.Item>
    </Form>),
    async onOk() {
      try {
        setLoading(true);
        const payload = {
          id: ServiceCharge.id,
          data: {
            max_order_amount: chargeAmount ? chargeAmount : ServiceCharge.max_order_amount,
            fee: chargeFee ? chargeFee : ServiceCharge.fee,
            type: chargeType ? chargeType : ServiceCharge.type,
            status: chargeStatus ? chargeStatus : ServiceCharge.status,
          }
        };
        await dispatch(await updateServiceCharge({ payload }))
        .then((response) => {
          console.log("Update Success")
          notification.success({
            message: "Category",
            description: "Charges updated successfully",
          });
          getAllServiceCharges()
          setPageLoading(false);
        })          
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
    },
    onCancel() {},
  }); 
}


  // Add Service Charge to a store
  const AddNewServiceCharge = async (value) => {
    try {
      const payload = {
          data: {
            max_order_amount: value.maxOrder,
            fee: value.fee,
            type: value.type
      }}
      await dispatch(await AddServiceCharge({ payload }))
        .then((response) => {
          setAddServiceCharge(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
          getAllServiceCharges()
          // setAddCategory(false)
          onReset()
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

 const confirmDelete = (id) => {
  confirm({
    title: "Confirm delete",
    icon: <ExclamationCircleOutlined />,
    content: "Are you sure to delete this Service Charge?",
    async onOk() {
      try {
        setPageLoading(true);
        const payload = { 
          id: id
         };

        await dispatch(await deleteServiceCharge({ payload }))
          .then((response) => {
            setPageLoading(false);
            notification.success({
              message: "Delete Service Charge",
              description: "ServiceCharge deleted successfully",
            });
            getAllServiceCharges();
          })
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
    },
    onCancel() {},
  });
};
// CONFIGURATION

const getAllConfigurations = async () => {

  try {
    setLoading(true);
    await dispatch(await getConfigurations())
      .then((response) => {
        const data = response.data
        // sort data with start_radius
        // const postData = [...data].sort((a, b) =>
        //   a.end_radius > b.end_radius ? -1 : 1,
        // );
        setConfigurations(data)
        setLoading(false)
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to get all Service Charges, try again",
        });
        // console.log(categories)
      });
  } finally {
    setLoading(false);
  }
}


const EditConfiguration = (configuration) =>{
  console.log(configuration.key)
  confirm({
    title: "Edit Service Charge",
    icon: <ExclamationCircleOutlined />,
    content: ( <Form>
      <Form.Item  name="value"
          onChange={(text) => setconfigurationValue(text.target.value)}>
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="Value"
          placeholder={configuration.value}
          defaultValue={configuration.value}
        />
        </Form.Item>
        <Form.Item  name="description"
          onChange={(text) => configurationDescription(text.target.value)}>
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="Description"
          placeholder={configuration.description}
          defaultValue={configuration.description}
        />
        </Form.Item>
        
    </Form>),
    async onOk() {
      try {
        setLoading(true);     
        const payload = {
          key: configuration.key,
          data: {
            key: configuration.key,
           value: configurationValue ? configurationValue : configuration.value,
            description: configurationDescription ? configurationDescription : configuration.description
          }
        };
        await dispatch(await updateConfiguration({ payload }))
        .then((response) => {
          console.log("Update Success")
          notification.success({
            message: "Category",
            description: "Category updated successfully",
          });
          getAllConfigurations()
          setPageLoading(false);
        })          
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
    },
    onCancel() {},
  }); 
}



const onReset = () => {
  form.resetFields();
};



  return (
    <>
      <Head title="Service Charges" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Service Charges" page="Service Charge">
          <Button type="primary" className="mx-3" onClick={() => setAddServiceCharge(true)}>
          Add service charge </Button>

          <Button type="primary" className="mx-3" onClick={() => Router.push(`/fees/config`)}>
            <i className="bx bx-money"></i>
               <span className="ms-2">Fee configuration</span>
           </Button>
           
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
           <table className="table align-middle table-nowrap mb-0">
                <thead className="table-light">
                  <tr>
                    <th className="align-middle">Date</th>
                    <th className="align-middle">Order amount</th>
                    <th className="align-middle">Fee</th>
                    <th className="align-middle">type</th>
                    <th className="align-middle">Status</th>
                    <th className="align-middle">Edit</th>
                    <th className="align-middle">Delete</th>
                  </tr>
                </thead>
                <tbody>
                {ServiceCharges &&
                     ServiceCharges.reverse().map((ServiceCharge, index) =>(
                        <tr key={index}>
                        <td><span className="text-small">
                        {moment(ServiceCharge.created_at).format(
                          "ddd, MMM Do YYYY"
                        )}
                      </span></td>
                      <td>{`£${ServiceCharge.max_order_amount}`}</td>
                      <td>{ServiceCharge.type === "amount" ? `£${ServiceCharge.fee}` : `${ServiceCharge.fee}%`}</td>
                      <td className="text-capitalize">{ServiceCharge.type}</td>
                      <td className="text-capitalize">{ServiceCharge.status}</td>

                       {/* Edit */}
                    <td>
                      <Button type="primary" onClick={() => EditServiceCharge(ServiceCharge)}
                      > Edit service charge
                      </Button>
                      </td>

                      {/* Delete */}
                          <td>
                      <Tooltip title="Remove privilege">
                        <Button onClick={() => confirmDelete(ServiceCharge.id)}
                          type="danger"
                          style={{alignItems: "center"}} 
                          icon={<CloseCircleOutlined />}>
                            Delete
                          </Button>
                        
                      </Tooltip>

                      
                    </td>
                      </tr>
                      ))
                      }
                </tbody>
              </table>
           
            {/* Add Categry */}
             <Modal
                  title="Add service charge"
                  centered
                  visible={addServiceCharge}
                  width={500}
                  onCancel={() =>{ 
                    setAddServiceCharge(false)
                    onReset()}}
                  footer={null}
                >
               <Form
               form={form}
               name="add_product"
               onFinish={AddNewServiceCharge}
              
               layout="vertical"
             >
              
              <Form.Item  name="fee"
              label="Fee">
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="Fee"
          placeholder="order fee"
        />
        </Form.Item>

        <Form.Item  name="maxOrder"
        label="Price of total order">
        <InputNumber
        // placeholder="Category title"
          className="inputWidthFull"
          label="Orders"
          placeholder="Numbers of orders"
        />
        </Form.Item>
        <Form.Item
                      name="type"
                      label="Charge type">

                      <Select
                        defaultValue="percentage"
                        onChange={value => console.log(value)}
                      >
                        <Option value={'percentage'} >Percentage</Option>
                        <Option value={'amount'}>Amount</Option>
                      </Select>
                    </Form.Item>

               <Form.Item>
                 <Button
                   loading={loading}
                   htmlType="submit"
                   className="mt-2"
                   type="primary"
                 >
                   Add service charge
                 </Button>
                 <Button
                   className="mt-2 mx-3"
                   type="primary"
                   onClick={() =>{
                     setAddServiceCharge(false)
                     onReset()
                   }}
                 >
                   Cancel
                 </Button>
               </Form.Item >
             </Form>
             </Modal>

                  
            </Spin> 
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
