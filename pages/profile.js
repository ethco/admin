import { useState } from "react";
import Head from "../components/Head";
import DashboardLayout from "../layouts/DashboardLayout";
import PageHeader from "../components/PageHeader";
import AuthStorage from "../helper/utils/auth-storage";
import Link from "next/link";

export default function Home() {
  const [user, setUser] = useState(AuthStorage.user || {});

  const getAdminRole = (role) => {
    return role.replace(/_/g, " ");
  };

  return (
    <>
      <Head title="Home" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Dashboard" page="Home" />

          <div className="row">
            <div className="col-xl-4">
              <div className="card overflow-hidden">
                <div className="bg-primary bg-soft">
                  <div className="row">
                    <div className="col-7"></div>
                    <div className="col-5 align-self-end">
                      <img
                        src="/images/profile-img.png"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-sm-3">
                      <div className="avatar-md profile-user-wid mb-4">
                        <img
                          src={
                            user.picture ||
                            `https://ui-avatars.com/api/?name=${user.name}&background=3c29b3&color=fff&size=128`
                          }
                          alt={user.name}
                          className="img-thumbnail rounded-circle"
                        />
                      </div>
                    </div>

                    <div className="col-sm-7">
                      <div className="pt-4">
                        <div className="row">
                          <h5 className="font-size-15 text-truncate">
                            {user && user.name}
                          </h5>
                          <p>
                            <a
                              target="_blank"
                              href={`mailto:${user.email}`}
                              rel="noreferrer"
                            >
                              {user.email}
                            </a>
                          </p>
                        </div>
                        <div className="mt-2">
                          {/* <Link href="/update-profile">
                            <a className="btn btn-primary waves-effect waves-light btn-sm">
                              Update Profile
                            </a>
                          </Link> */}
                          <Link href="/update-password">
                            <a className="btn btn-secondary waves-effect waves-light btn-sm ">
                              Update Password
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-4">
              <div className="card">
                <div className="card-body">
                  <h4 className="card-title mb-4">Personal Information</h4>

                  <div className="table-responsive">
                    <table className="table table-nowrap mb-0">
                      <tbody>
                        <tr>
                          <th scope="row">Full Name :</th>
                          <td>{user.name}</td>
                        </tr>
                        <tr>
                          <th scope="row">E-mail :</th>
                          <td>{user.email}</td>
                        </tr>
                        <tr>
                          <th scope="row">Role :</th>
                          <td>
                            <span className="text-capitalize">
                              {user.admin && getAdminRole(user.admin.role)}
                            </span>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">Status :</th>
                          <td>
                            <span
                              className={`badge 
                            ${
                              user.admin && user.admin.status === "active"
                                ? "badge-soft-success"
                                : "badge-soft-danger"
                            }`}
                            >
                              {user.admin && user.admin.status}
                            </span>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    </>
  );
}
