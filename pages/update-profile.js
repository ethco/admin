import { useState } from "react";
import DashboardLayout from "../layouts/DashboardLayout";
import Head from "../components/Head";
import Link from "next/link";
import { Form, Button, Input, notification, Select, InputNumber } from "antd";
import PageHeader from "../components/PageHeader";
import { actionAddPartner } from "../helper/redux/actions/app";
import { useDispatch } from "react-redux";

const AddPartner = (props) => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const { Option } = Select;

  const submitForm = async (values) => {
    // try {
    //   setLoading(true);
    //   const data = {
    //     ...values,
    //   };
    //   await dispatch(await actionAddPartner({ data }))
    //     .then((response) => {
    //       form.resetFields();
    //       notification.success({
    //         message: "Partner",
    //         description: "Partner added successfully",
    //       });
    //     })
    //     .catch((error) => {});
    // } finally {
    //   setLoading(false);
    // }
  };
  return (
    <>
      <Head title="Update Profile" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Update profile" page="update-profile">
            <Link href="/profile">
              <a>
                <Button type="border">View profile</Button>
              </a>
            </Link>
          </PageHeader>
          <div className="row">
            <div className="col-md-7">
              <div className="card card-bordered">
                <div className="card-body">
                  <Form
                    form={form}
                    size="large"
                    name="add_partner"
                    onFinish={submitForm}
                    layout="vertical"
                  >
                    <div className="row">
                      <div className="col-md-6">
                        <Form.Item
                          name="contact_name"
                          label="Full name"
                          rules={[
                            {
                              required: true,
                              message: "Please input full name!",
                            },
                          ]}
                        >
                          <Input className="inputWidthFull" label="Full name" />
                        </Form.Item>
                      </div>

                      <div className="col-md-6">
                        <Form.Item
                          name="contact"
                          label="Mobile number"
                          rules={[
                            {
                              required: true,
                              message: "Please input mobile number!",
                            },
                          ]}
                        >
                          <Input label="Mobile number" />
                        </Form.Item>
                      </div>

                      <div className="col-md-12">
                        <Form.Item
                          name="email"
                          label="Email address"
                          rules={[
                            {
                              type: "email",
                              message: "The input is not valid E-mail!",
                            },
                            {
                              required: true,
                              message: "Please input partner email address!",
                            },
                          ]}
                        >
                          <Input label="Email address" type="email" />
                        </Form.Item>
                      </div>
                    </div>

                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      >
                        Update profile
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    </>
  );
};

export default AddPartner;
