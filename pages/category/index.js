import { useState, useEffect } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import ReactPaginate from 'react-paginate';
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  getCategories,
  AddCategory,
  updateCategory,
  updateCategoryStatus,
  deleteCategory,
  getStoreCategory
} from "../../helper/redux/actions/app";
import { ExclamationCircleOutlined,
  UploadOutlined, CloseCircleOutlined } from "@ant-design/icons";
import { useAsync } from "react-use";
import Router from "next/router";
import { notification,  Button, Form, Badge, Avatar,Input,
  Upload, Table, Switch, Spin, Modal, Tooltip} from "antd";
import { render } from "react-dom";




export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  // const [deleteLoading, setDeleteLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);
  const [addCategory, setAddCategory] = useState(false);
  // const [editCategory, setEditCategory] = useState(false);
  const [categories, setCategories] = useState([]);
  const [offset, setOffset] = useState(0);  
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);

  const [categoryName, setCategoryName] = useState('');
  const [categoryList, setCategoryList] = useState([]);

  const [isEditing, setIsEditing] = useState(false)
  const [editCategory, setEditCategory] = useState({})
  // const [pageLoading, setPageLoading] = useState(false);

  const { confirm } = Modal;
  const [form] = Form.useForm();


  const columns = [
    {
      key: 1,
      title: 'Date',
      dataIndex: 'created_at',
      render: (created_at) => (<span className="text-small">
        {moment(created_at).format(
          "ddd, MMM Do YYYY"
        )}
      </span>)
    },
    {
      key: 2,
      title: 'Category',
      dataIndex: ['name', 'banner'],
      render: (name, category) => (
        <>
          <Avatar
            style={{
              backgroundColor: "#e09335",
              verticalAlign: "middle",
            }}
            src={category.banner}
            size={24}
          >
            {category.name && (
              <span>{category.name.charAt(0).toUpperCase()}</span>
            )}
          </Avatar>
          <span className="mb-1 mx-2">{category.name}</span>
        </>
      )
    },
    {
      key: 3,
      title: 'Status',
      dataIndex: ['status', 'id'],
      render: (status, category) => (
        <Switch
          style={{}}
          checkedChildren="Enabled"
          unCheckedChildren="Disabled"
          checked={
            category.status === "enabled" ? true : false
          }
          onChange={(value) => {
            statusChange(category.id, value);
            console.log(`getAllCategories Status Changed ${category.status}`)
          }}
        />
      )
    },
    {
      key: 4,
      title: 'Date',
      dataIndex: 'updated_at',
      render: (updated_at) => (<span className="text-small">
        {moment(updated_at).format(
          "ddd, MMM Do YYYY"
        )}
      </span>)
    },
    {
      title: "Edit",
      key: 5,
      dataSource: ['name', 'id'],
      render: (text, record) => (
        <Button
          type="primary"
          onClick={() => EditCategory(record)}
        >
          Edit category
        </Button>
      )
    },
    {
      title: "Delete",
      key: 6,
      dataSource: ['name', 'id'],
      render: (text, record) => (
        <Button
          onClick={() => {
            confirmDelete(record.id);
          }}
          type="danger"
          icon={<CloseCircleOutlined />}>
          Delete
        </Button>
      )
    }
  ];
 

  // Same Function with getAllStoreData()
    const getAllCategories = async () =>{   
    try {
      setLoading(true);
      await dispatch(await getCategories())
        .then((response) => {
          const data = (response.data);
          setCategoryList(data)
          console.log(data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all Categories, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }
  useEffect(async () => {
    await getAllCategories()
  },[offset])


  const actionUpdateCategory = async (value) => {
    try {
      setLoading(true);
      const images1 = value.image > 0 ? value.image.map(e, i => {
        return e[i].thumbUrl
      }) : value.image ? value.image[0].thumbUrl : null
      console.log(images1)
      const payload = {
        id: editCategory.id,
        data: {
          name: value.name ? value.name : editCategory.name,
          banner: images1 ? images1 : editCategory.banner,
        }
      };
      await dispatch(await updateCategory({ payload }))
      .then((response) => {
        console.log("Update Success")
        setIsEditing(false)
        notification.success({
          message: "Category",
          description: "Category updated successfully",
        });
        getAllCategories()
        setPageLoading(false);
      })          
        .catch((error) => {
          setPageLoading(false);
        });
    } finally {
      setLoading(false);
    }
  }

  const EditCategory = (category) => {
    setEditCategory({...category})
    setIsEditing(true)
  };



  // Add product to a store
  const AddNewCategory = async (value) => {
    try {
      setLoading(true);
      const images1 = value.image > 0 ? value.image.map(e, i => {
        return e[i].thumbUrl
      }) : value.image[0].thumbUrl
      console.log(images1)
      const payload = {
        data: {
          name: value.name,
          banner: images1
        }
      };
      await dispatch(await AddCategory({ payload }))
        .then((response) => {
          setAddCategory(false)
          console.log("Update Success")
          notification.success({
            message: "Category",
            description: "Category added successfully",
          });
          getAllCategories()
          // setAddCategory(false)
          onReset()
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

 const confirmDelete = (id) => {
  confirm({
    title: "Confirm delete",
    icon: <ExclamationCircleOutlined />,
    content: "Are you sure to delete this category?",
    async onOk() {
      try {
        setPageLoading(true);
        const payload = { 
          id: id
         };

        await dispatch(await deleteCategory({ payload }))
          .then((response) => {
            setPageLoading(false);
            notification.success({
              message: "Delete Category",
              description: "Category deleted successfully",
            });
            getAllCategories();
          })
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
    },
    onCancel() {},
  });
};


const statusChange = async (id, status) => {
  
  try {
    setPageLoading(true);
    const payload = {
      id: id,
      data: {
        status: status ? "enabled" : "disabled"
      }
    };
    console.log(id, status)
    await dispatch(await updateCategoryStatus({ payload }))
      .then((response) => {
        notification.success({
          message: "Category Status",
          description: "Status updated successfully",
        });
        getAllCategories();
        setPageLoading(false);
        console.log(response)
      })
      .catch((error) => {
        setPageLoading(false);
      });
  } finally {
    setLoading(false);
  }
};

const handlePageClick = (e) => {
  const selectedPage = e.selected;
  setOffset(selectedPage + 1)
};

const addCategoryImage = (e) => {
  console.log('Upload event:', e);

  if (Array.isArray(e)) {
    return e;
  }

  return e?.fileList;
};

const onReset = () => {
  form.resetFields();
};



  return (
    <>
      <Head title="Category" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Category" page="Categories">
          <Button type="primary" onClick={() => setAddCategory(true)}>
          Add Category </Button>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
          
          <Table
            loading={loading}
            columns={columns}
            dataSource={categoryList}
            pagination={{
              current: page,
              pageSize: pageSize,
              total: categoryList.length,
              onChange: (page, pageSize)=>{
                setPage(page);
                setPageSize(pageSize);
              }
            }
            }
            />   

            {/* Add Categry */}
             <Modal
                  title="Add Category"
                  centered
                  visible={addCategory}
                  width={500}
                  onCancel={() =>{ 
                    setAddCategory(false)
                    onReset()}}
                  footer={null}
                >
               <Form
               form={form}
               name="add_category"
               onFinish={AddNewCategory}
              
               layout="vertical"
             >
               <Form.Item  name="name" label="Category title">
                 <Input
                 placeholder="Category title"
                   className="inputWidthFull"
                   label="Category title"
                  
                 /></Form.Item>
               <Form.Item name="image" valuePropName="fileList"
                 getValueFromEvent={addCategoryImage}
               >
                 <Upload name="image"
                   listType="picture">
                   <Button icon={<UploadOutlined />}>Click to upload</Button>
                 </Upload>
               </Form.Item>
            

               <Form.Item>
                 <Button
                   loading={loading}
                   htmlType="submit"
                   className="mt-2"
                   type="primary"
                 >
                   Add Category
                 </Button>
                 <Button
                   className="mt-2 mx-3"
                   type="primary"
                   onClick={() =>{
                     setAddCategory(false)
                     onReset()
                   }}
                 >
                   Cancel
                 </Button>
               </Form.Item >
             </Form>
             </Modal>


             {/* Edit Category */}
             <Modal
                title="Edit Category"
                centered
                visible={isEditing}
                width={500}
                onCancel={() => {
                  setIsEditing(false)
                  onReset()
                }}

                onOk={() => {
                  setIsEditing(false)
                }}
                footer={null}
              >
                {editCategory &&
                <Form
                  form={form}
                  name="add_Category"
                  onFinish={actionUpdateCategory}
                  layout="vertical"
                >
                                  
                     <Form.Item  name="name"
                    //  initialValue={editCategory?.name}
                     >
                                  <Input
                                  // placeholder="Category title"
                                    className="inputWidthFull"
                                    label="Category title"
                                    placeholder={editCategory?.name}
                                    
                                   
                                  /></Form.Item>
                                <Form.Item name="image" valuePropName="fileList"
                                   getValueFromEvent={addCategoryImage}>
                                  <Upload name="image"
                                    listType="picture"
                                    // placeholder={category.banner}
                                    // defaultValue={category.banner}
                                    >
                                    <Button icon={<UploadOutlined />}>Click to upload</Button>
                                  </Upload>

                  </Form.Item>

      

    

                  <Form.Item>
                    <Button
                      loading={loading}
                      htmlType="submit"
                      className="mt-2"
                      type="primary"
                    >
                      Add Category
                    </Button>
                    <Button
                      className="mt-2 mx-3"
                      type="primary"
                      onClick={() => {
                        setIsEditing(false)
                        onReset()
                      }}
                    >
                      Cancel
                    </Button>
                  </Form.Item >
                </Form>
}
              </Modal>
  
            </Spin> 
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
