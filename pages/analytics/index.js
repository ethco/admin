import { Fragment, useState } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  getStoreReport,
  getAllStores
} from "../../helper/redux/actions/app";
import { useAsync } from "react-use";
import Router from "next/router";
import {
  notification,
  Avatar,
  Button,
  Modal,
  Badge,
  Switch,
  Spin,
  Tooltip,
} from "antd";

export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [stores, setStores] = useState([]);
  const [pageLoading, setPageLoading] = useState(false);
  // const { confirm } = Modal;

  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getStoreReport())
        .then((response) => {
          console.log(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, []);
  
  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getAllStores())
        .then((response) => {
          setStores(response.data)
          // console.log(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, []);
  console.log(stores);


  return (
    <>
      <Head title="Manager" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Analytics" page="analytics"></PageHeader>
          {loading ? (
            <PageLoading />
          ) : (<div>
              <h1>stores</h1>
                 
                    {stores.map((store, index) => (
                    <Fragment key={index}>
                        <p className="mb-1">
                              {store.name}
                        </p>
                    </Fragment>
                  ))}
          </div>
            
          )} 
        </div>
      </DashboardLayout>
    </>
  );
}
