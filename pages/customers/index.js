import { useState } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import Router from "next/router";
import {
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { Button, Modal, Spin, Avatar, Switch, Tooltip, Table } from "antd";
import {
  getAllCustomers,
} from "../../helper/redux/actions/app";
import { useDispatch } from "react-redux";
import { useAsync } from "react-use";
import moment from "moment";

export default function Index() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [customers, setCustomers] = useState([]);
  const [customerID, setCustomerID] = useState("");
  const [pageLoading, setPageLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);

  const { confirm } = Modal;

   const columns = [
    {
      key: 1,
      title: 'Date added',
      dataIndex: 'created_at',
      render: (created_at) => (<span className="text-small">
        {moment(created_at).format(
          "ddd, MMM Do YYYY"
        )}
      </span>)
     },
     {
       key: 2,
       title: 'Full name',
       dataIndex: ['user'],
       render: (user) => (
         <>
           <Avatar
             style={{
               backgroundColor: "#e09335",
               verticalAlign: "middle",
             }}
             src={user?.picture}
             size={30}
           >
             {user?.name && (
               <span>
                 {user?.name.charAt(0).toUpperCase()}
               </span>
             )}
           </Avatar>
           <span className="mx-2">{user?.name}</span>
         </>
       )
     },
     {
      key: 3,
      title: 'Email',
      dataIndex: ['user'],
      render: (user) => (
       <p>{user?.email}</p>
      )
    },
    {
      key: 4,
      title: 'Phone number',
      dataIndex: ['phone', 'id'],
      render: (phone, customer) => (
       <p>{customer.phone} {customer.phone !== null && 
        <span className="mx-2">{customer.user?.phone_verified ? 
        <i className="bx bx-check-circle font-size-24" style={{color: "green"}}></i> : 
        <i className="bx bx-check-circle font-size-24" style={{color: "red"}}></i>
        }
        </span>}</p>
      )
    },

    {
      title: "View Customer",
      key: 3,
      dataSource: ['id'],
      render: (customer) => (
        <Button
        type="primary"
        onClick={() => {
          Router.push(`/customers/${customer._id}`);
        }}>
        View
      </Button>
      )
    },
   
  ];

  const getAllCustomersData = async () => {
    try {
      setLoading(true);
      await dispatch(await getAllCustomers())
        .then((response) => {
          setLoading(false);
          setCustomers(response.data);
          setCustomerID(response.data._id);
          console.log(response.data)
        })
        .catch((error) => {
          setLoading(false);
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  };


  useAsync(async () => {
    await getAllCustomersData();
    console.log(customerID)
  }, []);



  return (
    <>
      <Head title="Customers" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Customers" page="Customers">
            
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
              {/* <div className="table-responsive">
                <table className="table align-middle table-nowrap mb-0">
                  <thead className="table-light">
                    <tr>
                      <th className="align-middle">Date added</th>
                      <th className="align-middle">Full name</th>
                      <th className="align-middle">Email addres</th>
                      <th className="align-middle">Mobile number</th>
                      <th className="align-middle">View</th>
                    </tr>
                  </thead>
                  <tbody>
                    {customers.map((customer, index) => (
                      
                      <tr key={index}>
                        <>
                        <td>
                          <span className="text-small">
                            {moment(customer.created_at).format("ddd, MMM Do YYYY")}
                          </span>
                        </td>
                        <td><Avatar
                        style={{
                          backgroundColor: "#e09335",
                          verticalAlign: "middle",
                        }}
                        src={customer.user?.picture}
                        size={30}
                      >
                        {customer.user?.name && (
                          <span>
                            {customer.user?.name.charAt(0).toUpperCase()}
                          </span>
                        )}
                      </Avatar><span className="mx-2">{customer.user?.name}</span></td>
                        <td>{customer.user?.email}</td>
                        <td>{customer.phone}
                        {customer.phone !== null && 
                        <span className="mx-2">{customer.user?.phone_verified ? 
                        <i className="bx bx-check-circle font-size-24" style={{color: "green"}}></i> : 
                        <i className="bx bx-check-circle font-size-24" style={{color: "red"}}></i>
                        }
                        </span>}
                        </td>
                        <td><Button
                          type="primary"
                          onClick={() => {
                            Router.push(`/customers/${customer._id}`);
                          }}>
                          View Customer
                        </Button></td>
                        </> 
                      </tr>
                        ))}
                  </tbody>
                </table>
              </div> */}
                            
 <Table
            loading={loading}
            columns={columns}
            dataSource={customers}
            pagination={{
              current: page,
              pageSize: pageSize,
              total: customers.length,
              onChange: (page, pageSize)=>{
                setPage(page);
                setPageSize(pageSize);
              }
            }
            }
            /> 
            </Spin>
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
