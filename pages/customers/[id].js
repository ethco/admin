import { useState, useEffect } from "react";
import DashboardLayout from "../../layouts/DashboardLayout";
import Head from "../../components/Head";
import Link from "next/link";
import { Modal, Button, Avatar, Input, Select, InputNumber, Form } from "antd";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
  getCustomerData,
  updateCustomerAddress,
  actionUpdateCustomer,
  deleteCustomer,
  getCustomerAddress
} from "../../helper/redux/actions/app";
import { ExclamationCircleOutlined } from "@ant-design/icons";

const Customer = ({id}) => {
  const [loading, setLoading] = useState(true);

  const [customerData, setCustomerData] = useState({});
  const [customerUser, setCustomerUser] = useState("");
  const [addressLoading, setAddressLoading] = useState(false)
  const [detailsLoading, setDetailsLoading] = useState(false)
  const [onEdit, setOnEdit] = useState(false)
  const [emailDetails, setEmailDetails] = useState(false)
  const [phoneDetails, setPhoneDetails] = useState(false)
  const [address, setAddress] = useState([])
  const [deleteLoading, setDeleteLoading] = useState(false);



  const dispatch = useDispatch();
  const { confirm } = Modal;
  const {form} = Form.useForm();

  // Delete Customer
  const confirmDelete = () => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to delete this partner?",
      async onOk() {
        try {
          setDeleteLoading(true);
          const id = customerUser._id;

          await dispatch(await deleteCustomer({ id }))
            .then((response) => {
              Router.push("/customers");
                setDeleteLoading(false);
              notification.success({
                message: "Customer",
                description: "Customer deleted successfully",
              });
            })
            .catch((error) => {
              setDeleteLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() { },
    });
  };
// Uppdate Email
const updateCustomerEmail = async (value) => {
  try {
    setLoading(true);
    setDetailsLoading(true)
    const payload = {
      user_id: customerUser._id,
      data: {
        key : "email",
        value: value.email,
      }
    };
    await dispatch(await actionUpdateCustomer({ payload }))
      .then((response) => {
        setDetailsLoading(false)
        setLoading(false)
        setEmailDetails(false)
        getCustomerInfo()
        
        console.log("Update Success")
        notification.success({
          message: "User Info",
          description: "Customer updated successfully",
        });
      })
      .catch((error) => {
        setDetailsLoading(false)
      });
  } finally {
    setLoading(false);
    setDetailsLoading(false)
  }
}; 
 
// Update Phone number
const updateCustomerPhone = async (value) => {
  try {
    setLoading(true);
    setDetailsLoading(true)
    const payload = {
      user_id: customerUser._id,
      data: {
       key: "phone",
        value : value.phone
      }
    };
    await dispatch(await actionUpdateCustomer({ payload }))
      .then((response) => {
        setDetailsLoading(false)
        setLoading(false)
        setPhoneDetails(false)
        getCustomerInfo()
        
        console.log("Update Success")
        notification.success({
          message: "User Info",
          description: "Customer updated successfully",
        });
      })
      .catch((error) => {
        setDetailsLoading(false)
      });
  } finally {
    setLoading(false);
    setDetailsLoading(false)
  }
}; 

// Change Address
const onEditAddress = async (value) => {
  try {
    setAddressLoading(true);
    const payload = {
      address_id: "63311f9930b79796c6ff6737",
      data: {
        street: value.address,
        postcode: "12345",
        latitude: 52.2457888,
        longitude: -0.892535,
        phone: customerData.phone,
        address_type: " "

      },
    };
    await dispatch(await updateCustomerAddress({ payload }))
      .then((response) => {
        setAddressLoading(false);
        setOnEdit(false)
        customerAddress()
        getCustomerInfo()
        notification.success({
          message: "Address",
          description: "Address Changed successfully",
        });

        setTimeout(() => {
          Router.push("/orders");
        }, 1500);
      })
      .catch((error) => {
        setAddressLoading(false);
      });
  } finally {
    setLoading(false);
  }
}

// Code to Get Customer Address
const customerAddress = async () => {
  try {
    setLoading(true);
    await dispatch(await getCustomerAddress(id))
      .then((response) => {
        setAddress(response.data);
        console.log(response.data)
        
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to get customer details, try again",
        });
      });
    } finally {
      setLoading(false);
    }
  };

// Code to Get Customer Data
  const getCustomerInfo = async () => {
    try {
      setLoading(true);
      await dispatch(await getCustomerData(id))
        .then((response) => {
          setCustomerData(response.data);
          console.log(response.data)
          setCustomerUser(response.data.user)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get customer details, try again",
          });
        });
      } finally {
        setLoading(false);
      }
    };

  useAsync(async () => {
    await getCustomerInfo();
    customerAddress()
    console.log(id)
  }, []);





 
  return (
    <>
      <Head title={customerUser && customerUser.name || ""} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Customer" page="customers" page1={customerUser?.name}>
         
      
            <Button danger loading={deleteLoading} onClick={confirmDelete}>
              Delete Customer
            </Button>
            
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
              <div className="row">
                {customerData &&
                  <div className="row">
                    <div className="col-xl-6">
                      <div className="card overflow-hidden">
                        <div className="bg-primary bg-soft">
                          <div className="row">
                            <div className="col-7"></div>
                            <div className="col-5 align-self-end">
                              <img
                                src="/images/profile-img.png"
                                alt=""
                                className="img-fluid"
                              />
                            </div>
                          </div>
                        </div>
                        <div className="card-body pt-0">
                          <div className="row">
                            <div className="col-sm-3">
                              <div className="avatar-md profile-user-wid mb-4">
                                <img
                                  src={
                                    customerData.user?.picture ||
                                    `https://ui-avatars.com/api/?name=${customerData.user?.name.charAt(0).toUpperCase()}&background=3c29b3&color=fff&size=128`
                                  }
                                  alt={customerData.user?.name}
                                  className="img-thumbnail rounded-circle"
                                />
                              </div>
                            </div>

                            <div className="col-sm-9">
                              <div className="pt-4">
                                <div className="row">
                                  <h5 className="font-size-15 text-truncate">
                                    {customerData.user && customerData.user.name}
                                  </h5>
                                  <p style={{display: "flex"}}>
                                    <a
                                      target="_blank"
                                      href={`mailto:${customerData.user?.email}`}
                                      rel="noreferrer"
                                    >
                                      {customerData.user?.email}
                                    </a>
                                    <span
                                      className={`badge 
                              ${customerData.user?.email_verified && customerData.user?.email_verified
                                          ? "badge-soft-success"
                                          : "badge-soft-danger"
                                        } mx-3`} style={{height: "fit-content"}}
                                    >
                                      {customerData.user?.email_verified && customerData.user.email_verified
                                        ? "Verified"
                                        : "Not verified"
                                      }
                                    </span>

                                  </p>
                                  {customerData.phone && 
                                  <p>{customerData.phone}
                                    <span
                                      className={`badge 
                              ${customerData.user?.phone_verified && customerData.user.phone_verified
                                          ? "badge-soft-success"
                                          : "badge-soft-danger"
                                        } mx-3`}
                                    >
                                      {customerData.user?.phone_verified && customerData.user.phone_verified
                                        ? "Verified"
                                        : "Not verified"
                                      }
                                    </span>
                                  </p>}
                                    {address && address.map(add => {
                                      return (
                                      add.default &&
                                        <p className="font-size-15 text-truncate">
                                          {add.street}
                                        </p>
                                      )}
                                      )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <button className="col-sm mx-3 btn btn-primary waves-effect waves-light btn-sm" loading={addressLoading} onClick={() => setOnEdit(true)}>
                        Update Address
                        </button>
                        <button className="col-sm mx-3 btn btn-primary waves-effect waves-light btn-sm" primary loading={detailsLoading} onClick={() => setPhoneDetails(true)}>
                          Update Phone number
                        </button>
                        <button className="col-sm mx-3 btn btn-primary waves-effect waves-light btn-sm" primary loading={detailsLoading} onClick={() => setEmailDetails(true)}>
                          Update Email
                        </button>
                      </div>
                    </div>
                  <div className="col-xl-6">
                    <div className="card">
                      <div className="card-body">
                        <h4 className="card-title mb-4">Customer activities</h4>

                        <div className="table-responsive">
                          <table className="table table-nowrap mb-0">
                            <tbody>
                              <tr>
                                <th scope="row">Registered date :</th>
                                <td>{moment(customerData.created_at).format("ddd, MMM Do YYYY")}</td>
                              </tr>
                                <tr>
                                  <th scope="row">Life time spend :</th>
                                  <td>{`£${customerData.life_time_spend}`}</td>
                                </tr>
                             
                                {/* <tr>
                                  <th scope="row">Phone :</th>
                                  <td>
                                    {customerData.phone && customerData.phone}
                                    <span
                                      className={`badge 
                                       ${customerData.user.phone_verified && customerData.user.phone_verified
                                          ? "badge-soft-success"
                                          : "badge-soft-danger"
                                        } mx-3`}
                                    >
                                      {customerData.user.phone_verified && customerData.user.phone_verified
                                        ? "Verified"
                                        : "Not verified"
                                      }
                                    </span>
                                  </td>
                                </tr> */}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
              
                  </div>
                }

                         
              {/* <div className="col-md-12">
                <div className="table-responsive">
                  <table className="table align-middle table-nowrap mb-0">
                    <thead className="table-light">
                      <tr>
                      <th className="align-middle">S/N</th>
                        <th className="align-middle">Date</th>
                        <th className="align-middle">Product</th>
                        <th className="align-middle">Price</th>
                        <th className="align-middle">Quantity</th>
                        <th className="align-middle">View</th>
                      </tr>
                    </thead>
                    <tbody>
                      {orderData.orders &&
                        orderData.orders.map((order, index) => (
                          <tr key={index}>
                            <td>
                              <a
                                href="javascript: void(0);"
                                className="text-body fw-bold"
                              >
                                {index + 1}
                              </a>
                            </td>
                            <td>
                              {moment(order.created_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                            <td>{order.title}</td>
                            <td>{`£${order.price}`}</td>
                            <td>{order.quantity}</td>
                            <td>
                              <Button
                          type="primary"
                          onClick={() => {
                            Router.push(`/orders/product/${order._id}`);
                          }}
                        >
                          View Product
                        </Button>
                          </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
                
              
              </div> */}

              {/* Change Customer Address */}
                <Modal
                  title="Update Customer Address"
                  centered
                  visible={onEdit}
                  onCancel={() => setOnEdit(false)}
                  width={500}
                  footer={null}
                >
                  <Form
                    form={form}
                    size="large"
                    name="address"
                    onFinish={onEditAddress}
                    layout="vertical"
                  >
                    <Form.Item name="address"
                      label="New Address"
                      // rules={[{ required: true, message: 'Please select an address' }]}
                      >
                      <Input
                        className="inputWidthFull"
                        label="Change address"
                        placeholder="Enter address"
                        required
                      />
                    </Form.Item>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      // onClick={() => setOnEdit(false)}
                      >
                        Update Address
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => setOnEdit(false)}
                      >
                        Cancel
                      </Button>
                    </Form.Item>
                  </Form>
                </Modal>


              {/* Update Customer Phone number */}
              <Modal
                  title="Update Customer Phone number"
                  centered
                  visible={phoneDetails}
                  onCancel={() => setPhoneDetails(false)}
                  width={500}
                  footer={null}
                >
                  <Form
                    form={form}
                    size="large"
                    name="phone_num"
                    onFinish={updateCustomerPhone}
                    layout="vertical"
                  >
                    <Form.Item name="phone"
                      label="change Phone number"
                      rules={[{ required: true, message: 'Please enter a phone number' }]}
                      >
                      <Input
                        className="inputWidthFull"
                        label="Phone number"
                        placeholder="Enter phone number"
                        required
                      />
                    </Form.Item>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      // onClick={() => setOnEdit(false)}
                      >
                        Update Phone number
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => setPhoneDetails(false)}
                      >
                        Cancel
                      </Button>
                    </Form.Item>
                  </Form>
                </Modal>

                {/* Update Customer Email */}
              <Modal
                  title="Update Customer Email"
                  centered
                  visible={emailDetails}
                  onCancel={() => setEmailDetails(false)}
                  width={500}
                  footer={null}
                >
                  <Form
                    form={form}
                    size="large"
                    name="customer_data"
                    onFinish={updateCustomerEmail}
                    layout="vertical"
                  >
                    <Form.Item name="email"
                      label="Change Email"
                      rules={[{ required: true, message: 'Please enter an email' }]}
                      >
                      <Input
                        className="inputWidthFull"
                        label="Email"
                        placeholder="Enter email"
                        required
                        type="email"
                      />
                    </Form.Item>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      // onClick={() => setOnEdit(false)}
                      >
                        Update Email
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => setEmailDetails(false)}
                      >
                        Cancel
                      </Button>
                    </Form.Item>
                  </Form>
                </Modal>


            </div>
          )}
        </div>
      </DashboardLayout>

      

     

    </>
  );
};

Customer.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default Customer;
