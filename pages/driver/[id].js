import { useState, useEffect } from "react";
import DashboardLayout from "../../layouts/DashboardLayout";
import Head from "../../components/Head";
import Link from "next/link";
import { Modal, Button, Input, Form, Select, Avatar, TimePicker, Table } from "antd";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
  getDriverOrders,
  getSingleDriver,
   UpdateDriver,

} from "../../helper/redux/actions/app";
import { ExclamationCircleOutlined } from "@ant-design/icons";
const SingleCategory = ({ id }) => {
  const [loading, setLoading] = useState(true);
  const [deleteLoading, setDeleteLoading] = useState(false);
  const [driverStatus, setDriverStatus] = useState(false);
  const [editDriver, setEditDriver] = useState(false);
  const [driver, setDriverData] = useState({});
  const [driverOrders, setDriverOrders] = useState([]);
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);
  const [perPage] = useState(10);

  
  const [ form ] = Form.useForm();
  const { Option } = Select;
  const { TextArea } = Input;
  const { confirm } = Modal;
  const { RangePicker } = TimePicker;
  
  const format = "HH:mm";

  useAsync(async () =>{
    await getDriver()
    await getOrders()
   }, [])

  
  const getDriver = async () =>{
    try {
      setLoading(true);
      await dispatch(await getSingleDriver(id))
        .then((response) => {
          setDriverData(response.data);
          console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get Driver Info, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  } 

  const getOrders = async () =>{
    try {
      setLoading(true);
      await dispatch(await getDriverOrders(id))
        .then((response) => {
          setDriverOrders(response.data);
          console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get Driver Orders, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  } 

  const EditDriver = async (value, timeString) => {
    try {

let driveTime = [
  {"day": "monday", "opens_at" : value.monday[0], "closes_at": value.monday[1]}, 
  {"day": "tuesday", "opens_at" : value.tuesday[0], "closes_at": value.tuesday[1]}, 
  {"day": "wednesday", "opens_at" : value.wednesday[0], "closes_at": value.wednesday[1]}, 
  {"day": "thursday", "opens_at" : value.thursday[0], "closes_at": value.thursday[1]}, 
  {"day": "friday", "opens_at" : value.friday[0], "closes_at": value.friday[1]}, 
  {"day": "saturday", "opens_at" : value.saturday[0], "closes_at": value.saturday[1]},
  {"day": "sunday", "opens_at" : value.sunday[0], "closes_at": value.sunday[1]}]
  // console.log(timeString.monday)
  setLoading(true);

  const payload = {
    data: {
      name: value.name,
      phone: value.phone,
      email: value.email,
      availability: driveTime
    }
  };
  await dispatch(await UpdateDriver({ payload }))
    .then((response) => {
      setEditDriver(false)
      getDriver()
      console.log("Update Success")
      notification.success({
        message: "Driver",
        description: "Driver updated successfully",
      });
      onReset()
    })
    .catch((error) => { });
} finally {
  setLoading(false);
}
};


const orders = [
  {
      key: 2,
      title: 'Status',
      dataIndex: 'status'
  },
  {
    key: 3,
    title: 'Message',
    dataIndex: 'message',
    // render:  (type)=> (
  },
  {
    key: 4,
    title: 'Order details',
    dataIndex: 'order_id',
    render:  (type)=> (
      <Button onClick={()=> Router.push(`/orders/${type}`)}>
        View
      </Button>
    )
  }
];
   
 


 

    // Delete Coupon
    const confirmDelete = () => {
      confirm({
        title: "Confirm delete",
        icon: <ExclamationCircleOutlined />,
        content: "Are you sure to delete this coupon?",
        async onOk() {
          try {
            setDeleteLoading(true);
  
            await dispatch(await deleteCoupon(couponId))
              .then((response) => {
                Router.push(`/coupon`);
                  setDeleteLoading(false);
                notification.success({
                  message: "Coupon",
                  description: "Coupon deleted successfully",
                });
              })
              .catch((error) => {
                setDeleteLoading(false);
              });
          } finally {
            setLoading(false);
          }
        },
        onCancel() { },
      });
    };

  return (
    <>
      <Head title={"Driver"} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Dashboard" page="Driver" />

          <div className="row">
            <div className="col-xl-4">
              <div className="card overflow-hidden">
                <div className="bg-primary bg-soft">
                  <div className="row">
                    <div className="col-7"></div>
                    <div className="col-5 align-self-end">
                      <img
                        src="/images/profile-img.png"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-sm-3">
                      <div className="avatar-md profile-user-wid mb-4">
                        <img
                          src={
                            driver?.picture ||
                            `https://ui-avatars.com/api/?name=${driver.name}&background=3c29b3&color=fff&size=128`
                          }
                          alt={driver.name}
                          className="img-thumbnail rounded-circle"
                        />
                      </div>
                    </div>

                    <div className="col-sm-7">
                      <div className="pt-4">
                        <div className="row">
                          <h5 className="font-size-15 text-truncate">
                            {driver && driver.name}
                          </h5>
                          <p>
                            <a
                              target="_blank"
                              href={`mailto:${driver.email}`}
                              rel="noreferrer"
                            >
                              {driver.email}
                            </a>
                          </p>
                        </div>
                        <div className="mt-2">
                            <p className="btn btn-secondary waves-effect waves-light btn-sm" onClick={()=> setEditDriver(true)}>
                              Update Information
                            </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* Persona Information */}
              <div className="card">
                <div className="card-body">
                  <h4 className="card-title mb-4">Personal Information</h4>

                  <div className="table-responsive">
                    <table className="table table-nowrap mb-0">
                      <tbody>
                        <tr>
                          <th scope="row">Full Name :</th>
                          <td>{driver.name}</td>
                        </tr>
                        <tr>
                          <th scope="row">E-mail :</th>
                          <td>{driver.email}</td>
                        </tr>
                        <tr>
                          <th scope="row">Status :</th>
                          <td>
                            <span className="text-capitalize">
                              {driver.status}
                            </span>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-8">
                 <Table
               loading={loading}
               columns={orders}
               dataSource={driverOrders}
               pagination={{
                 current: page,
                 pageSize: pageSize,
                 total: driverOrders.length,
                 onChange: (page, pageSize)=>{
                   setPage(page);
                   setPageSize(pageSize);
                 }
               }
               }
               /> 
            </div>
          </div>
        </div>
        <Modal
          title="Update status"
          centered
          visible={editDriver}
          width={500}
          onCancel={() => setEditDriver(false)}
          footer={null}
        >
           <Form
                  form={form}
                  name="edit_driver"
                  onFinish={EditDriver}
                  // onFinish={checkForm}
                  // layout="vertical"
                >
                  <Form.Item name="name" label="Name">
                    <Input
                      placeholder={driver.name}
                      className="inputWidthFull"
                    />
                  </Form.Item>
                  <Form.Item name="email" label="Email">
                    <Input
                      placeholder={driver.email}
                      className="inputWidthFull"
                      label="Driver email"
                    />
                  </Form.Item>
                  <Form.Item name="phone" label="Phone number">
                    <Input
                      placeholder={driver.phone}
                      className="inputWidthFull"
                      label="Driver phone number"
                    />
                  </Form.Item>
                  
                  <Form.Item label="Monday" name='monday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                      // placeholder={driver?.availability.monday.closes_at}
                              use24Hours
                              format={format}
                              // onChange={checkForm}

                                />
                  </Form.Item>
                  <Form.Item label="Tuesday" name='tuesday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Wednesday" name='wednesday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Thursday" name='thursday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Friday" name='friday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Saturday" name='saturday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Sunday" name='sunday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  

                  <Form.Item>
                    <Button
                      loading={loading}
                      htmlType="submit"
                      className="mt-2"
                      type="primary"
                    >
                      Update
                    </Button>
                    <Button
                      className="mt-2 mx-3"
                      type="primary"
                      onClick={() => {
                        setEditDriver(false)
                        onReset()
                      }}
                    >
                      Cancel
                    </Button>
                  </Form.Item >
                </Form>
        </Modal>
      </DashboardLayout>
    </>
  );
};

SingleCategory.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default SingleCategory;
