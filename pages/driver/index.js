import { useState, useEffect } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import ReactPaginate from 'react-paginate';
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  getDrivers,
  AddDriver,
  UpdateDriver,
  deleteDriver
} from "../../helper/redux/actions/app";
import {
  ExclamationCircleOutlined,
  UploadOutlined, CloseCircleOutlined
} from "@ant-design/icons";
import { useAsync } from "react-use";
import Router from "next/router";
import {
  notification, Button, Form, Badge,Select, Avatar, Input, InputNumber,
  Table, Switch, Spin, Modal, Tooltip, DatePicker, TimePicker
} from "antd";
import { render } from "react-dom";




export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  // const [deleteLoading, setDeleteLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);
  const [addDriver, setAddDriver] = useState(false);
  const [isEditing, setIsEditing] = useState(false)
  const [Drivers, setDrivers] = useState([]);
  const [offset, setOffset] = useState(0);
  const [perPage] = useState(10);
  const [pageCount, setPageCount] = useState(0)
  const [driverName, setDriverName] = useState('');
  const [description, setDescription] = useState('');
  const [duration, setDuration] = useState('');
  const [pick, setPicker] = useState([]);
  const [editDriver, setEditDriver] = useState(null)

  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);




  const { confirm } = Modal;
  const [form] = Form.useForm();
  const { Option } = Select;
  const { TextArea } = Input;
  const { RangePicker } = TimePicker;


  const format = "HH:mm";
  const daysOfTheWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

  // DriverColumn
  const driverColumns = [
    {
      key: 1,
      title: 'Store Name',
      dataIndex: 'name'
    },
    {
      key: 3,
      title: 'Phone',
      dataIndex: 'phone'
    },
    {
      key: 4,
      title: 'Email',
      dataIndex: 'email',
    },
    {
      key: 4,
      title: 'Status',
      dataIndex: 'status',
    },
    {
      title: "Edit driver",
      key: 5,
      dataSource: "id",
      render: (record) => (
        <Button type="primary" onClick={() => Router.push(`/driver/${record.id}`)}>
          Edit driver          
        </Button>
      )
    },
    {
      title: "Delete",
      key: 5,
      dataSource: "id",
      render: (record) => (
        <Tooltip title="Remove driver">
        <Button type="danger" onClick={() => confirmDelete(record.id)}
        icon={<CloseCircleOutlined />}>
          Delete         
        </Button>
        </Tooltip>
      )
    }
  ];
  // Same Function with getAllStoreData()
  const getAllDrivers = async () => {
    try {
      setLoading(true);
      await dispatch(await getDrivers())
        .then((response) => {
          const data = (response.data);
          console.log(data)
          setDrivers(response.data)
        setLoading(false);


          // const slice = data.slice(offset, offset + perPage)
          // const postData = data.map(((driver) => (
          //   <tr key={driver.id}>

              
          //     <td className="text-capitalize">
          //       {driver.name}
          //     </td>
          //     <td className="text-capitalize">
          //       {driver.phone}
          //     </td>
          //     <td className="text-capitalize">
          //       {driver.email}
          //     </td>
          //     <td className="text-capitalize">
          //       {driver.status}
          //     </td>
              

          //     {/* Edit */}
          //     <td>
          //       <Button
          //         type="primary"
          //         onClick={() => {
          //         //  EditDriver(driver)
          //          Router.push(`/driver/${driver.id}`)
          //         }}
          //       >
          //         Edit driver
          //       </Button>
          //     </td>
          //     {/* Delete */}
          //     <td>
          //       <Tooltip title="Remove privilege">
          //         <Button
          //           onClick={() => {
          //             confirmDelete(driver.id);
          //           }}
          //           type="danger"
          //           icon={<CloseCircleOutlined />}>
          //           Delete
          //         </Button>

          //       </Tooltip>


          //     </td>
          //   </tr>
          // )))
          // setDrivers(postData)
          // setPageCount(Math.ceil(data.length / perPage))
          // console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all Drivers, try again",
          });
          // console.log(Drivers)
        });
    } finally {
      setLoading(false);
    }
  }
  useEffect(async () => {
    await getAllDrivers()
  }, [offset])



  // Add product to a store
  const AddNewDriver = async (value, timeString) => {
        try {
          // const [monday, tuesday, wednesday, thursday, friday, saturday,sunday] = value

    // let driveTime = [
    //   {"day": "monday", "opens_at" : value.monday[0], "closes_at": value.monday[1]}, 
    //   {"day": "tuesday", "opens_at" : value.tuesday[0], "closes_at": value.tuesday[1]}, 
    //   {"day": "wednesday", "opens_at" : value.wednesday[0], "closes_at": value.wednesday[1]}, 
    //   {"day": "thursday", "opens_at" : value.thursday[0], "closes_at": value.thursday[1]}, 
    //   {"day": "friday", "opens_at" : value.friday[0], "closes_at": value.friday[1]}, 
    //   {"day": "saturday", "opens_at" : value.saturday[0], "closes_at": value.saturday[1]},
    //   {"day": "sunday", "opens_at" : value.sunday[0], "closes_at": value.sunday[1]}]


      let driveTime = [
        {"day": "monday", "opens_at" : value.time[0], "closes_at": value.time[1]},
        {"day": "tuesday", "opens_at" : value.time[0], "closes_at": value.time[1]},
        {"day": "wednesday", "opens_at" : value.time[0], "closes_at": value.time[1]},
        {"day": "thursday", "opens_at" : value.time[0], "closes_at": value.time[1]}, 
        {"day": "friday", "opens_at" : value.time[0], "closes_at": value.time[1]}, 
        {"day": "saturday", "opens_at" : value.time[0], "closes_at": value.time[1]},
        {"day": "sunday", "opens_at" : value.time[0], "closes_at": value.time[1]}]
      // console.log(timeString.monday)
      setLoading(true);
 
      const payload = {
        data: {
          name: value.name,
          phone: value.phone,
          email: value.email,
          availability: driveTime
        }
      };
      await dispatch(await AddDriver({ payload }))
        .then((response) => {
          setAddDriver(false)
          getAllDrivers()
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
          getAllDrivers()
          // setAddDriver(false)
          onReset()
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  const checkForm = (value) => {
    console.log(value.monday)
    {moment(value).format(format)}
    setPicker([value.monday, value.tuesday, value.wednesday, value.thursday, value.friday, value.saturday,value.sunday])
    console.log(pick)
    // console.log(pick)


  }
  const handleChange = (event) => {
    setPicker([...pick, event.target.value])
    console.log(pick);
    console.log(event);
  };
  const  onChange = (time, timeStrin) => {
    console.log(time, timeStrin);
  }

  const EditDriver = (driver) => {
    setEditDriver({...driver})
    setIsEditing(true)
  };

  const updateDriver = async (value) => {
        try {
          let driveTime = [
            {"day": "monday", "opens_at" : value.monday ? value.monday[0] : editDriver?.availability.monday.opens_at , "closes_at": value.monday ? value.monday[1] : editDriver?.availability.monday.closes_at}, 
            {"day": "tuesday", "opens_at" : value.tuesday ? value.tuesday[0] : editDriver?.availability.tuesday.opens_at, "closes_at": value.tuesday ? value.tuesday[1] : editDriver?.availability.mtuesay.closes_at}, 
            {"day": "wednesday", "opens_at" : value.wednesday ? value.wednesday[0] : editDriver?.availability.wednesday.opens_at, "closes_at": value.wednesday ? value.wednesday[1] : editDriver?.availability.wednesday.closes_at}, 
            {"day": "thursday", "opens_at" : value.thursday ? value.thursday[0] : editDriver?.availability.thursday.opens_at, "closes_at": value.thursday ? value.thursday[1] : editDriver?.availability.thursday.closes_at}, 
            {"day": "friday", "opens_at" : value.friday ? value.friday[0] : editDriver?.availability.friday.opens_at, "closes_at": value.friday ? value.friday[1] : editDriver?.availability.friday.closes_at}, 
            {"day": "saturday", "opens_at" : value.saturday ? value.saturday[0] : editDriver?.availability.saturday.opens_at, "closes_at": value.saturday ? value.saturday[1] : editDriver?.availability.saturday.closes_at},
            {"day": "sunday", "opens_at" : value.sunday ? value.sunday[0] : editDriver?.availability.sunday.opens_at, "closes_at": value.sunday ? value.sunday[1] : editDriver?.availability.sunday.closes_at}]
          setLoading(true);
          const payload = {
            driver_id: editDriver?.id,
            data: {
              name: value.name,
              phone: value.phone,
              email: value.email,
              availability: driveTime
            }
          };
          await dispatch(await UpdateDriver({ payload }))
            .then((response) => {
              console.log("Update Success")
              console.log(driveTime)
              notification.success({
                message: "Driver",
                description: "Driver updated successfully",
              });
              getAllDrivers()
              setPageLoading(false);
            })
            .catch((error) => {
              setPageLoading(false);
            });
        } finally {
          setLoading(false);
        }
      }
    // ,  onCancel() { },
    // });
  // };

  const confirmDelete = (id) => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to delete this driver?",
      async onOk() {
        try {
          setPageLoading(true);
          const payload = {
            driver_id: id
          };

          await dispatch(await deleteDriver({ payload }))
            .then((response) => {
              setPageLoading(false);
              notification.success({
                message: "Delete Category",
                description: "Category deleted successfully",
              });
              getAllDrivers();
            })
            .catch((error) => {
              setPageLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() { },
    });
  };

  const statusChange = async (id, status) => {

    try {
      setPageLoading(true);
      const payload = {
        id: id,
        data: {
          status: status ? "enabled" : "disabled"
        }
      };
      console.log(id, status)
      await dispatch(await UpdateDriverStatus({ payload }))
        .then((response) => {
          notification.success({
            message: "Category Status",
            description: "Status updated successfully",
          });
          getAllDrivers();
          setPageLoading(false);
          console.log(response)
        })
        .catch((error) => {
          setPageLoading(false);
        });
    } finally {
      setLoading(false);
    }
  };

  const handlePageClick = (e) => {
    const selectedPage = e.selected;
    setOffset(selectedPage + 1)
  };

  const addDriverImage = (e) => {
    console.log('Upload event:', e);

    if (Array.isArray(e)) {
      return e;
    }

    return e?.fileList;
  };

  const onReset = () => {
    form.resetFields();
  };



  return (
    <>
      <Head title="Driver" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Drivers" page="Drivers">
            <Button type="primary" onClick={() => setAddDriver(true)}>
              Add Driver </Button>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
              {/* <table className="table align-middle table-nowrap mb-0">
                <thead className="table-light">
                  <tr>
                    <th className="align-middle">Name</th>
                    <th className="align-middle">Phone number</th>
                    <th className="align-middle">Email</th>
                    <th className="align-middle">Status</th>
                    <th className="align-middle">Edit</th>
                    <th className="align-middle">Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {Drivers}
                </tbody>
              </table>
              <ReactPaginate
                previousLabel={"prev"}
                nextLabel={"next"}
                breakLabel={"..."}
                breakClassName={"break-me"}
                pageCount={pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={7}
                onPageChange={handlePageClick}
                containerClassName={"pagination"}
                subContainerClassName={"pages pagination"}
                activeClassName={"active"} /> */}


<Table
            loading={loading}
            columns={driverColumns}
            dataSource={Drivers}
            pagination={{
              current: page,
              pageSize: pageSize,
              total: Drivers.length,
              onChange: (page, pageSize)=>{
                setPage(page);
                setPageSize(pageSize);
              }
            }
            }
            /> 
              {/* Add Categry */}
              <Modal
                title="Add Driver"
                centered
                visible={addDriver}
                width={500}
                onCancel={() => {
                  setAddDriver(false)
                  onReset()
                }}
                footer={null}
              >
                <Form
                  form={form}
                  name="add_driver"
                  onFinish={AddNewDriver}
                  // onFinish={checkForm}
                  // layout="vertical"
                >
                  <Form.Item name="name" label="Driver name">
                    <Input
                      placeholder="Driver name"
                      className="inputWidthFull"
                      label="Driver name"
                    />
                  </Form.Item>
                  <Form.Item name="email" label="Driver email">
                    <Input
                      placeholder="Driver email"
                      className="inputWidthFull"
                      label="Driver email"
                    />
                  </Form.Item>
                  <Form.Item name="phone" label="Driver phone number">
                    <Input
                      placeholder="Driver phone number"
                      className="inputWidthFull"
                      label="Driver phone number"
                    />
                  </Form.Item>
                  
                  {/* <Form.Item label="Monday" name='monday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                              // onChange={checkForm}

                                />
                  </Form.Item>
                  <Form.Item label="Tuesday" name='tuesday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Wednesday" name='wednesday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Thursday" name='thursday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Friday" name='friday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Saturday" name='saturday'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item> */}

                  <Form.Item label="Working hours" name='time'
                             rules={[{ required: true, message: 'Pick a time range' }]}
                             >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  

                  <Form.Item>
                    <Button
                      loading={loading}
                      htmlType="submit"
                      className="mt-2"
                      type="primary"
                    >
                      Add Driver
                    </Button>
                    <Button
                      className="mt-2 mx-3"
                      type="primary"
                      onClick={() => {
                        setAddDriver(false)
                        onReset()
                      }}
                    >
                      Cancel
                    </Button>
                  </Form.Item >
                </Form>
              </Modal>

              {/* Edit Driver */}
              <Modal
                title="Edit Driver"
                centered
                visible={isEditing}
                width={500}
                onCancel={() => {
                  setIsEditing(false)
                  onReset()
                }}
                footer={null}
              >
                <Form
                  form={form}
                  name="edit_driver"
                  onFinish={updateDriver}
                  // onFinish={checkForm}
                  // layout="vertical"
                >
                  <Form.Item name="name" label="Driver name">
                    <Input
                      placeholder={editDriver?.name}
                      className="inputWidthFull"
                      label="Driver name"
                    />
                  </Form.Item>
                  <Form.Item name="email" label="Driver email">
                    <Input
                      placeholder={editDriver?.email}
                      className="inputWidthFull"
                      label="Driver email"
                    />
                  </Form.Item>
                  <Form.Item name="phone" label="Driver phone number">
                    <Input
                      // placeholder={editDriver?.phone}
                      placeholder={editDriver?.availability.monday.closes_at}

                      
                      className="inputWidthFull"
                      label="Driver phone number"
                    />
                  </Form.Item>
                  
                  <Form.Item label="Monday" name='monday' 
                      >
                              <RangePicker
                              use24Hours
                              format={format}
                              // onChange={checkForm}


                                />
                  </Form.Item>
                  <Form.Item label="Tuesday" name='tuesday' >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Wednesday" name='wednesday' >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Thursday" name='thursday' >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Friday" name='friday' >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Saturday" name='saturday' >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  <Form.Item label="Sunday" name='sunday' >
                              <RangePicker
                              use24Hours
                              format={format}
                                />
                  </Form.Item>
                  

                  <Form.Item>
                    <Button
                      loading={loading}
                      htmlType="submit"
                      className="mt-2"
                      type="primary"
                    >
                      Add Driver
                    </Button>
                    <Button
                      className="mt-2 mx-3"
                      type="primary"
                      onClick={() => {
                        setAddDriver(false)
                        onReset()
                      }}
                    >
                      Cancel
                    </Button>
                  </Form.Item >
                </Form>
              </Modal>




            
            </Spin>
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
