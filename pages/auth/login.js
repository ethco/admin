import { useState, useEffect } from "react";
import Head from "../../components/Head";
import { useDispatch } from "react-redux";
import { Form, Input, Button, notification } from "antd";
import Link from "next/link";
import AuthStorage from "../../helper/utils/auth-storage";
import Router from "next/router";
import { actionLogin, getUser } from "../../helper/redux/actions/auth";

const Login = (props) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const getUserProfile = async (token) => {
    try {
      setLoading(true);
      await dispatch(await getUser(token))
        .then((response) => {
          AuthStorage.value = {
            token,
            user: response.data,
          };
          notification.success({
            message: "Login",
            description: "You are successfully logged in",
          });
          Router.push("/");
        })
        .catch((error) => {});
    } finally {
      setLoading(false);
    }
  };
  const submitForm = async (values) => {
    try {
      setLoading(true);
      await dispatch(await actionLogin({ values }))
        .then((response) => {
         
          const { token,user } = response;
          console.log(token)
          console.log(user)
          AuthStorage.value = {
            token,
            user
          };
          console.log(response)
          notification.success({
            message: "Login",
            description: "You are successfully logged in",
          });
          Router.push("/");
          // getUserProfile(response.token);
        })
        .catch((error) => {});
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (AuthStorage.loggedIn) {
      Router.push("/");
    }
  }, []);

  return (
    <>
      <Head title="Login" />
      <div className="account-pages my-5 pt-sm-5">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-6 col-xl-5">
              <div className="card overflow-hidden">
                <div className="bg-primary bg-soft">
                  <div className="row">
                    <div className="col-7">
                      <div className="text-primary p-4">
                        <h5 className="text-primary">Welcome Back !</h5>
                        <p>Sign in to continue to Ethco.</p>
                      </div>
                    </div>
                    <div className="col-5 align-self-end">
                      <img
                        src="/images/profile-img.png"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
                <div className="card-body pt-0">
                  <div className="auth-logo">
                    <div className="avatar-md profile-user-wid mb-4">
                      <span className="avatar-title rounded-circle bg-light">
                        <img
                          src="/images/logo.svg"
                          alt=""
                          className=""
                          height="34"
                        />
                      </span>
                    </div>
                  </div>
                  <div className="p-2">
                    <Form
                      size="large"
                      name="user_login"
                      className="login-form"
                      onFinish={submitForm}
                      layout="vertical"
                    >
                      <Form.Item
                        name="email"
                        label="Email"
                        rules={[
                          {
                            type: "email",
                            message: "The input is not valid E-mail!",
                          },
                          {
                            required: true,
                            message: "Please input your email address!",
                          },
                        ]}
                      >
                        <Input label="Email address" type="email" />
                      </Form.Item>

                      <Form.Item
                        name="password"
                        label="Password"
                        rules={[
                          {
                            required: true,
                            message: "Please input your password!",
                          },
                        ]}
                      >
                        <Input label="Password" type="password" />
                      </Form.Item>

                      <Form.Item>
                        <Button
                          loading={loading}
                          block
                          htmlType="submit"
                          className="mt-2"
                          type="primary"
                        >
                          Login
                        </Button>
                      </Form.Item>

                      <div className="mt-4 text-center">
                        <Link href={`/auth/forgot-password`}>
                          <a className="text-muted">
                            <i className="mdi mdi-lock me-1"></i> Forgot your
                            password?
                          </a>
                        </Link>
                      </div>
                    </Form>
                  </div>
                </div>
              </div>
              <div className="mt-5 text-center">
                <div>
                  <p>
                    ©<script>document.write(new Date().getFullYear());</script>
                    Ethco
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Login;
