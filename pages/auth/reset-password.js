import Head from "../../components/Head";

import Link from "next/link";
import { Form, Button, Input } from "antd";

const Login = (props) => {
  const submitForm = async (values) => {};
  return (
    <>
      <Head title="Reset Password" />
      <div className="account-pages my-5 pt-sm-5">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-6 col-xl-5">
              <div className="card overflow-hidden">
                <div className="bg-primary bg-soft">
                  <div className="row">
                    <div className="col-7">
                      <div className="text-primary p-4">
                        <h5 className="text-primary">Reset Password</h5>
                        <p>Reset Password with Ethco</p>
                      </div>
                    </div>
                    <div className="col-5 align-self-end">
                      <img
                        src="assets/images/profile-img.png"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
                <div className="card-body pt-0">
                  <div>
                    <div className="avatar-md profile-user-wid mb-4">
                      <span className="avatar-title rounded-circle bg-light">
                        <img
                          src="/images/logo.svg"
                          alt=""
                          className=""
                          height="34"
                        />
                      </span>
                    </div>
                  </div>

                  <div className="p-2">
                    <Form
                      size="large"
                      name="user_login"
                      className="login-form"
                      onFinish={submitForm}
                      layout="vertical"
                    >
                      <Form.Item
                        name="password"
                        label="New Password"
                        rules={[
                          {
                            required: true,
                            message: "Please input your password!",
                          },
                        ]}
                      >
                        <Input.Password label="New Password" type="password" />
                      </Form.Item>

                      <Form.Item>
                        <Button
                          block
                          htmlType="submit"
                          className="mt-2"
                          type="primary"
                        >
                          Reset Password
                        </Button>
                      </Form.Item>
                    </Form>
                  </div>
                </div>
              </div>
              <div className="mt-5 text-center">
                <p>
                  Remember It ?{" "}
                  <Link href="/auth/login">
                    <a className="fw-medium text-primary">Sign In here</a>
                  </Link>
                </p>
                <p>
                  ©<script>document.write(new Date().getFullYear());</script>
                  Ethco
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Login;
