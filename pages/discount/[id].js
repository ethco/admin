import { useState, useEffect } from "react";
import DashboardLayout from "../../layouts/DashboardLayout";
import Head from "../../components/Head";
import Link from "next/link";
import { Modal, Button, Input, Form, InputNumber, Select, Spin, Checkbox } from "antd";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
  getDiscounts,
  AddDiscount,
  getProducts,
  getCategories,
  deleteDiscount,
  getDiscountProducts,
  updateDiscount,
  removeProductDiscount,
} from "../../helper/redux/actions/app";
import { ExclamationCircleOutlined, CloseCircleOutlined } from "@ant-design/icons";
const SingleCoupon = ({ id }) => {
  const [loading, setLoading] = useState(true);
  const [pageLoading, setPageLoading] = useState(false);
  const [discounts, setDiscounts] = useState([]);
  const [discountProducts, setDiscountProducts] = useState([]);
  const [discountProductID, setDiscountProductID] = useState('');
  const [addNewDiscount, setAddNewDiscount] = useState(false);
  const [all, setAll] = useState(false);
  const [editCoupon, setEditCoupon] = useState(false);
  const [products, setProducts] = useState([]);
  const [categoryID, setCategoryID] = useState(null);
  // const [categoryList, setCategoryList] = useState([]);
  
  const [isView, setIsView] = useState(false)
  const [isEdit, setIsEdit] = useState(false)
  const [isProducts, setIsProducts] = useState(false)
  const [viewDiscount, setViewDiscount] = useState({})
  const dispatch = useDispatch();

  
  const [ form ] = Form.useForm();
  const { Option } = Select;
  const { confirm } = Modal;


  const onReset = () => {
    form.resetFields();
  };

  const checkForm = (value) => {
    console.log(value)
    console.log(all)
  }


  // View Discount

  const ViewDiscounts = (discount) => {
    setViewDiscount({...discount})
    setIsView(true)
  };
// Get All Discounts
useAsync(async () =>{
  await getAllDiscounts()
  await getAllStoreData()
},[])



// Get Store Information
const getAllStoreData = async () => {
  try {
    setLoading(true);
    await dispatch(await getAllPartnerStoreByName(id))
      .then((response) => {
        setLoader(true)
        console.log(response.data)
        setStoreData(response.data);
        setCategoryID(response.data.category)
      })
      .catch((error) => {
        // notification.error({
        //   message: "Oops!",
        //   description: "Unable to get partner details, try again",
        // });
      });
  } finally {
    setLoading(false);
  }
}
  const getAllDiscounts = async() =>{
    try {
      setLoading(true);
      await dispatch(await getDiscounts(id))
        .then((response) => {
          setDiscounts(response.data);
          console.log(response.data.id)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "please try again",
          });
        });
    } finally {
      setLoading(false);
    }
    }

// Get Discount Products
const getDiscountProds = async(id) =>{
  console.log(id)
  try {
    setLoading(true);
    await dispatch(await getDiscountProducts(id))
      .then((response) => {
        setDiscountProducts(response.data);
        console.log(response.data)
      })
      .catch((error) => {
        // notification.error({
        //   message: "Oops!",
        //   description: "please try again",
        // });
      });
  } finally {
    setLoading(false);
  }
  }
//  Show Discount Products
  // {useEffect(async() =>{
  //   await getDiscountProds(viewDiscount?.id)
  // },[isView])}
    

    // Add Discount Code
  const AddDiscountCode = async (value) => {
    try {
      setLoading(true);
   
      const payload = {
        store_id: id,
        data: {
          discount: {
            offer: value.offer,
            type: value.type,
            min_amount: value.min_amount,
            status: value.status,
          },
          products: value.products,
          categories: categoryID,
          all_products: all
        }
      };
      await dispatch(await AddDiscount({ payload }))
        .then((response) => {
          getAllDiscounts()
          onReset()
          setAddNewDiscount(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  // Get all products
  useAsync(async () =>{
  try {
    await dispatch(await getProducts(id))
      .then((response) => {
        setLoading(false);
        setProducts(response.data);
      })
      .catch((error) => {
        setLoading(false);
        // notification.error({
        //   message: "Oops!",
        //   description: "Unable to send response, try again",
        // });
      });
  } finally {
    setLoading(false);
  }
}, [])

  // Get all categories
useAsync(async () => {
  try {
    setLoading(true);
    await dispatch(await getCategories())
      .then((response) => {
        setCategoryList(response.data);
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to send response, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}, [])

   // Add Discount
  const EditDiscount = async (value) => {
    try {
      setLoading(true);
   
      const payload = {
        discount_id: viewDiscount.id,
        data: {
            offer: value.offer ? value.offer : viewDiscount.offer,
            type: value.type ? value.type : viewDiscount.type,
            min_amount: value.min_amount ? value.min_amount : viewDiscount.min_amount,
            status: value.status ? value.status : viewDiscount.status,
        }
      };
      await dispatch(await updateDiscount({ payload }))
        .then((response) => {
          getAllDiscounts()
          onReset()
          setIsEdit(false)
          console.log("Update Success")
          notification.success({
            message: "Discount",
            description: "Discount updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

    // Delete Discount
 const confirmDelete = (id) => {
  confirm({
    title: "Confirm delete",
    icon: <ExclamationCircleOutlined />,
    content: "Are you sure to delete this discount?",
    async onOk() {
      try {
        setPageLoading(true);
        const payload = { 
          id: id
         };

        await dispatch(await deleteDiscount({ payload }))
          .then((response) => {
            setPageLoading(false);
            getAllDiscounts();
            notification.success({
              message: "Delete Category",
              description: "Category deleted successfully",
            });
          })
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
    },
    onCancel() {},
  });
};


// Delete Products from Discount
const removeProduct = (index) => {
  const newDiscountProducts = discountProducts.filter((_, i) => i !== index);
  setDiscountProducts(newDiscountProducts)
  console.log(discountProducts)
};
const productFilter = discountProducts.map((productid)=>{
  console.log(productid.id)
  return {product_id:productid.id}
})
const deleteProduct = async () => {
 
      try {
        setPageLoading(true);
        const payload = { 
          store_id: id,
          data: productFilter
          
         }
        await dispatch(await removeProductDiscount({ payload }))
          .then((response) => {
            setPageLoading(false);
            getAllDiscounts();
            notification.success({
              message: "Delete Category",
              description: "Category deleted successfully",
            });
          })
          .catch((error) => {
            setPageLoading(false);
          });
      } finally {
        setLoading(false);
      }
    }

  return (
    <>
      <Head title={"Discounts"} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader  title="Discounts" page="discount">
          <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setAddNewDiscount(true)}
            >
              Add new discount
            </Button>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>


              {/* Add new Discount to store */}
                {addNewDiscount &&
                 <div className="row">
                 <div className="col-md-2"></div>
                 <div className="col-md-8">
                  <Form
                    form={form}
                    name="add_discount"
                    onFinish={AddDiscountCode}
                    // onFinish={checkForm}
                    
                    layout="vertical"
                    style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', gridColumnGap: '24px' }}
                  >

                    <Form.Item name="offer"
                      label="Amount/Percentage" rules={[
                        {
                          required: true,
                          message: "Enter Amount/Percentage!",
                        },
                      ]}>
                      <Input
                      placeholder="Amount/Percentage"
                        className="inputWidthFull"
                        label="Offer"

                      />
                    </Form.Item>
                      <div style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      width:'100%',
                      gap: '16px'
                    }}>
                    <Form.Item name="type" label="Type"
                    // initialValue='percentage' 
                    style={{ width:'100%'}}>
                     <Select
                        placeholder="Type"
                      >
                        <Option value="percentage">Percentage</Option>
                        <Option value="amount">Amount</Option>
                      </Select>
                    </Form.Item>
                    <Form.Item name="applied" label="Applied to"
                    // initialValue='product' 
                    style={{ width:'100%'}}>
                     <Select
                        placeholder="Applied to"
                      >
                        <Option value="product">Product</Option>
                        <Option value="checkout">Checkout</Option>
                      </Select>
                    </Form.Item>
                    </div>


                    <Form.Item name="min_amount" label="Minimum amount"rules={[
                      {
                        required: true,
                        message: "Please enter a minimum amount!",
                      },
                    ]}>
                      <InputNumber
                      placeholder="Minimum amount of order"
                        className="inputWidthFull"
                        label="Minimum amount of order"
                      />
                    </Form.Item>

                    <Form.Item name="status" label="Status"
                    rules={[
                      {
                        required: true,
                        message: "Select a status!",
                      },
                    ]}>
                      <Select
                        placeholder="Status"
                      >
                        <Option value="active">Activate</Option>
                        <Option value="disabled">Deactivate</Option>
                      </Select>
                    </Form.Item>

                    <Form.Item name="products" label="Products"
                    //  rules={[
                    //   {
                    //     required: true,
                    //     message: "Select a products!",
                    //   },
                    // ]}
                    >
                  
                      <Select
                        mode="multiple"
                        placeholder="Select products"
                      >
                        {products && products.map((product, index) => (
                          <Option key={index} value={product.id}>{product.title}</Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item name="all"  
                       >
                <Checkbox
                 onChange={(e) => 
                  setAll(e.target.checked)
                }>
                All Products
            </Checkbox>
                    </Form.Item>

                    

                    <Form.Item  style={{gridColumn: '1/3'}}>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"

                        // onClick={() => {
                        //   // onReset()
                        //   setAddNewDiscount(false)
                        // }}
                      >
                        Add Discount
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => {
                          onReset()
                          setAddNewDiscount(false)
                        }}
                      >
                        Close
                      </Button>
                    </Form.Item >

                  </Form>
                  </div>
                 <div className="col-md-2"></div>
                </div>
}
              <div className="col-md-12">
                <div className="card">
                  <div className="card-body">
                    <div className="table-responsive">
                <table className="table align-middle table-nowrap mb-0">
                  <thead className="table-light">
                    <tr>
                      <th className="align-middle">Date added</th>
                      <th className="align-middle">Applied</th>
                      <th className="align-middle">Offer</th>
                      <th className="align-middle">Minimum amount</th>
                      <th className="align-middle">Status</th>
                      <th className="align-middle">View</th>
                      <th className="align-middle">Products</th>
                      <th className="align-middle">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                  {discounts.map((discount, index) => (
                      <tr key={index}>
                        {/* <td>{index + 1}</td> */}
                        <td>
                          <span className="text-small">
                            {moment(discount.created_at).format("ddd, MMM Do YYYY")}
                          </span>
                        </td>
                        <td className="text-capitalize">{discount.applied_to}</td>
                        <td>{discount.offer}</td>
                        <td>{discount.min_amount}</td>
                        <td className={`text-capitalize badge-pill
                              ${discount.status && discount.status === "active"
                                          ? "badge-soft-success"
                                          : "badge-soft-danger"
                                        } mx-3`}>{discount.status}</td>
                        <td><Button type="primary"
                          primary
                          onClick={() => {
                            
                            ViewDiscounts(discount)}
                          }
                        >
                          Detail
                        </Button></td>
                        <td><Button type="primary"
                          primary
                          onClick={() => {
                            getDiscountProds(discount.id)
                            {discountProducts && setIsProducts(true)}
                          }}
                        >
                          Products
                        </Button></td>
                        <td><Button
                          onClick={() => {
                            confirmDelete(discount.id);
                          }}
                          type="danger"
                          icon={<CloseCircleOutlined />}>
                            Delete
                          </Button></td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
                  </div>
                </div>
              </div>
                </Spin>
          )}
        </div>
     

                 {/* View Discount */}
             <Modal
                title="View Discount"
                centered
                visible={isView}
                width={500}
                onCancel={() => {
                  setIsView(false)
                  onReset()
                }}
                okText='Update discount'

                onOk={() => {
                  setIsEdit(true)
                  setIsView(false)
                }}
                // footer={null}
              >
                <div>
                   {viewDiscount && 
                   <>
                    <p>Created: <span>{moment(viewDiscount?.created_at).format(
                                "ddd, MMM Do YYYY"
                              )}</span></p>
                    <p>Applied to: <span>{viewDiscount?.applied_to}</span></p>
                    <p>Minimum amount: <span>{viewDiscount?.min_amount}</span></p>
                    <p>Offer: <span>{viewDiscount?.offer}</span></p>
                    <p>Type: <span>{viewDiscount?.type}</span></p>
                    <p>Status: <span>{viewDiscount?.status}</span></p>
                    {discountProducts && <>
                      <p>Products:</p>
                      <div>
                    {discountProducts.map((prods, i) => {
                   
                      {prods.id}
                      {prods.title}
                      {prods.id}
                    })}
                    </div>
                    </>
                    }
                    </>
               }
                </div>
              </Modal>


                {/* Edit Discount */}
             <Modal
                title="Edit Discount"
                centered
                visible={isEdit}
                width={500}
                onCancel={() => {
                  setIsEdit(false)
                  onReset()
                }}
                okText='Save'

                onOk={() => {
                  setIsEdit(false)
                  form.submit()
                }}
                // footer={null}
              >
                <Form
                    form={form}
                    name="edit_discount"
                    onFinish={EditDiscount}
                    // onFinish={checkForm}
                    
                    // layout="vertical"
                    // style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', gridColumnGap: '24px' }}
                  >

                    <Form.Item name="offer"
                      label="Offer">
                      <Input
                      placeholder={viewDiscount.offer}
                        className="inputWidthFull"
                        label="Offer"

                      />
                    </Form.Item>
                      
                    <Form.Item name="type" label="Type"
                    // initialValue={viewDiscount.type}
                    >
                     <Select
                        placeholder={viewDiscount.type}
                      >
                        <Option>Type </Option>
                        <Option value="percentage">Percentage</Option>
                        <Option value="amount">Amount</Option>
                      </Select>
                    </Form.Item>

                    <Form.Item name="min_amount" label="Minimum amount"
                      // initialValue={viewDiscount.min_amount}
                      >
                      <InputNumber
                      placeholder={viewDiscount.min_amount}
                        className="inputWidthFull"
                        label="Minimum amount"
                      />
                    </Form.Item>

                    <Form.Item name="status" label="Status"
                    // initialValue={viewDiscount.status}
                    >
                      <Select
                        placeholder={viewDiscount.status}
                      >
                        <Option value="active">Activate</Option>
                        <Option value="disabled">Deactivate</Option>
                      </Select>
                    </Form.Item>

                  </Form>
              </Modal>


                  {/* View Products */}
             <Modal
                title="View Products"
                centered
                visible={isProducts}
                width={500}
                onCancel={() => {
                  setIsProducts(false)
                  onReset()
                }}
                okText='Save'

                onOk={() => {
                  setIsProducts(false)
                  deleteProduct()
                }}
                // footer={null}
              >

<table className="table align-middle table-nowrap mb-0">
                  <thead className="table-light">
                    <tr>
                      <th className="align-middle">Title</th>
                      <th className="align-middle">Remove</th>
                    </tr>
                  </thead>
                  <tbody>
                  {discountProducts.map((prods, index) => (
                      <tr key={index}>
                        <td>{prods.title}</td>
                        <td>
                        <Button
                          onClick={() => {
                            removeProduct(index);
                          }}
                          type="danger"
                          icon={<CloseCircleOutlined />}>
                            Remove
                          </Button>
                           </td>
                      </tr>
                    ))}
                  </tbody>
                </table>

              </Modal>
      </DashboardLayout>
    </>
  );
};

SingleCoupon.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default SingleCoupon;
