import { useEffect, useState } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Randomstring from "randomstring";
import Router from "next/router";
// import SearchSelect from "../../components/SearchSelect";
import Link from "next/link";
import {
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  Select, Modal, Spin, Form, InputNumber, Input, Button, Checkbox
} from "antd";
import {
  getProducts,
  getAllStores,
  getCategories,
  AddDiscount,
  getDiscounts,
  getStoreCategory,
} from "../../helper/redux/actions/app";
import { useDispatch } from "react-redux";
import { useAsync } from "react-use";
import moment from "moment";

export default function Index() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [isTrue, setIsTrue] = useState(true);
   const [productCategory, setProductCategory] = useState("");
   const [productCategoryID, setProductCategoryID] = useState("");
  const [discounts, setDiscounts] = useState([]);
  const [products, setProducts] = useState([]);
  // const [product, setProduct] = useState([]);
  const [storeList, setStoreList] = useState([]);
  const [categoryList, setCategoryList] = useState([]);
  const [categories, setCategories] = useState([]);
  // const [storeID, setStoreID] = useState('');
  const [storeID, setStoreID] = useState([]);
  const [pageLoading, setPageLoading] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [addNewDiscount, setAddNewDiscount] = useState(false);
  const [results, setResults] = useState([]);

  const { confirm } = Modal;
  const { Option } = Select;
  const { TextArea } = Input;
  const [form] = Form.useForm();

  // Same Function with getStoreList()

  const onReset = () => {
    form.resetFields();
  };

  const getAllDiscounts = async () => {
    try {
      setLoading(true);
      await dispatch(await getDiscounts("632365496004eaf624725a96"))
        .then((response) => {
          setLoading(false);
          setDiscounts(response.data);
        })
        .catch((error) => {
          setLoading(false);
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  };




   useAsync(async () =>{
      storeID.map(async (i) => {

    try {
      await dispatch(await getProducts(i))
        .then((response) => {
          setLoading(false);
          setProducts(response.data);
          
        })
        .catch((error) => {
          setLoading(false);
          // notification.error({
          //   message: "Oops!",
          //   description: "Unable to send response, try again",
          // });
        });
    } finally {
      setLoading(false);
    }
  })
  }, [storeID.length])
  // useEffect(()=>{
  //   getProduct()
  // },[storeID])

   useEffect(()=>{
  //   getProduct()
  console.log(storeID)
  console.log(products)
  },[storeID.length])

// Get all stores
  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getAllStores())
        .then((response) => {
          setStoreList(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, [])

  const checkForm = (value) => {
    console.log('code:' + Randomstring.generate(7))
    console.log(value)
  }

 

  // get all categories 
  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getCategories())
        .then((response) => {
          setCategoryList(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, [])

  // const merge = []
  // useEffect(() => {
  //   setResults([...products])
  //    console.log(...products)
  // }, [products])

// Add Discount Code
  const AddDiscountCode = async (value) => {
    try {
      setLoading(true);
   
      const payload = {
        data: {
          discount: {
            code: Randomstring.generate(7),
            offer: value.offer,
            type: value.type,
            min_amount: value.min_amount,
            status: value.status,
          },
          products: value.products,
          categories: value.categories,
          stores: value.stores,
        }
      };
      await dispatch(await AddDiscount({ payload }))
        .then((response) => {
          getAllDiscounts()
          onReset()
          setAddNewDiscount(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  useAsync(async () => {
    await getAllDiscounts()
    await getAllProduct()
  }, []);


  return (
    <>
      <Head title="All Discounts" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Discount" page="discount">
            <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setAddNewDiscount(true)}
            >
              Add new discount
            </Button>
           
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
                         {addNewDiscount &&
                <div className="row">
                  <Form
                    form={form}
                    name="add_discount"
                    onFinish={AddDiscountCode}
                    // onFinish={checkForm}
                    
                    layout="vertical"
                    style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', gridColumnGap: '24px' }}
                  >

                    <Form.Item name="offer"
                      label="Offer"rules={[
                        {
                          required: true,
                          message: "Offer number!",
                        },
                      ]}>
                      <Input
                      placeholder="Number of orders"
                        className="inputWidthFull"
                        label="Offer"

                      />
                    </Form.Item>
                      <div style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      width:'100%',
                      gap: '16px'
                    }}>
                    <Form.Item name="type" label="Type"
                     
                    style={{ width:'100%'}}>
                     <Select
                        placeholder="Type"
                      >
                        <Option>Discount Type</Option>
                        <Option value="percentage">Percentage</Option>
                        <Option value="amount">Amount</Option>
                      </Select>
                    </Form.Item>
                    <Form.Item name="applied" label="Applied to"
                     style={{ width:'100%'}}>
                     <Select
                        placeholder="Applied to"
                      >
                        <Option>When to be applied</Option>
                        <Option value="product">Product</Option>
                        <Option value="checkout">Checkout</Option>
                      </Select>
                    </Form.Item>
                    </div>


                    <Form.Item name="min_amount" label="Minimum amount"rules={[
                      {
                        required: true,
                        message: "Please enter a minimum amount!",
                      },
                    ]}>
                      <InputNumber
                      placeholder="Minimum amount for discount"
                        className="inputWidthFull"
                        label="Minimum amount"
                      />
                    </Form.Item>

                    <Form.Item name="status" label="Status"
                    rules={[
                      {
                        required: true,
                        message: "Select a status!",
                      },
                    ]}>
                      <Select
                        placeholder="Status"
                      >
                        <Option>Status</Option>
                        <Option value="active">Activate</Option>
                        <Option value="disabled">Deactivate</Option>
                      </Select>
                    </Form.Item>

                    <Form.Item name="categories" label="Categories" rules={[
                      {
                        required: true,
                        message: "Select a category!",
                      },
                    ]}>
                      <Select
                        mode="multiple"
                        placeholder="Choose category"
                      >
                        {categoryList && categoryList.map((category, index) => (
                          <Option key={index} value={category.id}>{category.name}</Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item name="products" label="Products"
                    //  rules={[
                    //   {
                    //     required: true,
                    //     message: "Select a products!",
                    //   },
                    // ]}
                    >
                  
                      <Select
                        mode="multiple"
                        placeholder="Select products"
                      >
                        {products && products.map((product, index) => (
                          <Option key={index} value={product.id}>{product.title}</Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item name="all"  
                       >
                <Checkbox
                 onChange={(e) => 
                  setAll(e.target.checked)
                }>
                All Products
            </Checkbox>
                    </Form.Item>

                    

                    <Form.Item  style={{gridColumn: '1/3'}}>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"

                        // onClick={() => {
                        //   // onReset()
                        //   setAddNewDiscount(false)
                        // }}
                      >
                        Add Discount
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => {
                          onReset()
                          setAddNewDiscount(false)
                        }}
                      >
                        Close
                      </Button>
                    </Form.Item >

                  </Form>
                </div>
}
              <div className="table-responsive">
                <table className="table align-middle table-nowrap mb-0">
                  <thead className="table-light">
                    <tr>
                      <th className="align-middle">Date added</th>
                      <th className="align-middle">Code</th>
                      <th className="align-middle">Offer</th>
                      <th className="align-middle">Minimum amount</th>
                      <th className="align-middle">Status</th>
                      <th className="align-middle">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                  {discounts.map((discount, index) => (
                      <tr key={index}>
                        {/* <td>{index + 1}</td> */}
                        <td>
                          <span className="text-small">
                            {moment(discount.created_at).format("ddd, MMM Do YYYY")}
                          </span>
                        </td>
                        <td>{discount.code}</td>
                        <td>{discount.offer}</td>
                        <td>{discount.min_amount}</td>
                        <td>{discount.status}</td>
                        <td><Button type="primary"
                          primary
                          onClick={() => {
                            Router.push(`discount/${discount.id}`);
                            console.log(discount.id)
                          }}
                        >
                          Detail
                        </Button></td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </Spin>
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
