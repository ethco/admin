import { useState, useEffect } from "react";
import DashboardLayout from "../../layouts/DashboardLayout";
import Head from "../../components/Head";
import Link from "next/link";
import { Modal, Button, Input, Form, InputNumber, Select, Upload } from "antd";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
  getAllStores,
  getCategories,
  getAllProducts,
  getProducts,
  getAllPartnerStoreByName,
  getStoreCategory,
  getCoupon,
  deleteCoupon,
  updateCoupon,
} from "../../helper/redux/actions/app";
import { ExclamationCircleOutlined } from "@ant-design/icons";
const SingleCoupon = ({ id }) => {
  const [loading, setLoading] = useState(true);
  const [coupon, setCoupon] = useState({});
  const [editCoupon, setEditCoupon] = useState(false);
  const [isTrue, setIsTrue] = useState(true);
  const [categories, setCategories] = useState([]);
  const [stores, setStores] = useState([]);
  const [products, setProducts] = useState([]);

  const [deleteLoading, setDeleteLoading] = useState(false);
  const [categoryList, setCategoryList] = useState([]);
  const [storeList, setStoreList] = useState([]);
  const [productList, setProductList] = useState([]);
  const [category, setCategory] = useState([]);
  const [storeName, setStoreName] = useState([]);
  const [productName, setProductName] = useState([]);
  const [storeID, setStoreID] = useState([]);

  const [couponId, setCouponId] = useState('');
  const dispatch = useDispatch();


  const [form] = Form.useForm();
  const { Option } = Select;
  const { confirm } = Modal;


  const onReset = () => {
    form.resetFields();
  };


  const getAllCategories = async () => {
    try {
      setLoading(true);
      await dispatch(await getCategories())
        .then((response) => {
          setCategories(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }
  const getAllStore = async () => {
    try {
      setLoading(true);
      await dispatch(await getAllStores())
        .then((response) => {
          setStores(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }

  // Get All Products for dropdown
  useAsync(async () => {
    storeID.map(async (id) => {
      try {
        await dispatch(await getProducts(id))
          .then((response) => {
            setLoading(false);
            setProducts(response.data);
            console.log(response.data)
          })
          .catch((error) => {
            setLoading(false);
            // notification.error({
            //   message: "Oops!",
            //   description: "Unable to send response, try again",
            // });
          });
      } finally {
        setLoading(false);
      }
    })
  }, [storeID?.length])

  useAsync(async () => {
    await getAllCategories()
    await getAllStore()
  }, [])



  // Get Coupon Details
  useAsync(async () => {
    await getAllCoupons()
  }, [])

  const getAllCoupons = async () => {
    try {
      setLoading(true);
      await dispatch(await getCoupon(id))
        .then((response) => {
          setCoupon(response.data);
          setCouponId(response.data.id)
          setCategoryList(response.data.categories)
          setStoreList(response.data.stores)
          setStoreID(response.data.stores)
          setProductList(response.data.products)
          console.log(response.data.id)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "please try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }

  // Show List for Category, store and products
  // categoryList
  useAsync(async () => {
    categoryList.map(async (id) => {
      try {
        setLoading(true);
        await dispatch(await getStoreCategory(id))
          .then((response) => {
            console.log(response.data.name)
            setCategory(old => [...old, response.data]);
          })
          .catch((error) => {
            notification.error({
              message: "Oops!",
              description: "Unable to send response, try again",
            });
          });
        // }
      }
      finally {
        setLoading(false);
      }
    })
  }, [categoryList?.length])

  //  storeList
  useEffect(() => {
    storeList?.map(async (id) => {
      try {
        setLoading(true);
        await dispatch(await getAllPartnerStoreByName(id))
          .then((response) => {
            console.log(response.data.name)
            setStoreName(old => [...old, response.data])
          })
          .catch((error) => {
            notification.error({
              message: "Oops!",
              description: "Unable to send response, try again",
            });
          });
      }
      finally {
        setLoading(false);
      }
    }
    )
  }, [storeList?.length])

  // ProductList
  useEffect(() => {
    productList?.map(async (id) => {
      try {
        setLoading(true);
        await dispatch(await getProducts(id))
          .then((response) => {
            console.log(response.data)
            setProductName(old => [...old, response.data.name])
          })
          .catch((error) => {
            notification.error({
              message: "Oops!",
              description: "Unable to send response, try again",
            });
          });
      }
      finally {
        setLoading(false);
      }
    }
    )
  }, [productList])



  // Add Coupon Code
  const EditCouponCode = async (value) => {
    try {
      setLoading(true);

      const payload = {
        id: id,
        data: {
          discount: {
            code: coupon.code,
            offer: value.offer ? value.offer : coupon.offer,
            type: value.type ? value.type : coupon.type,
            min_amount: value.min_amount ? value.min_amount : coupon.min_amount,
            status: value.status ? value.status : coupon.status,
          },
          products: value.products ? value.products : coupon.products,
          categories: value.categories ? value.categories : coupon.categories,
          stores: value.stores ? value.stores : coupon.stores,
        }
      };
      await dispatch(await updateCoupon({ payload }))
        .then((response) => {
          getAllCoupons()
          onReset()
          setEditCoupon(false)
          console.log("Update Success")
          notification.success({
            message: "Coupon",
            description: "Coupon updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  // Delete Coupon
  const confirmDelete = () => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to delete this coupon?",
      async onOk() {
        try {
          setDeleteLoading(true);
          const payload = {
            id: id
          };

          await dispatch(await deleteCoupon({ payload }))
            .then((response) => {
              Router.push(`/coupon`);
              setDeleteLoading(false);
              notification.success({
                message: "Coupon",
                description: "Coupon deleted successfully",
              });
            })
            .catch((error) => {
              setDeleteLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() { },
    });
  };

  return (
    <>
      <Head title={"Coupon"} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Coupon details" page="coupon">
            <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setEditCoupon(true)}
            >
              Update Coupon
            </Button>
            <Button danger loading={deleteLoading} onClick={confirmDelete}>
              Delete Coupon
            </Button>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <div className="row">
              <div className="col-md-12">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title mb-4">Coupon Information</h4>
                    <div className="table-responsive">
                      <table className="table table-nowrap mb-0">
                        <tbody>
                          <tr>
                            <th className="row">Date Created</th>
                            <td>
                              {moment(coupon.created_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                          </tr>
                          <tr>
                            <th className="row">Coupon code</th>
                            <td>{coupon.code}</td>
                          </tr>

                          <tr>
                            <th className="row">Minimum amount</th>
                            <td>{coupon.min_amount}</td>
                          </tr>
                          <tr>
                            <th className="row">Offer</th>
                            <td>{coupon.offer}</td>
                          </tr>
                          {category.length > 0 &&
                            <tr>
                              <th className="row">Categories</th>
                              <td> {category.map((item, index) => {
                                return <span key={index}>{(index ? ', ' : '') + item.name}</span>;
                              })
                              }</td>
                            </tr>
                          }
                          {storeName.length > 0 &&
                            <tr>
                              <th className="row">Stores</th>
                              {/* <td>{coupon.stores}</td> */}
                              <td>{storeName && storeName.map((item, index) => {
                                return <span key={index}>{(index ? ', ' : '') + item.name}</span>;
                              })}</td>

                            </tr>
                          }
                          {category.length === 0 &&
                            <tr>
                              <th className="row">Products</th>
                              <td>All products</td>
                            </tr>
                          }
                          <tr>
                            <th className="row">Type</th>
                            <td>{coupon.type}</td>
                          </tr>

                          <tr>
                            <th className="row">Updated date</th>
                            <td>
                              {moment(coupon.updated_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          )}
        </div>
        <Modal
          title="Edit Coupon"
          centered
          visible={editCoupon}
          onCancel={() => setEditCoupon(false)}
          width={"800px"}
          footer={null}
        >
          <Form
            form={form}
            name="add_product"
            onFinish={EditCouponCode}
            layout="vertical"
            style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', gridColumnGap: '24px' }}
          >

            <Form.Item name="offer"
              label="Offer">
              <Input
                placeholder={coupon.offer}
                className="inputWidthFull"
                label="Offer"

              />
            </Form.Item>

            <Form.Item name="type" label="Type"
              initialValue={coupon.type}>
              <Select
                placeholder="Type"
              >
                <Option value="percentage">Percentage</Option>
                <Option value="amount">Amount</Option>
              </Select>
            </Form.Item>

            <Form.Item name="min_amount" label="Minimum amount"
            // rules={[
            //   {
            //     required: true,
            //     message: "Please enter a minimum amount!",
            //   },
            // ]}
            >
              <InputNumber
                placeholder={coupon.min_amount}
                className="inputWidthFull"
                label="Minimum amount"
              />
            </Form.Item>

            <Form.Item name="status" label="Status"
            // rules={[
            //   {
            //     required: true,
            //     message: "Select a status!",
            //   },
            // ]}
            >
              <Select
                // placeholder="Status"
                defaultValue={coupon.status}
              >
                <Option value="active">Activate</Option>
                <Option value="disabled">Deactivate</Option>
              </Select>
            </Form.Item>

            <Form.Item name="categories" label="Categories"
            // rules={[
            //   {
            //     required: true,
            //     message: "Select a category!",
            //   },
            // ]}
            >

              <Select
                mode="multiple"
                placeholder="Choose category"
                defaultValue={category && category.map((item) => {
                  return item.id
                })}
              >
                {categories && categories.map((category, index) => (
                  <Option key={index} value={category.id}>{category.name}</Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item name="stores" label="Stores"
            //  rules={[
            // {
            //   required: true,
            //   message: "Select a store!",
            // },
            // ]}
            >
              <Select
                mode="multiple"
                placeholder="Select stores"
                defaultValue={storeName && storeName.map((item) => {
                  return item._id
                })}
                onChange={e => (setStoreID([...storeID, e]), setIsTrue(false)
                )}
              >
                {stores && stores.map((store, index) => (
                  <Option key={index} value={store._id}>{store.name}</Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item name="products" label="Products"
            //  rules={[
            //   {
            //     required: true,
            //     message: "Select a products!",
            //   },
            // ]}
            >

              <Select
                disabled={isTrue}
                mode="multiple"
                placeholder="Select products"
                defaultValue={productName}
              >
                {products && productList?.map((product, index) => (
                  <Option key={index} value={product.id}>{product.title}</Option>
                ))}
              </Select>
            </Form.Item>


            <Form.Item style={{ gridColumn: '1/3' }}>
              <Button
                loading={loading}
                htmlType="submit"
                className="mt-2"
                type="primary"
              >
                Edit Coupon
              </Button>
              <Button
                className="mt-2 mx-3"
                type="primary"
                onClick={() => {
                  onReset()
                  setEditCoupon(false)
                }}
              >
                Close
              </Button>
            </Form.Item >

          </Form>
        </Modal>
      </DashboardLayout>
    </>
  );
};

SingleCoupon.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default SingleCoupon;
