import { useEffect, useState } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Randomstring from "randomstring";
import Router from "next/router";
// import SearchSelect from "../../components/SearchSelect";
import Link from "next/link";
import {
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  Select, Modal, Spin,
  Form, InputNumber, Input, Button, Checkbox, Table
} from "antd";
import {
  getProducts,
  getAllStores,
  getCategories,
  AddCoupon,
  getCoupons,
  getStoreCategory,
} from "../../helper/redux/actions/app";
import { useDispatch } from "react-redux";
import { useAsync } from "react-use";
import moment from "moment";

export default function Index() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [isTrue, setIsTrue] = useState(true);
  const [isAll, setIsAll] = useState(false);
  const [coupons, setCoupons] = useState([]);
  const [products, setProducts] = useState([]);
  const [storeList, setStoreList] = useState([]);
  const [categoryList, setCategoryList] = useState([]);
  const [storeID, setStoreID] = useState([]);
  const [pageLoading, setPageLoading] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [addNewCoupon, setAddNewCoupon] = useState(false);
  const [all, setAll] = useState(true);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);

  const { confirm } = Modal;
  const { Option } = Select;
  const { TextArea } = Input;
  const [form] = Form.useForm();

  // Same Function with getStoreList()

  const onReset = () => {
    form.resetFields();
  };

  const getAllCoupons = async (value) => {
    try {
      setLoading(true);
      await dispatch(await getCoupons(value))
        .then((response) => {
          setLoading(false);
          setCoupons(response.data);
        })
        .catch((error) => {
          setLoading(false);
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  };

  const columns = [
    {
      key: 1,
      title: 'Date added',
      dataIndex: 'created_at',
      render: (created_at) => (<span className="text-small">
        {moment(created_at).format(
          "ddd, MMM Do YYYY"
        )}
      </span>)
    },
    {
      key: 2,
      title: 'Code',
      dataIndex: 'code',
      render: (code) => (
        <p>{code}</p>
      )
    },
    {
      key: 3,
      title: 'Offer',
      dataIndex: 'offer',
      render: (offer) => (
        <p>{offer}</p>
      )
    },
    {
      key: 4,
      title: 'Minimum amount',
      dataIndex: 'min_amount',
      render: (min_amount) => (
        <p>{min_amount}</p>
      )
    },
    {
      key: 5,
      title: 'Status',
      dataIndex: 'status',
      render: (status) => (
        <p>{status}</p>
      )
    },
    {
      title: "Details",
      key: 6,
      dataSource: ['id'],
      render: (record) => (
        <Button type="primary"
          primary
          onClick={() => {
            Router.push(`coupon/${record.id}`);
          }} >
          Detail
        </Button >
      )
    }
  ];


useAsync(async () => {
  storeID.map(async (i) => {

    try {
      await dispatch(await getProducts(i))
        .then((response) => {
          setLoading(false);
          setProducts(response.data);

        })
        .catch((error) => {
          setLoading(false);
          // notification.error({
          //   message: "Oops!",
          //   description: "Unable to send response, try again",
          // });
        });
    } finally {
      setLoading(false);
    }
  })
}, [storeID.length])
// useEffect(()=>{
//   getProduct()
// },[storeID])

useEffect(() => {
  //   getProduct()
  console.log(storeID)
  console.log(products)
}, [storeID.length])

// Get all stores
useAsync(async () => {
  try {
    setLoading(true);
    await dispatch(await getAllStores())
      .then((response) => {
        setStoreList(response.data);
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to send response, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}, [])

const checkForm = (value) => {
  // console.log('code:' + Randomstring.generate(7))
  console.log(value)
}



// get all categories 
useAsync(async () => {
  try {
    setLoading(true);
    await dispatch(await getCategories())
      .then((response) => {
        setCategoryList(response.data);
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to send response, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}, [])

// const merge = []
// useEffect(() => {
//   setResults([...products])
//    console.log(...products)
// }, [products])

// Add Coupon Code
const AddCouponCode = async (value) => {
  try {
    setLoading(true);

    const payload = {
      data: {
        discount: {
          code: value.coupon,
          offer: value.offer,
          type: value.type,
          min_amount: value.min_amount,
          status: value.status,
        },
        products: value.products,
        categories: value.categories,
        stores: value.stores,
        all_products: all
      }
    };
    await dispatch(await AddCoupon({ payload }))
      .then((response) => {
        getAllCoupons()
        onReset()
        setAddNewCoupon(false)
        console.log("Update Success")
        notification.success({
          message: "Store",
          description: "Store updated successfully",
        });
      })
      .catch((error) => { });
  } finally {
    setLoading(false);
  }
};
useEffect(() => {
  all ? setIsAll(true)
    : setIsAll(false)
}, [all])

useAsync(async () => {
  await getAllCoupons()
  await getAllProduct()
}, []);


return (
  <>
    <Head title="All Coupons" />
    <DashboardLayout>
      <div className="container-fluid">
        <PageHeader title="All Coupon" page="coupon">
          <Button
            type="primary"
            className="mx-3"
            primary
            onClick={() => setAddNewCoupon(true)}
          >
            Add new coupon
          </Button>

        </PageHeader>
        {loading ? (
          <PageLoading />
        ) : (
          <Spin spinning={pageLoading}>
            {addNewCoupon &&
              <div className="row">
                <div className="col-md-2"></div>
                <div className="col-md-8">
                  <Form
                    form={form}
                    name="add_product"
                    // onFinish={checkForm}
                    onFinish={AddCouponCode}
                    layout="vertical"
                    style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', gridColumnGap: '24px' }}
                  >

                    <Form.Item name="coupon"
                      label="Enter coupon code" rules={[
                        {
                          required: true,
                          message: "Enter coupon code!",
                        },
                      ]}>
                      <Input
                        placeholder="Enter coupon code"
                        className="inputWidthFull"
                        label="Coupon code"

                      />
                    </Form.Item>

                    <div style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      width: '100%',
                      gap: '16px'
                    }}>
                      <Form.Item name="type" label="Type"
                        style={{ width: '100%' }}
                        initialValue='percentage'>
                        <Select
                          placeholder="Type"
                        >
                          <Option value="percentage">Percentage</Option>
                          <Option value="amount">Amount</Option>
                        </Select>
                      </Form.Item>
                      <Form.Item name="status" label="Status"
                        style={{ width: '100%' }}
                        rules={[
                          {
                            required: true,
                            message: "Select a status!",
                          },
                        ]}>
                        <Select
                          placeholder="Status"
                        >
                          <Option value="active">Activate</Option>
                          <Option value="disabled">Deactivate</Option>
                        </Select>
                      </Form.Item>

                    </div>

                    <Form.Item name="categories" label="Categories"
                    // rules={[
                    //   {
                    //     required: true,
                    //     message: "Select a category!",
                    //   },
                    // ]}
                    >
                      <Select
                        disabled={isAll}
                        mode="multiple"
                        placeholder="Choose category"
                      >
                        {categoryList && categoryList.map((category, index) => (
                          <Option key={index} value={category.id}>{category.name}</Option>
                        ))}
                      </Select>
                    </Form.Item>

                    <div style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      width: '100%',
                      gap: '16px'
                    }}>
                      <Form.Item name="offer"
                        label="Amount/Percentage" rules={[
                          {
                            required: true,
                            message: "Enter Amount/Percentage!",
                          },
                        ]}>
                        <Input
                          placeholder="Amount/Percentage"
                          className="inputWidthFull"
                          label="Offer"

                        />
                      </Form.Item>
                      <Form.Item name="min_amount" label="Minimum amount" rules={[
                        {
                          required: true,
                          message: "Please enter a minimum amount of order!",
                        },
                      ]}>
                        <InputNumber
                          placeholder="Minimum amount of order"
                          className="inputWidthFull"
                          label="Minimum amount of order"
                        />
                      </Form.Item>
                    </div>

                    <Form.Item name="stores" label="Stores"
                    // rules={[
                    //   {
                    //     required: true,
                    //     message: "Select a store!",
                    //   },
                    // ]}
                    >
                      <Select
                        disabled={isAll}
                        mode="multiple"
                        placeholder="Select stores"
                        onChange={e => (
                          setStoreID([...storeID, e]),
                          setIsTrue(false)
                        )}
                      >
                        {storeList && storeList.map((store, index) => (
                          <Option key={index} value={store._id}>{store.name}</Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item name="products" label="Products"
                    //  rules={[
                    //   {
                    //     required: true,
                    //     message: "Select a products!",
                    //   },
                    // ]}
                    >

                      <Select
                        disabled={isTrue || isAll}
                        mode="multiple"
                        placeholder="Select products"
                      >
                        {products && products.map((product, index) => (
                          console.log(product.title),
                          <Option key={index} value={product.id}>{product.title}</Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item name="all"
                    >
                      <Checkbox
                        checked={all}
                        onChange={(e) =>
                          setAll(e.target.checked)

                        }>
                        Select All Products
                      </Checkbox>
                    </Form.Item>

                    <Form.Item style={{ gridColumn: '1/3' }}>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"

                      // onClick={() => {
                      //   // onReset()
                      //   setAddNewCoupon(false)
                      // }}
                      >
                        Add Coupon
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => {
                          onReset()
                          setAddNewCoupon(false)
                        }}
                      >
                        Close
                      </Button>
                    </Form.Item >

                  </Form>
                </div>
                <div className="col-md-2"></div>
              </div>
            }
             <Table
            loading={loading}
            columns={columns}
            dataSource={coupons}
            pagination={{
              current: page,
              pageSize: pageSize,
              total: coupons.length,
              onChange: (page, pageSize)=>{
                setPage(page);
                setPageSize(pageSize);
              }
            }
            }
            />   

          </Spin>
        )}
      </div>
    </DashboardLayout>
  </>
);
}
