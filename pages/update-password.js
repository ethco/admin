import { useState } from "react";
import DashboardLayout from "../layouts/DashboardLayout";
import Head from "../components/Head";
import Link from "next/link";
import { Form, Button, Input, notification, Select, InputNumber } from "antd";
import PageHeader from "../components/PageHeader";
import { actionUpdatePassword } from "../helper/redux/actions/app";
import { useDispatch } from "react-redux";
import AuthStorage from "../helper/utils/auth-storage";
import Router from "next/router";

const AddPartner = (props) => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const { Option } = Select;

  const submitForm = async (values) => {
    try {
      setLoading(true);
      const data = {
        user: AuthStorage.user.id || ``,
        values,
      };

      console.log(data);
      await dispatch(await actionUpdatePassword({ data }))
        .then((response) => {
          setLoading(false);
          form.resetFields();
          notification.success({
            message: "Password Update",
            description: "Password has been updated successfully",
          });
          sessionStorage.clear();
          setTimeout(() => {
            AuthStorage.destroy(() => {
              Router.push("/auth/login");
            });
          }, 1000);
        })
        .catch((error) => {
          setLoading(false);
        });
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      <Head title="Update Password" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Update password" page="update-password">
            <Link href="/profile">
              <a>
                <Button type="border">View profile</Button>
              </a>
            </Link>
          </PageHeader>
          <div className="row">
            <div className="col-md-7">
              <div className="card card-bordered">
                <div className="card-body">
                  <Form
                    form={form}
                    size="large"
                    name="add_partner"
                    onFinish={submitForm}
                    layout="vertical"
                  >
                    <div className="row">
                      <div className="col-md-6">
                        <Form.Item
                          name="password"
                          label="New password"
                          rules={[
                            {
                              required: true,
                              message: "Please input your new password!",
                            },
                          ]}
                        >
                          <Input.Password
                            className="inputWidthFull"
                            label="New password"
                          />
                        </Form.Item>
                      </div>
                      <div className="col-md-6">
                        <Form.Item
                          name="confirm_password"
                          label="Confirm password"
                          rules={[
                            {
                              required: true,
                              message: "Please input password!",
                            },
                          ]}
                        >
                          <Input.Password
                            className="inputWidthFull"
                            label="Confirm password"
                          />
                        </Form.Item>
                      </div>
                    </div>

                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      >
                        Update password
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    </>
  );
};

export default AddPartner;
