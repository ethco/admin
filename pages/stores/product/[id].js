import { useState, useEffect } from "react";
import DashboardLayout from "../../../layouts/DashboardLayout";
import Head from "../../../components/Head";
import Link from "next/link";
import { Modal, Button, Input, Form, InputNumber, Select, Upload, Image } from "antd";
import PageHeader from "../../../components/PageHeader";
import PageLoading from "../../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
  actionEditProduct,
    deleteProduct,
    getProduct,
    actionProductQuantity,
    getCategories,
    getStoreCategory,
    getPartnerCategories,


} from "../../../helper/redux/actions/app";
import {
  UploadOutlined,
  ExclamationCircleOutlined
} from "@ant-design/icons";
const SingleProduct = ({ id }) => {
  const [loading, setLoading] = useState(true);
  const [editProduct, setEditProduct] = useState(false);
  const [deleteLoading, setDeleteLoading] = useState(false);
  const [product, setProduct] = useState({});
  const [addProduct, setAddProduct] = useState(null);
  const [categoryList, setCategoryList] = useState([]);
  const [categoryID, setCategoryID] = useState([]);
  const [storeID, setStoreID] = useState('');
  const dispatch = useDispatch();

  
  const [ form ] = Form.useForm();
  const { Option } = Select;
  const { TextArea } = Input;
  const { confirm } = Modal;

 // get all categories 
 useAsync(async () => {
  try {
    setLoading(true);
    await dispatch(await getCategories())
      .then((response) => {
        setCategoryList(response.data);
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to send response, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}, [])

  // product Category
  // const getProductCategory = async () => {
  //   try {
  //     // Get plan ID to replace the data
  //     await dispatch(await getStoreCategory(categoryID))
  //       .then((response) => {
  //         setStoreCategory(response.data.name);
  //         console.log(response.data.name)
  //       })
  //       .catch((error) => {
  //         notification.error({
  //           message: "Oops!",
  //           description: "Unable to get store category, try again",
  //         });
  //       });
  //   } finally {
  //     setLoading(false);
  //   }
  // }

// Get Products Details
const productDetails = async (value) => {
    try {
      setLoading(true);
      await dispatch(await getProduct(id))
        .then((response) => {
          setProduct(response.data);
          setStoreID(response.data.store_id)
          setCategoryID(response.data.categories)
          console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "try again",
          });
        });
    } finally {
      setLoading(false);
    }
    }
 useAsync(async () =>{
   await productDetails()
  }, [])






const productCategories = async (value) => {
  try {
    setLoading(true);
    await dispatch(await getStoreCategory(id))
      .then((response) => {
        setProduct(response.data);
        console.log(response.data)
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "try again",
        });
      });
  } finally {
    setLoading(false);
  }
  }


   // update Store Subscription
   const productEdit = async (value) => {
    try {
      setLoading(true);
      const images1 = value.image > 0 ? value.image.map(e, i => {
        return e[i].thumbUrl
      }) : value.image === 0 ? [value.image[0].thumbUrl] : null
      const payload = {
        product_id: id,
        data: {
          title: value.title ? value.title : product.title,
          sku: value.sku  ? value.sku : product.sku,
          description: value.description  ? value.description : product.description,
          price: value.price ? value.price : product.price,
          image: images1 ? images1 : product.image,
          categories: value.category ? value.category : product.categories,
          quantity: value.quantity ? value.quantity : product.quantity,
          unit: "kg",
        }
      };
      await dispatch(await actionEditProduct({ payload }))
        .then((response) => {
          setLoading(false)
          setEditProduct(false)
          onReset()
          productDetails()
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => {
          setEditProduct(false)
        });
    } finally {
      setLoading(false);
    }
  };
  const addProductImage = (e) => {
    console.log('Upload event:', e);

    if (Array.isArray(e)) {
      return e;
    }

    return e?.fileList;
  };

// Restock product
const restock = async () => {
  try {
    const payload = {
      product_id: id,
      data: {
        quantity: addProduct ? addProduct : product.quantity,
      }
    };
    await dispatch(await actionProductQuantity({ payload }))
      .then((response) => {
        productDetails()
        setDisabled(true)
        console.log("Update Success")
        notification.success({
          message: "Product",
          description: "Product added successfully",
        });
      })
      .catch((error) => {
        // error.message()
      });
  } finally {
    setLoading(false);
  }
};
const [disabled, setDisabled] = useState(true);
const toggle = () => {
  setDisabled(!disabled);
};
  // Delete Customer
  const confirmDelete = () => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to delete this product?",
      async onOk() {
        try {
          setDeleteLoading(true);
          const id = product.id;

          await dispatch(await deleteProduct({ id }))
            .then((response) => {
              Router.push(`/stores/store/${storeID}`);
                setDeleteLoading(false);
              notification.success({
                message: "Product",
                description: "Product deleted successfully",
              });
            })
            .catch((error) => {
              setDeleteLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() { },
    });
  };
  // Form reset
  const onReset = () => {
    form.resetFields();
  };

 

  return (
    <>
      <Head title={product.title || ""} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader  title="Product info" page="store" page1="product">
          
          <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setEditProduct(true)}
            >
             Update Product
            </Button>
            <Button danger loading={deleteLoading} onClick={confirmDelete}>
              Delete Product
            </Button>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <div className="row">
               <div className="col-md-12 mb-3">
               <Image.PreviewGroup>
               {product.image && product.image.map((image, index) => (
                <Image key={index} width={200} src={image} alt={`product images`}/>
               ))}
               </Image.PreviewGroup>
  </div>
              <div className="col-md-9">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title mb-4">Product Information</h4>
                    <div className="table-responsive">
                      <table className="table table-nowrap mb-0">
                        <tbody>
                          <tr>
                            <th className="row">Date Created</th>
                            <td>
                              {moment(product.created_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                          </tr>
                          <tr>
                            <th className="row">Product Name</th>
                            <td>{product.title}</td>
                          </tr>
                          <tr>
                            <th className="row">SKU</th>
                            <td>{product.sku}</td>
                          </tr>
                          <tr>
                            <th className="row" >About Description</th>
                            <td style={{whiteSpace:"break-spaces"}}>{product.description}</td>
                          </tr>
                          <tr>
                            <th className="row">Price</th>
                            <td>{`£${product.price}`}</td>
                          </tr>
                          <tr>
                            <th className="row">Quantity</th>
                            <td>{product.quantity}</td>
                          </tr>
                          <tr>
                            <th className="row">Unit</th>
                            <td>{product.unit}</td>
                          </tr>
                          {/* Display status */}
                          {product.status && product.status === "unpublished" ?
                          <tr>
                          <th className="row">Status</th>
                          <td className="text-capitalize badge-soft-danger">{product.status}</td>
                        </tr> :
                        <tr>
                            <th className="row">Status</th>
                            <td className="text-capitalize badge-soft-success">{product.status}</td>
                          </tr>
                              }
                          <tr>
                            <th className="row">Date Updates</th>
                            <td>
                              {moment(product.updated_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <p className="h4 mb-2">Add product to store</p>
              <InputNumber min={1} disabled={disabled} defaultValue={product.quantity} onChange={(num) => setAddProduct(num)}/>
              <Button onClick={restock}  disabled= {disabled} type="primary"   className="mx-3"> Add to product </Button>
      <div
        style={{
          marginTop: 20,
        }}
      >
        <Button onClick={toggle} type="primary">
          Update stock
        </Button>
      </div>
                </div>

                {/* UPDATE Product details */}
                <Modal
                  title="Update product"
                  centered
                  visible={editProduct}
                  width={500}
                  onCancel={() => setEditProduct(false)}
                  footer={null}
                >
                  <Form
                        form={form}
                        name="add_product"
                        onFinish={productEdit}
                        layout="vertical"
                      >
                        <Form.Item  name="title" label="Product title"
                        >
                          <Input
                            className="inputWidthFull"
                            label="Product title"
                            defaultValue={product.title}
                           
                          /></Form.Item>

                        <Form.Item name="sku" label="SKU"
                        >
                          <Input 
                            className="inputWidthFull"
                            label="SKU"
                            defaultValue={product.sku}
                           
                          />
                        </Form.Item>

                        <Form.Item name="price" label="Price"
                        >
                          <InputNumber
                          name="price"
                          className="inputWidthFull"
                          label="price"
                          defaultValue={product.price}
                          />
                        </Form.Item>

                        <Form.Item name="quantity" label="Quantity"
                        >
                          <InputNumber
                            className="inputWidthFull"
                            label="quantity"
                            defaultValue={product.quantity}
                          />
                        </Form.Item>

                        <Form.Item name="category" label="Categories"
                          // initialValue={productCategory}
                          
                          >
                          <Select
                            mode="multiple"
                            placeholder="Choose category"
                            defaultValue={product.categories}
                          >
                            {categoryList && categoryList.map((category, index) => (
                              <Option key={index} value={category.id}>{category.name}</Option>
                            ))}
                          </Select>
                        </Form.Item>

                        <Form.Item name="image" valuePropName="fileList"
                          getValueFromEvent={addProductImage}
                        >
                          <Upload name="image"
                            listType="picture"
                            multiple>
                            <Button icon={<UploadOutlined />}>Click to upload</Button>
                          </Upload>
                        </Form.Item>
                        
                        <Form.Item name="description"
                        >
                          <TextArea rows={4}
                            placeholder="Product description"
                            // className="inputWidthFull"
                            label="Description"
                            defaultValue={product.description}
                          // maxLength={6}
                          />

                        </Form.Item>

                        <Form.Item>
                          <Button
                            loading={loading}
                            htmlType="submit"
                            className="mt-2"
                            type="primary"
                            
                          // onClick={() =>{onReset}}
                          >
                            Update Product
                          </Button>
                          <Button
                            className="mt-2 mx-3"
                            type="primary"
                            onClick={() =>{
                              setEditProduct(false)
                              onReset()
                            }}
                          >
                            Cancel
                          </Button>
                        </Form.Item >
                      </Form>
                </Modal>
            </div>
          )}
        </div>
      </DashboardLayout>
    </>
  );
};

SingleProduct.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default SingleProduct;
