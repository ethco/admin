import { useState, useEffect, Fragment } from "react";
import DashboardLayout from "../../../layouts/DashboardLayout";
import Head from "../../../components/Head";
import Link from "next/link";


import {
  Modal, Button, Avatar,
  Input, Form, Select, InputNumber,
  Tabs, Switch, Upload, Table, Badge
} from "antd";
import PageHeader from "../../../components/PageHeader";
import PageLoading from "../../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import Randomstring from "randomstring";
import {
  getAllPartners,
  getAllPartnerStoreByName,
  getStoreOrders,
  getPlans,
  getStoreCategory,
  getProducts,
  getUnpublishedProducts,
  getCategories,
  getStoreSubscription,
  subscribeStoreFree,
  actionUpdateStore,
  UpdateSubscription,
  cancelSubscription,
  updateStoreAddress,
  storeConfiguration,
  AddProductToStore,
  getProduct,
  AddMultipleProducts,
  UpdateHours,
  updatePublishStatus,
  deleteStore,
  subscribeStorePayment,
  getASubscription,
  getPaymentKey

} from "../../../helper/redux/actions/app";
import { UploadOutlined, ExclamationCircleOutlined } from "@ant-design/icons";
import { set } from "lodash";
import { Elements } from "@stripe/react-stripe-js";
// import CheckoutForm from "../payment/CheckoutForm";
import { loadStripe } from "@stripe/stripe-js";


const sampleData = [];

const StoreDetails = ({ id }) => {
  const [addressLoading, setAddressLoading] = useState(false);
  const [loading, setLoading] = useState(true);
  const [pageLoading, setPageLoading] = useState(true);
  // const [pageloading, setPageLoading] = useState(false);
  const [loader, setLoader] = useState(false);
  const [storeData, setStoreData] = useState({});
  const [storeID, setStoreID] = useState("");
  const [partnerID, setPartnerID] = useState("");
  const [categoryID, setCategoryID] = useState('');
  const [category, setCategory] = useState([]);
  const [plans, setPlans] = useState([]);
  const [categories, setCategories] = useState([]);
  const [partners, setPartners] = useState([]);
  const [storeInfo, setStoreInfo] = useState(false);
  const [storeContact, setStoreContact] = useState(false);
  const [storeAddress, setStoreAddress] = useState(false);
  const [orders, setStoreOrders] = useState([]);
  const [news, setNews] = useState([]);
  const [dispatched, setDispatched] = useState([]);
  const [cancelled, setCancelled] = useState([]);
  const [hours, setHours] = useState({});
  const [products, setProducts] = useState([]);
  const [unpublished, setUnpublished] = useState([]);
  const [forRender, setForRender] = useState(false);
  const [addNewProduct, setAddNewProduct] = useState(false);
  const [addSub, setAddSub] = useState(false);
  const [freeSub, setFreeSub] = useState(false);
  const [paidSub, setPaidSub] = useState(false);
  const [isStoreActive, setIsStoreActive] = useState('');
  const [opens, setOpens] = useState('');
  const [closes, setClose] = useState('');
  


  
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);

  const dispatch = useDispatch();

  


  const [categoryList, setCategoryList] = useState([]);

  const [storePlan, setStorePlan] = useState('');
  const [storeCategory, setStoreCategory] = useState('');
  const [storePartner, setStorePartner] = useState('');
  const [subStatus, setSubStatus] = useState("");
  const [planID, setPlanID] = useState("");
  const [subID, setSubID] = useState("");
  const [subcription, setSubcription] = useState("");
  const [clientSecret, setClientSecret] = useState("");
  const [paymentKey, setPaymentKey] = useState("");
  const [stripePromise, setStripePromise] = useState(null);
  const [makePayment, setMakePayment]  = useState(false);

  


  // const [productName, setProductName] = useState([]);


  const [productCategory, setProductCategory] = useState([]);
  const [productCategoryID, setProductCategoryID] = useState('');
  const [addProducts, setAddProducts] = useState([{
    title: "",
    sku: "",
    description: "",
    price: "",
    image: "",
    categories: "",
    quantity: "",
    unit: "kg",
    product_code: Randomstring.generate(7)
  }])

  const { TabPane } = Tabs;

  const [form] = Form.useForm();
  const { Option } = Select;
  const { TextArea } = Input;
  const { confirm } = Modal;

// **************************** /


  // ****************************** */ RENDERS
  useAsync(async () => {
    await getAllStoreData()
    await getPartners()
    // await getPublicKey()
    await getPlansList()
  }, []);

  useEffect(() => {
    if (loader) {
      const toRef = setTimeout(() => {
        setForRender(true);
        setLoader(false)
        clearTimeout(toRef);
      }, 1000);
    }

  }, [(loader)]);

  useEffect(() => {
    if (forRender) {
      const toRef = setTimeout(() => {
        setForRender(false);
        clearTimeout(toRef);
        getStoreCategoryData()
        publishedProducts()
        unpublishedProducts()
      }, 2000);
    }
  }, [forRender]);


  const countNew = 0;
  const countProcessing = 0;
  const countProcessed = 0;
  const countCancelled = 0;
  const countDispatched = 0
  {orders
     && orders.map(order => {
      return (
        (order.status.status === "new") ? countNew += 1 :
        (order.status.status === "processed") ? countProcessed += 1 :
        (order.status.status === "processing") ? countProcessing += 1 :
          (order.status.status === "cancelled") ? countCancelled += 1 :
          countDispatched += 1
      )
    })
  }

  const daysOfTheWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

  // ***************************** */ GET FUNCTIONS
  const getAllStoreData = async () => {
    try {
      setLoading(true);
      await dispatch(await getAllPartnerStoreByName(id))
        .then(async (response) => {
          setLoader(true)
          setStoreData(response.data);
          setStoreID(response.data._id)
          setPartnerID(response.data.partner_id)
          setStorePartner(response.data.partner.business_name)
          setCategoryID(response.data.category)
          setHours(response.data.opening_hours)
          setPlanID(response.data.subscription.plan_id)
          setStorePlan(response.data.subscription.plan.name.toUpperCase());
          setSubStatus(response.data.subscription?.status)
          setSubID(response.data.subscription?._id)
          
          // setForRender(true);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get store details, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }
//  Order Table
  const orderColumns = [
    // {
    //   key: 1,
    //   title: 'Date',
    //   dataIndex: 'created_at',
    //   render:  (created_at)=> (<span className="text-small">
    //     {moment(created_at).format(
    //           "ddd, MMM Do YYYY"
    //                   )}
    //                   </span>)
    // },
    {
      key: 1,
      title: 'Name',
      dataIndex: 'pickup',
      render: (pickup)=> pickup.name
    },
    {
      key: 2,
      title: 'Status',
      dataIndex: 'status',
      render: (status => <Badge
      color={status.status === "dispatched" ? "blue" : 
      status.status === "new" ? "green" : 
      status.status === 'processed' ? "green" : 
      status.status === "processing" ? "yellow" 
      : "red"}
      text={status.status.toUpperCase()}
    />),
    filters:[
      {text:"NEW", value:"new"},
      {text:"PROCESSING", value:"processing"},
      {text:"PROCESSED", value:"processed"},
      {text:"DISPATCHED", value:"dispatched"},
      {text:"CANCELLED", value:"cancelled"}
    ],
    onFilter:(filterStatus, record)=>{
      return record.status.status === filterStatus
    }
    },
    {
      key: 4,
      title: 'Price',
      dataIndex: 'sub_total',
      render: (price)=> <p>{`£${price}`}</p>
    },
    // {
    //   key: 1,
    //   title: 'Address',
    //   dataIndex: 'pickup',
    //   render: (pickup)=> pickup.address
    // },
    // {
    //   key: 3,
    //   title: 'Email',
    //   dataIndex: 'pickup',
    //   render: (pickup)=> pickup.email
    // },
    {
      title: "View Order",
      key: "action",
      dataSource: "user",
      render: (record) => (
        <Button type="primary" onClick={() => Router.push(`/orders/${record._id}`)}>
          View Order  
        </Button>
      )
    }
  ];

  //  Order Table
  const productColumns = [
    // {
    //   key: 1,
    //   title: 'Date added',
    //   dataIndex: 'created_at',
    //   render:  (created_at)=> (<span className="text-small">
    //     {moment(created_at).format(
    //           "ddd, MMM Do YYYY"
    //                   )}
    //                   </span>)
    // },
    {
      key: 2,
      title: 'Product name',
      dataIndex: 'title',
    },
    // {
    //   key: 3,
    //   title: 'SKU',
    //   dataIndex: 'sku',
    // },
    {
      key: 4,
      title: 'Price',
      dataIndex: 'price',
      render: (price)=> <p>{`£${price}`}</p>
    },
    {
      key: 5,
      title: 'Quantity',
      dataIndex: 'quantity',
    },
    {
      key: 6,
      title: 'Status',
      dataIndex: ['status', 'id'],
      render: ((status, product) => <Switch
        style={{}}
        checkedChildren="Published"
        unCheckedChildren="Unpublished"
        checked={
          product.status === "published" ? true : false
        }
        onChange={(value) => {
          publishStatus(product.id, value);
        }}
      />)},  
    {
      title: "View product",
      key: "action",
      dataSource: "id",
      render: (product) => (
        <Button type="primary" onClick={() => Router.push(`/stores/product/${product.id}`)}>
          View product
        </Button>
      )
    }
  ];


  //  Get Store products
  const publishedProducts = async () => {
    try {
      await dispatch(await getProducts(storeID))
        .then((response) => {
          setLoading(false);
          setProducts(response.data.reverse());
        })
        .catch((error) => {
          setLoading(false);
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  };

    //  Get Store products
    const unpublishedProducts = async () => {
      try {
        await dispatch(await getUnpublishedProducts(storeID))
          .then((response) => {
            setLoading(false);
            setUnpublished(response.data.reverse());
          })
          .catch((error) => {
            setLoading(false);
            notification.error({
              message: "Oops!",
              description: "Unable to send response, try again",
            });
          });
      } finally {
        setLoading(false);
      }
    };


  // partners
  const getPartners = async () => {
    try {
      await dispatch(await getAllPartners())
        .then((response) => {
          setPartners(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get partner details, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }

  // Get Single product
  useAsync(async () => {
    productCategory.map(async (id) => {
      try {
        setLoading(true);
        await dispatch(await getStoreCategory(id))
          .then((response) => {
            setCategory(old => [...old, response.data.name]);
          })
          .catch((error) => {
            notification.error({
              message: "Oops!",
              description: "Unable to send response, try again",
            });
          });
        // }
      }
      finally {
        setLoading(false);
      }
    })
  }, [productCategory])

  useAsync(async () => {
    categoryList.map(async (cat) => {
    try {
      setLoading(true);
      await dispatch(await getStoreCategory(cat))
        .then((response) => {
          setCategory(old => [...old, response.data.name]);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
      // }
    }
    finally {
      setLoading(false);
    }
  })}, [categoryList])




  // Plans
  useAsync(async () => {
    try {
      await dispatch(await getPlans())
        .then((response) => {
          setPlans(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get partner details, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, [])

  // storeCategory
  const getStoreCategoryData = async () => {
    try {
      // Get plan ID to replace the data
      await dispatch(await getStoreCategory(categoryID))
        .then((response) => {
          setStoreCategory(response.data.name);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get store category, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }

  // categories
  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getCategories())
        .then((response) => {
          const data = (response.data);
          setCategories(data)
          // console.log(data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get Categories, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, [])

  // Get Store Subscription
  useEffect(async () => {
    try {
      setLoading(true);
      const payload = { 
        store_id: id,
        subscription_id: subID
       };
      await dispatch(await getASubscription(payload))
        .then((response) => {
          // console.log(response.data)
          // setPlanID(response.data.plan_id)       
        })
        .catch((error) => {
          // notification.error({
          //   message: "Oops!",
          //   description: "Unable to get Subscription",
          // });
        });
    } finally {
      setLoading(false);
    }
  }, [subID]);
 
 
 
  // Get all Store Orders
  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getStoreOrders(id))
        .then((response) => {
          setStoreOrders(response.data.reverse())
          const data = (response.data);
          setNews(old => [...old, response.data])
          // const postData = data.map(((driver) => {}));
        
          console.log(response.data)
        // }
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get Orders",
          });
        });
    } finally {
      setLoading(false);
    }
  }, []);

  // Get total revenue of the store
  const storeRevenue = orders.reduce((accumulator, object) => {
    return accumulator + object.sub_total;
  }, 0);



  // ************** */ NEW DATA FORM

  // Add product to a store
  const addProduct = async (value) => {
    try {
      setLoading(true);
      const images1 = value.image > 0 ? value.image.map(e, i => {
        return e[i].thumbUrl
      }) : [value.image[0].thumbUrl]
      const payload = {
        store_id: storeID,
        data: {
          title: value.title,
          sku: "ETHCO" + value.sku,
          description: value.description,
          price: value.price,
          image: images1,
          categories: [categoryID],
          quantity: value.quantity,
          unit: "kg",
          product_code: "ETHCO" + value.sku,
          // product_code: Randomstring.generate(7)
        }
      };
      await dispatch(await AddProductToStore({ payload }))
        .then((response) => {
          getAllStoreData()
          setAddNewProduct(false)
    // console.log(stripePromise)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };
  const addProductImage = (e) => {
    console.log('Upload event:', e);

    if (Array.isArray(e)) {
      return e;
    }
    return e?.fileList;
  };

  const onReset = () => {
    form.resetFields();
  };


  // Add Store Subscription Free
  const addSubFree = async (value) => {
    try {
      setLoading(true);
      const payload = {
        store_id: storeID,
        data: {
          plan_id: value.plan,
          paid: true,
          // duration: value.duration
        }
      };
      await dispatch(await subscribeStoreFree({ payload }))
        .then((response) => {
          setLoading(false)
          getAllStoreData()
          setSubcription(false)
          setFreeSub(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => {
          setSubcription(false)
        });
    } finally {
      setLoading(false);
    }
  };


  const addSubPaid = async (value) => {
    try {
      setLoading(true);
      const payload = {
        store_id: storeID,
        data: {
          plan_id: value.plan,
          // duration: value.duration,
          // paid: value.paid,
          coupon_code: value.coupon ? value.coupon : ""
        }
      };
      // console.log(subID)
      await dispatch(await subscribeStorePayment({ payload }))
        .then(async (response) => {
          console.log(response.data)
          setClientSecret(response.data.client_secret)
          console.log(response.data.client_secret)
          setLoading(false)
          setMakePayment(true)
          getAllStoreData()
          setSubcription(false)
          setPaidSub(false)
          setStripePromise(loadStripe('pk_test_51KKMxfHEWN29ooFqmg0HuxBfCNA1JVm9NCaZkFGmrTgMxFuLKdVaJBIajs4hHvbQw482sQFmwiKysAJjEzUvyYBN00TzK45PBJ'))
          // setClientSecret(response?.client_secret)
          
          

          // Router.push(`/stores/payment/${storeID}`)
          // setStripePromise(loadStripe(value.plan));
          // console.log(clientSecret)
          console.log("Update Success")
          notification.success({
            message: "Subscribe",
            description: "Store subscribed successfully",
          });
        })
        .catch((error) => {
          setSubcription(false)
        });
    } finally {
      setLoading(false);
    }
  };
  // useEffect(() => {
  //   {clientSecret && stripePromise && (
  //     setMakePayment(true)
  //     )}
  // }, [clientSecret])
  
 

  {clientSecret && 
    console.log(clientSecret)}

    
// Get public key
const getPublicKey = async () => {
  try {
    await dispatch(await getPaymentKey("test"))
      .then((response) => {
        setPaymentKey(response.data);
        setStripePromise(loadStripe(response.data));
        console.log(response.data)
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to get payment key, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}



  // ******************* */ UPDATE DATA FUNCTIONS

  // Change Store Address Information
  const submitAddress = async (value) => {
    try {
      setLoading(true);
      const payload = {
        store_id: storeID,
        data: {
          postcode: value.postcode,
          street: value.street,
          latitude: value.latitude,
          longitude: value.longitude
        }
      };
      console.log(value.street)
      await dispatch(await updateStoreAddress({ payload }))
        .then((response) => {
          setLoading(false)
          setAddressLoading(false)
          getAllStoreData()
          setStoreAddress(false)

          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  // Change Store Information
  const updateStoreInfo = async (value) => {
    try {
      setLoading(true);
      const payload = {
        store_id: storeID,
        data: {
          name: value.name ? value.name : storeData.name,
          mile_radius: value.mile_radius ? value.mile_radius : storeData.mile_radius,
          category: value.category ? value.category : storeData.category,
          featured: value.featured ? value.featured : storeData.featured
        }
      };
      console.log(value.street)
      await dispatch(await actionUpdateStore({ payload }))
        .then((response) => {
          setLoading(false)
          getAllStoreData()
          setStoreInfo(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  // Change Store Information
  const updateStoreHours = async (value) => {
    console.log(value)
    try {
      setLoading(true);
      const payload = {
        store_id: storeID,
        data: {
          day: value.day,
          opens_at: "09:00",
          closes_at: "16:15"
        }
      };
      console.log(value.street)
      await dispatch(await UpdateHours({ payload }))
        .then((response) => {
          setLoading(false)
          getAllStoreData()

          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  // Change Store Information
  const updateStoreConfig = async (value) => {
    try {
      setLoading(true);
      const logo = value.logo > 0 ? value.logo.map(e, i => {
        return e[i].thumbUrl
      }) : value.logo[0].thumbUrl
      const payload = {
        store_id: storeID,
        data: {
          mile_radius: storeData.mile_radius,
          category: value.category ? value.category : categoryID,
          shopping_option: value.option ? value.option : storeData.shopping_option,
          logo: logo ? logo : storeData.logo ? storeData.logo : "",
          contact_person: {
            name: value.name ? value.name : storeData.name,
            phone: value.phone ? value.phone : storeData.phone ? storeData.phone : "",
            email: value.email ? value.email : storeData.email ? storeData.email : ""
          }
        }
      };
      console.log(value.street)
      await dispatch(await storeConfiguration({ payload }))
        .then((response) => {
          setLoading(false)
          getAllStoreData()
          setStoreContact(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };

  const addContactImage = (e) => {
    console.log('Upload event:', e);

    if (Array.isArray(e)) {
      return e;
    }
    return e?.fileList;
  };


  const confirmDelete = () => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to delete this Service Charge?",
      async onOk() {
        try {
          setPageLoading(true);
          const payload = {
            id: storeID
          };

          await dispatch(await deleteStore({ payload }))
            .then((response) => {
              setPageLoading(false);
              Router.push(`/partners/${partnerID}`)
              notification.success({
                message: "Delete Store",
                description: "Stored deleted successfully",
              });
            })
            .catch((error) => {
              setPageLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() { },
    });
  };

  // Publish status 
  const publishStatus = async (id, status) => {
    
    try {
      setPageLoading(true);
      // setLoading(true)
      const payload = {
        id: id,
        data: {
          status: status ? "published" : "unpublished"
        }
      };
      console.log(id, status)
      await dispatch(await updatePublishStatus({ payload }))
        .then((response) => {
          console.log(response)
          setPageLoading(false);
          publishedProducts();
          unpublishedProducts()
          // setLoading(false)
          
          notification.success({
            message: "Products Status",
            description: "Status updated successfully",
          });
          // setPageLoading(false);
        })
        .catch((error) => {
          // setLoading(false)
          setPageLoading(false);
        });
    } finally {
      // setLoading(false);
          setPageLoading(false);
    }
  };

  // update Store Subscription
  const updateSub = async (value) => {
    try {
      setLoading(true);
      const payload = {
        subscription_id: subID,
        data: {
          plan_id: planID,
          paid: value.paid
        }
      };
      await dispatch(await UpdateSubscription({ payload }))
        .then((response) => {
          setLoading(false)
          getAllStoreData()
          setSubcription(false)
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => {
          setSubcription(false)
        });
    } finally {
      setLoading(false);
    }
  };

  const cancelSub = () => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to cancel this subscription?",
      async onOk() {
        try {
          setPageLoading(true);
          const payload = {
            subscription_id: subID
          };

          await dispatch(await cancelSubscription({ payload }))
            .then((response) => {
              setPageLoading(false);
              getAllStoreData()
              notification.success({
                message: "Cancel subscription",
                description: "Subscription cancelled successfully",
              });
            })
            .catch((error) => {
              setPageLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() { },
    });
  };


// {stripePromise && console.log(clientSecret)
// console.log(stripePromise)}




  // // Add Multiple products to a store

  let handleChange = (i, e) => {
    const { name, value } = e.target
    let newProductValues = [...addProducts];
    newProductValues[i][name] = value;
    setAddProducts(newProductValues);
  }

  let addFormFields = () => {
    setAddProducts([...addProducts, addProducts])
  }

  let removeFormFields = (i) => {
    let newProductValues = [...addProducts];
    newProductValues.splice(i, 1);
    setAddProducts(newProductValues)
  }

  let handleSubmit = (event) => {
    // event.preventDefault();
    alert(JSON.stringify(addProducts));
    console.log(addProducts)
  }










  return (
    <>
      <Head title={storeData.name || ""} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Single store" page="Go back" page1="Store" className="">
            <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setAddNewProduct(true)}
            >
              Add new product
            </Button>
            {subID ?
              <> 
               <Button 
                  type="primary"
                  className="mx-3 my-2"
                  primary
                  onClick={() =>   Router.push(`/stores/payment/${storeID}`)} >
                  payment </Button>
              <Button
                type="primary"
                className="mx-3 my-2"
                primary
                onClick={() => setSubcription(true)} >
                Subcription </Button>
                {subStatus !== "cancelled" && <Button
                  type="primary"
                  className="mx-3"
                  primary
                  onClick={() => cancelSub()}
                >
                  Cancel sub
                </Button>} </> :
              <Button
                type="primary"
                className="mx-3"
                primary
                onClick={() => setPaidSub(true)} >
                Subscribe store </Button>}
            <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setStoreInfo(true)} >
              Update Store </Button>
            <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setStoreAddress(true)} >
              Update Store Address </Button>
            <Button
              type="primary"
              className="mx-3"
              danger
              onClick={() => confirmDelete()} >
              Delete store </Button>

          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (<>
               <Modal title="Subscribe"
                    centered visible={makePayment}
                    // onCancel={() => setMakePayment(false)}
                    footer={null}
                    // closable={false}
                  > 
                    <Elements stripe={stripePromise} options={{ clientSecret }}>
                      <CheckoutForm  />
                    </Elements>
                </Modal>
            <div className="row">
              <div className="col-md-3">
                <div className="card card-bordered">
                  <div className="card-body text-center">
                    <div className="mx-auto mb-4">
                      <Avatar
                        style={{
                          backgroundColor: "#e09335",
                          verticalAlign: "middle",
                        }}
                        src={storeData.logo}
                        size={80}
                      >
                        {storeData.name && (
                          <span>{storeData.name.charAt(0).toUpperCase()}</span>
                        )}
                      </Avatar>
                    </div>
                    <h5 className="font-size-15 mb-3 text-dark">
                      {storeData.name}
                    </h5>
                    {/* Store hours status  */}
                    {Object.keys(hours).map((time, index) => {
                        return daysOfTheWeek[index] === moment().format('dddd') &&
                          <div key={index} className="d-flex flex-column py-auto mb-3 mx-auto" style={{ alignContent: "center", width:"80%"}} >   
                          {hours[time] && <>
                                 {hours[time].opens_at < moment().format('H:mm') && moment().format('H:mm') < hours[time].closes_at ?
                              <p className="font-size-16 mb-1 mx-1 font-weight-bold text-capitalized"
                                style={{ backgroundColor: "#008000", color: "white" }}> OPEN </p> : 
                              <p className="font-size-16 mb-1 mx-1 font-weight-bold text-capitalized"
                              style={{ backgroundColor: "#FF0000", color: "white" }}>CLOSED</p>} 
                          </>}
                          </div>
                           })}
                    <h5 className="font-size-12 mb-2 text-dark">
                      `{storeData.street} {storeData.postcode}`
                    </h5>
                    {/* <div className="d-flex flex-row text-dark" style={{justifyContent: "center"}}> */}

                    <h5 className="font-size-12 mb-2 text-dark">
                      Category: <span>{storeCategory.toLocaleUpperCase()}</span>
                    </h5>
                    {/* </div> */}
                    <p className="text-muted">{storeData.email}</p>
                    <p className="text-small">{storeData.description}</p>
                    <Button
                      type="primary"
                      className="my-3"
                      primary
                      onClick={() => setStoreContact(true)}
                    >
                      Update Contact
                    </Button>
                    <Button
                          type="success"
                          onClick={() => {
                            Router.push(`../../discount/${storeData._id}`);
                          }}
                        >
                      Discounts
                    </Button>
                  </div>

                </div>
              </div>
              <div className="col-md-9">
                <div className="row">
                  <div className="col-md-4">
                    <div className="card mini-stats-wid">
                      <div className="card-body">
                        <div className="d-flex">
                          <div className="flex-grow-1">
                            <p className="text-muted fw-medium">Total Orders</p>
                            <h5 className="mb-0">{orders.length} 
                            {/* {console.log(isStoreActive)} */}
                            {/* {moment().format('')} */}
                            
                            </h5>
                          </div>

                          <div className="flex-shrink-0 align-self-center">
                            <div className="mini-stat-icon avatar-sm rounded-circle bg-primary">
                              <span className="avatar-title">
                                <i className="bx bx-cart-alt font-size-24"></i>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="card mini-stats-wid">
                      <div className="card-body">
                        <div className="d-flex">
                          <div className="flex-grow-1">
                            <p className="text-muted fw-medium">Revenue</p>
                            <h5 className="mb-0">£{storeRevenue.toFixed(2)}</h5>
                          </div>

                          <div className="flex-shrink-0 align-self-center">
                            <div className="avatar-sm rounded-circle bg-primary mini-stat-icon">
                              <span className="avatar-title rounded-circle bg-primary">
                                <i className="bx bx-purchase-tag font-size-24"></i>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="card mini-stats-wid">
                      <div className="card-body">
                        <div className="d-flex">
                          <div className="flex-grow-1">
                            <p className="text-muted fw-medium">{storePlan} Plan</p>
                            <h5 className="mb-0">{subID ? subStatus.toUpperCase() : "Not Subscribed"}</h5>
                          </div>

                          <div className="flex-shrink-0 align-self-center">
                            <div className="avatar-sm rounded-circle bg-primary mini-stat-icon">
                              <span className="avatar-title rounded-circle bg-primary">
                                <i className="bx bx-purchase-tag-alt font-size-24"></i>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                {/* Orders */}
                          
                <div className="row">
                  <div className="col-md-auto">
                    <div className="card mini-stats-wid">
                      <div className="card-body">
                        <div className="d-flex">
                          <div className="flex-grow-1">
                            <p className="text-muted fw-medium">New Orders</p>
                            <h5 className="mb-0">{countNew}
                            </h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-auto">
                    <div className="card mini-stats-wid">
                      <div className="card-body">
                        <div className="d-flex">
                          <div className="flex-grow-1">
                            <p className="text-muted fw-medium">Processing</p>
                            <h5 className="mb-0">{countProcessing}
                            </h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-auto">
                    <div className="card mini-stats-wid">
                      <div className="card-body">
                        <div className="d-flex">
                          <div className="flex-grow-1">
                            <p className="text-muted fw-medium">Processed</p>
                            <h5 className="mb-0">{countProcessed}
                            
                            </h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-auto">
                    <div className="card mini-stats-wid">
                      <div className="card-body">
                        <div className="d-flex">
                          <div className="flex-grow-1">
                            <p className="text-muted fw-medium">Dispatched Orders</p>
                            <h5 className="mb-0">{countDispatched}</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-auto">
                    <div className="card mini-stats-wid">
                      <div className="card-body">
                        <div className="d-flex">
                          <div className="flex-grow-1">
                            <p className="text-muted fw-medium">Cancelled Orders</p>
                            <h5 className="mb-0">{countCancelled}</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                 {/* OPERATION HOURS */}
                <div className="card">
                  <div className="card-body">
                    <h2>Operation Hours</h2>
                    <div className="d-flex flex-row">


                      {Object.keys(hours).map((time, index) => {
                        return (
                          <div key={index} className="d-flex flex-column py-auto" style={{ alignContent: "center" }}
                          >
                            {/* {daysOfTheWeek[index] === moment().format('dddd') && (hours[time].opens_at < moment().format('h:mm') &&  moment().format('h:mm') > hours[time].closes_at) ?
                            console.log("Active") : console.log("Inactive")} */}

                            {hours[time] && <>
                              <p className="font-size-16 mb-1 mx-1 font-weight-bold btn text-capitalized"
                                style={{ backgroundColor: "#e09335", color: "white" }}

                              >
                               {daysOfTheWeek[index]}
                               {/* {hours[time].day} */}
                               {/* {setOpens(hours[time].opens_at)}
                               {setClose(hours[time].closes_at)} */}
                               
                              </p>
                              <div className="mx-2">
                                <p className="font-size-18 text-dark font-weight-bold d-flex flex-row">
                                  <span>{hours[time].opens_at}</span> -
                                  <span>{hours[time].closes_at}</span>
                                </p>
                              </div>
                            </>}
                          </div>
                        )
                      }
                      )}
                      
                     
                    </div>
                    {/* {Object.keys(hours).map((time, index) => {
                        return daysOfTheWeek[index] === moment().format('dddd') &&
                          <div key={index} className="d-flex flex-column py-auto" style={{ alignContent: "center" }} >   
                          
                          {hours[time] &&
                               <>
                              <p className="font-size-16 mb-1 mx-1 font-weight-bold btn text-capitalized"
                                style={{ backgroundColor: "#e09335", color: "white" }}

                              >
                               {hours[time].opens_at < moment().format('hh:m') && moment().format('hh:m') < hours[time].closes_at ? "Active" : "Inactive" }
                               
                              </p>
                            </>}
                          </div> }
                        ) } */}
                  </div>
                </div>

              </div>

              <div className="col-md-6">
                {orders.length === 0 ? '' :
                  <div className="col-md-12 mb-3">
                    <Tabs defaultActiveKey="new">
                      <TabPane tab={`Orders`} key="new">
  <Table
            loading={loading}
            columns={orderColumns}
            dataSource={orders}
            pagination={{
              current: page,
              pageSize: pageSize,
              total: orders.length,
              onChange: (page, pageSize)=>{
                setPage(page);
                setPageSize(pageSize);
              }
            }
            }
            /> 
                      </TabPane>
                    </Tabs>
                  </div>
                }
                </div>
            
              <div className="col-md-6">
              {products.length === 0 ? '' :
                <div className="col-md-12 mt-3">
                <Tabs defaultActiveKey="published">
                  <TabPane tab={`Published products`} key="published">

                    <Table
                      loading={loading}
                      columns={productColumns}
                      dataSource={products}
                      pagination={{
                        current: page,
                        pageSize: pageSize,
                        total: products.length,
                        onChange: (page, pageSize) => {
                          setPage(page);
                          setPageSize(pageSize);
                        }
                      }
                      }
                    />


                  </TabPane>
                  <TabPane tab={`Unpublished products`} key="unpublished">


                    <Table
                      loading={loading}
                      columns={productColumns}
                      dataSource={unpublished}
                      pagination={{
                        current: page,
                        pageSize: pageSize,
                        total: unpublished.length,
                        onChange: (page, pageSize) => {
                          setPage(page);
                          setPageSize(pageSize);
                        }
                      }
                      }
                    />

                  </TabPane>
                </Tabs>
            </div>
              }
              </div>


            </div>
            </>
          )}
        </div>
      </DashboardLayout>

      {/* Change Store Info Modal*/}
      <Modal
        title="Update store Info"
        centered
        visible={storeInfo}
        onCancel={() => setStoreInfo(false)}
        footer={null}
      >
        <Form
          form={form}
          name="update_store"

          onFinish={updateStoreInfo}
          closable={true}
          layout="vertical"
        >
          <Form.Item
            name="name"
            label="Store name">
            <Input
              // defaultValue={storeData.name}
              placeholder={storeData.name}
              className="inputWidthFull"
              label="Store name"
            />
          </Form.Item>
          <Form.Item
            name="mile_radius"
            label="Mile radius">
            <InputNumber
              placeholder={storeData.mile_radius}
              className="inputWidthFull"
              label="Mile radius"

            />
          </Form.Item>

          <Form.Item
            name="featured"
            label="Feature Status">

            <Select
              // defaultValue={storeData.featured}
              placeholder={storeData.featured === 0 ? 'Unfeatured' : 'Featured'}

            >
              <Option value={0} >Unfeature store</Option>
              <Option value={1}>Feature store</Option>
            </Select>
          </Form.Item>

          <Form.Item name="category" label="Categories">
            <Select
              // defaultValue={storeCategory}
              placeholder={storeCategory}
               
            >
              {categories && categories.map((cat, index) => (
                <Option key={index} value={cat.id}>{cat.name}</Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"
            >
              Update profile
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => {
                setStoreInfo(false)
                onReset
              }}
            >
              Cancel
            </Button>
          </Form.Item >
        </Form>
      </Modal>


      {/*Update Store Subscription*/}
      <Modal
        title="Update store Subcription"
        centered
        visible={subcription}
        onCancel={() => setSubcription(false)}
        footer={null}
      >
        <Form
          form={form}
          name="update_store"
          onFinish={updateSub}
          // onFinish={checkForm}
          closable={true}
          layout="vertical"
        >
          <h3>{storePlan}</h3>
          <Form.Item
            name="paid"
            label="Subscription status"
          >

            <Select
              placeholder="Store subscription"
            // defaultValue={storeData.featured}
            >
              <Option value={true}>Subscribe</Option>
              <Option value={false}>Unsubscribe</Option>
            </Select>
          </Form.Item>

          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"
            >
              Update profile
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => setSubcription(false)}
            >
              Cancel
            </Button>
          </Form.Item >
        </Form>
      </Modal>

      {/* UPDATE STORE ADDRESS */}
      <Modal
        title="Change store address"
        centered
        visible={storeAddress}
        width={500}
        onCancel={() => setStoreAddress(false)}
        footer={null}
      >
        <Form
          form={form}
          size="large"
          name="address"
          onFinish={submitAddress}
          layout="vertical"
        >
          <Form.Item
            name="street"
            label="Address">
            <Input
              // defaultValue={storeData.street}
              className="inputWidthFull"
              label="Address"
            />
          </Form.Item>
          <Form.Item
            name="postcode"
            label="Postcode"
          >

            <Input
              label="Post Code"
              placeholder="ubf 4rf" />
          </Form.Item>
          <Form.Item
            name="latitude"
            label="Latitude"
          >

            <InputNumber
              className="inputWidthFull"
              label="Latitude"
              type="number"
              placeholder="52.5000054" />
          </Form.Item>
          <Form.Item
            name="longitude"
            label="Longitude"
          >
            <InputNumber
              className="inputWidthFull"
              label="Longitude"
              type="number"
              placeholder="-1.9795775" />
          </Form.Item>
          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"
            >
              Update profile
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => setStoreAddress(false)}
            >
              Cancel
            </Button>
          </Form.Item>
        </Form>
      </Modal>

      {/* UPDATE STORE CONTACT */}
      <Modal
        title="Change store contact"
        centered
        visible={storeContact}
        width={500}
        onCancel={() => setStoreContact(false)}
        footer={null}
      >
        <Form
          form={form}
          size="large"
          name="name"
          onFinish={updateStoreConfig}
          layout="vertical"
        >
          <Form.Item
            name="name"
            label="Fullname">
            <Input
              defaultValue={storeData.name}
              className="inputWidthFull"
              label="Fullname"
            />
          </Form.Item>

          <Form.Item
            name="email"
            label="E-mail">
            <Input
              defaultValue={storeData.email}
              className="inputWidthFull"
              label="E-mail"
            />
          </Form.Item>
          <Form.Item
            name="phone"
            label="Phone">
            <Input
              defaultValue={storeData.phone}
              className="inputWidthFull"
              label="Phone"
            />
          </Form.Item>
          <Form.Item
            name="logo"
            valuePropName="fileList"
            getValueFromEvent={addContactImage}
          >
            <Upload name="logo"
              listType="picture"
              maxCount={1}>
              <Button icon={<UploadOutlined />}>Click to upload</Button>
            </Upload>
          </Form.Item>
          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"
            >
              Update Contact
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => setStoreContact(false)}
            >
              Cancel
            </Button>
          </Form.Item>
        </Form>
      </Modal>


      {/* ADD PRODUCT TO STORE */}
      <Modal
        title="Add a new product"
        centered
        visible={addNewProduct}
        onCancel={() => setAddNewProduct(false)}
        width={"800px"}
        footer={null}
      >
        <Form
          form={form}
          name="add_product"
          onFinish={addProduct}
          layout="vertical"
        >
          <Form.Item name="title" label="Product title"
            rules={[{ required: true, message: 'Please input the product title' }]}
          >
            <Input
              className="inputWidthFull"
              label="Product title"
            />
          </Form.Item>

          <Form.Item name="sku" label="SKU"
            rules={[{ required: true, message: 'Please input the product SKU' }]}
          >
            <Input
              className="inputWidthFull"
              label="SKU"
            />
          </Form.Item>

          <Form.Item name="price" label="Price"
            rules={[{ required: true, message: 'Please input the price' }]}
          >
            <InputNumber
              name="price"
              className="inputWidthFull"
              label="price"
            />
          </Form.Item>

          <Form.Item name="quantity" label="Quantity"
            rules={[{ required: true, message: 'Please select quantity' }]}
          >
            <InputNumber
              className="inputWidthFull"
              label="quantity"
            />
          </Form.Item>

          <Form.Item name="category" label="Categories"
            // rules={[{ required: true, message: 'Please select a pproduct category' }]}
          >
            <Select
              mode="multiple"
              placeholder={categoryID}
            >
              {categories && categories.map((category, index) => (
                <Option key={index} value={category.id}>{category.name}</Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            name="image"
            valuePropName="fileList"
            getValueFromEvent={addProductImage}
          >
            <Upload name="image"
              listType="picture"
              multiple>
              <Button icon={<UploadOutlined />}>Click to upload</Button>
            </Upload>
          </Form.Item>

          <Form.Item name="description"
            // rules={[{ required: true, message: 'Please enter product description' }]}
          >
            <TextArea rows={4}
              placeholder="Product description"
              label="Description"
            />

          </Form.Item>

          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"

              onClick={() => {
                onReset
                setAddNewProduct
              }}
            >
              Add Product
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={onReset}
            >
              Reset
            </Button>
          </Form.Item >
        </Form>
      </Modal>



      {/* Subscribe a Store Free*/}
      <Modal
        title="Add Subscription"
        centered
        visible={addSub}
        onCancel={() => setAddSub(false)}
        width={"250px"}
        footer={null}
      >
        <Form
          form={form}
          name="add_subscription"
          // onFinish={addSubFree}
          layout="vertical"
        >


          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"
              onClick={() => {setPaidSub(true)
                setAddSub(false)
              }}
            >
              Paid
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => {setFreeSub(true)
                setAddSub(false)
              }}
            >
              Free
            </Button>
          </Form.Item >
        </Form>
      </Modal>

      {/* Subscribe a Store Free*/}
      <Modal
        title="Add free subscription"
        centered
        visible={freeSub}
        onCancel={() => setFreeSub(false)}
        // width={"800px"}
        footer={null}
      >
        <Form
          form={form}
          name="add_subscription"
          onFinish={() => {
            addSubFree()
            Router.push(`/stores/payment/${storeID}`)
          }}
          layout="vertical"
        >
          <Form.Item name="plan" label="Select a plan"
            rules={[{ required: true, message: 'Please select a select plan' }]}
          >
            <Select
              placeholder="Select a plan"
            >
              {plans && plans.map((plan, index) => (
                <Option key={index} value={plan.id}>{plan.name}</Option>
              ))}
            </Select>
          </Form.Item>

          {/* <Form.Item name="paid" label="Payment status"
            rules={[{ required: true, message: 'Please select a payment status' }]}
          >
            <Select
              placeholder="Paid"
            >
              <Option value={true}>Paid</Option>
              <Option value={false}>Not paid</Option>

            </Select>
          </Form.Item> */}

          {/* <Form.Item name="duration" label="Subscription duration"
            rules={[{ required: true, message: 'Please select the subscription duration' }]}
          >
            <Select
              placeholder="Duration"
            >
              <Option value="monthly">Monthly</Option>
              <Option value="annually">Annually</Option>

            </Select>
          </Form.Item> */}

          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"

            // onClick={() =>{
            //   onReset
            //   setAddNewProduct
            // }}
            >
              Subscribe
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => {
                onReset()
                setFreeSub(false)
              }}
            >
              Close
            </Button>
          </Form.Item >
        </Form>
      </Modal>


      {/* Subscribe a Store Paid*/}
      <Modal
        title="Add paid subscription"
        centered
        visible={paidSub}
        onCancel={() => setPaidSub(false)}
        // width={"800px"}
        footer={null}
      >
        <Form
          form={form}
          name="add_subscription"
          onFinish={addSubPaid}
          layout="vertical"
        >
          <Form.Item name="plan" label="Select a plan"
            rules={[{ required: true, message: 'Please select a select plan' }]}
          >
            <Select
              placeholder="Select a plan"
            >
              {plans && plans.map((plan, index) => (
                <Option key={index} value={plan.id}>{plan.name}</Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name="coupon"
                      label="Enter coupon code"
                      // rules={[
                      //   {
                      //     required: true,
                      //     message: "Enter coupon code!",
                      //   },
                      // ]}
                      >
                      <Input
                      placeholder="Enter coupon code"
                        className="inputWidthFull"
                        label="Coupon code"

                      />
                    </Form.Item>


    {/* <Form.Item name="duration" label="Subscription duration"
            rules={[{ required: true, message: 'Please select the subscription duration' }]}
          >
            <Select
              placeholder="Duration"
            >
              <Option value="monthly">Monthly</Option>
              <Option value="annually">Annually</Option>

            </Select>
          </Form.Item> */}

          {/* <Form.Item name="paid" label="Payment status"
            rules={[{ required: true, message: 'Please select a payment status' }]}
          >
            <Select
              placeholder="Paid"
            >
              <Option value={true}>Paid</Option>
              <Option value={false}>Not paid</Option>

            </Select>
          </Form.Item> */}

          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"

            // onClick={() =>{
            //   onReset
            //   setAddNewProduct
            // }}
            >
              Subscribe
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => {
                onReset()
                setPaidSub(false)
              }}
            >
              Close
            </Button>
          </Form.Item >
        </Form>
      </Modal>


      {/* ADD MUTIPLE PRODUCTS TO STORE */}
      {/*                 
                  <Modal
                  title="Add a new product"
                  centered
                  visible={addNewProduct}
                  onCancel={() => setAddNewProduct(false)}
                  width={"800px"}
                  footer={null}
                >
                   <Form
                        form={form}
                        name="add_product"
                        onFinish={handleSubmit}
                        layout="vertical"
                      >
                        {addProducts.map((e, index) => (
                          <div key={index}>
                        <Form.Item  name="title" label="Product title"
                          rules={[{ required: true, message: 'Please input the product title' }]}
                          >
                          <Input
                            onChange={e => handleChange(index, e)}
                            className="inputWidthFull"
                            label="Product title"
                          />
                          </Form.Item>

                        <Form.Item name="sku" label="SKU"
                          rules={[{ required: true, message: 'Please input the product SKU' }]}
                          >
                          <Input
                            onChange={e => handleChange(index, e)}
                            className="inputWidthFull"
                            label="SKU"
                          />
                        </Form.Item>

                        <Form.Item name="price" label="Price"
                          rules={[{ required: true, message: 'Please input the price' }]}
                            onChange={e => handleChange(index, e)}
                          >
                          <InputNumber
                          name="price"
                            className="inputWidthFull"
                            label="price"
                          />
                        </Form.Item>

                        <Form.Item name="quantity" label="Quantity"
                          rules={[{ required: true, message: 'Please select quantity' }]}
                            onChange={e => handleChange(index, e)}
                          >
                          <InputNumber
                            className="inputWidthFull"
                            label="quantity"
                            name="quantity"
                          />
                        </Form.Item>

                        <Form.Item name="category" label="Categories"
                          rules={[{ required: true, message: 'Please select a pproduct category' }]}                          
                          >
                          <Select
                            mode="multiple"
                            placeholder="Choose category"
                            name="category"
                            onChange={handleChange(index, e)}
                          >
                            {/* {categories && categories.map((category, index) => (
                              <Option key={index} value={category.id}>{category.name}</Option>
                            ))} 
                           
                            <Option value="one">One</Option>
                            <Option value="two">Two</Option>
                          </Select>
                        </Form.Item>

                        <Form.Item
                          name="image"
                          valuePropName="fileList"
                          getValueFromEvent={addProductImage}
                        >
                          <Upload name="image"
                            listType="picture"
                            multiple>
                            <Button icon={<UploadOutlined />}>Click to upload</Button>
                          </Upload>
                        </Form.Item>
                        
                        <Form.Item name="description"
                          rules={[{ required: true, message: 'Please enter product description' }]}
                          >
                          <TextArea rows={4}
                           onChange={e => handleChange(index, e)}
                           placeholder="Product description"
                            label="Description"
                            name="description"
                          />

                        </Form.Item>
                            {index ? <Form.Item>
                              <Button
                                className="mt-2 mx-3"
                                type="primary"
                                onClick={() => removeFormFields(index)}
                              >
                                Remove
                              </Button>
                            </Form.Item> : null
                            }
                        </div>
                          ))}
                        <Form.Item>
                        <Button
                            className="mt-2 mx-3"
                            type="primary"
                            onClick={() => addFormFields()}
                          >
                            Add form
                          </Button>

                          <Button
                            loading={loading}
                            htmlType="submit"
                            className="mt-2"
                            type="primary"
                            
                          onClick={() =>{
                            onReset
                            setAddNewProduct
                          }}
                          >
                            Add Product
                          </Button>
                          <Button
                            className="mt-2 mx-3"
                            type="primary"
                            onClick={onReset}
                          >
                            Reset
                          </Button>
                        </Form.Item >
                      </Form>
                </Modal>              
               */}
    </>
  );
};

import { PaymentElement } from "@stripe/react-stripe-js";
import { useStripe, useElements, CardElement } from "@stripe/react-stripe-js";


function CheckoutForm() {
  const stripe = useStripe();
  const elements = useElements();

  const [message, setMessage] = useState(null);
  const [isProcessing, setIsProcessing] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    // const cardElement = elements.getElement(CardElement);
    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make sure to disable form submission until Stripe.js has loaded.
      return;
    }

    setIsProcessing(true);

    const { error } = await stripe.confirmPayment({
      elements,
      confirmParams: {
        // Make sure to change this to your payment completion page
        return_url: `${window.location}`,
      },
    });

    if (error.type === "card_error" || error.type === "validation_error") {
      setMessage(error.message);
    } else {
      setMessage("An unexpected error occured.");
    }

    setIsProcessing(false);
  };

  return (
    <form id="payment-form" onSubmit={handleSubmit}>
      <PaymentElement id="payment-element" />
      {/* <CardElement id="payment-element"/> */}
      <button disabled={isProcessing || !stripe || !elements} id="submit" style={{backgroundColor: "blue", width: "100%", padding: "8px", marginTop:"8px", textAlign: "center",  borderRadius: "8px", border: "solid 0.5px white"}}>
        <span id="button-text"style={{color: "white"}} >
          {isProcessing ? "Processing ... " : "Pay now"}
        </span>
      </button>
      {/* Show any error or success messages */}
      {message && <div id="payment-message">{message}</div>}
    </form>
  );
}

StoreDetails.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default StoreDetails;
