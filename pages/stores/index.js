import { useState, useEffect } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  getAllStores,
} from "../../helper/redux/actions/app";
import { useAsync } from "react-use";
import Router from "next/router";
import { notification,  Button,  Badge,Tabs, Table, AutoComplete } from "antd";

const { TabPane } = Tabs;

export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [stores, setStores] = useState([]);

  const [suggestions, setSuggestions] = useState([]);
  const [text, setText] = useState('');

  
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);

 
  const onChange = (text) => {
    let matches = []
    if(text.length > 0){
      matches = stores.filter(store => {
        const regex = new RegExp(`${text}`, "gi");
        return store.name.match(regex)
      })
    }
    console.log("matches: " + matches)
    setSuggestions(matches)
    setText(text);
  };
  
  // Function to getAllStores()
  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getAllStores())
        .then((response) => {
          setStores(response.data);
          
          console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
          console.log(stores)
        });
    } finally {
      setLoading(false);
    }
  }, []);

  //  Order Table
  const storeColumns = [
    {
      key: 1,
      title: 'Date',
      dataIndex: 'created_at',
      render:  (created_at)=> (<span className="text-small">
        {moment(created_at).format(
              "ddd, MMM Do YYYY"
                      )}
                      </span>)
    },
    {
      key: 2,
      title: 'Store Name',
      dataIndex: 'name'
    },
    {
      key: 3,
      title: 'Address',
      dataIndex: 'street'
    },
    {
      key: 4,
      title: 'Post Code',
      dataIndex: 'postcode',
    },
    {
      key: 4,
      title: 'Subscription',
      dataIndex: 'subscription',
      render: (record) => (
        <span>{record?.status}</span>
      )
    },
    {
      title: "View Store",
      key: 5,
      dataSource: "id",
      render: (record) => (
        <Button type="primary" onClick={() => Router.push(`stores/store/${record._id}`)}>
          View Store          
        </Button>
      )
    }
  ];

  return (
    <>
      <Head title="Stores" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Stores" page="Store">
            {/* <Link href="/partners/add-partner">
              <a>
                <Button type="primary">Add Partner</Button>
              </a>
            </Link> */}
           <form className="app-search  px-3 d-none d-lg-block">
                <div className="position-relative">
                  <input
                    type="text"
                    onChange={e=> onChange(e.target.value)}
                    value={text}
                    className="form-control"
                    placeholder="Search..."
                  />
                  <span className="bx bx-search-alt"></span>
                </div>
              </form>
              <div>
                {text}
              </div>
              {suggestions && suggestions.map((suggestion, index) =>{
                  <div key={index}>
                    {suggestion.name}
                  </div>
                })}
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (          
             <div className="row">
                 <div className="col-md-12">
                {/* <div className="table-responsive">
                  <table className="table align-middle table-nowrap mb-0">
                    <thead className="table-light">
                      <tr>
                        <th className="align-middle">S/N</th>
                        <th className="align-middle">Store Name</th>
                        <th className="align-middle">Address</th>
                        <th className="align-middle">Post Code</th>
                        <th className="align-middle">View Store</th>
                      </tr>
                    </thead>
                    <tbody>
                      {stores.map((store, index) => (
                          <tr key={index}>
                            <td>
                              <a
                                href="javascript: void(0);"
                                className="text-body fw-bold"
                              >
                                {index + 1}
                              </a>
                            </td>
                            <td>{store.name}</td>
                            <td>{store.street}</td>
                            <td>{store.postcode}</td>
                            <td>
                            <Button
                          type="primary"
                          onClick={() => {
                            Router.push(`stores/store/${store._id}`);
                            console.log(store._id)
                          }}
                        >
                          View Store
                        </Button>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div> */}
                
                <Table
            loading={loading}
            columns={storeColumns}
            dataSource={stores}
            pagination={{
              current: page,
              pageSize: pageSize,
              total: stores.length,
              onChange: (page, pageSize)=>{
                setPage(page);
                setPageSize(pageSize);
              }
            }
            }
            /> 

                
              </div>
             </div>
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
