import { useState } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import {
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { Button, Modal, Spin, Switch, Tooltip } from "antd";
import {
  actionRemovePrivilege,
  actionUpdateRoleStatus,
  getUsers,
} from "../../helper/redux/actions/app";
import { useDispatch } from "react-redux";
import { useAsync } from "react-use";
import moment from "moment";

export default function Index() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [users, setUsers] = useState([]);
  const [pageLoading, setPageLoading] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);

  const { confirm } = Modal;

  const getAdminRole = (role) => {
    return role.replace(/_/g, " ");
  };

  const getAllUsersData = async () => {
    try {
      setLoading(true);
      await dispatch(await getUsers())
        .then((response) => {
          setLoading(false);
          setUsers(response.data);
        })
        .catch((error) => {
          setLoading(false);
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  };

  const statusRoleChange = async (id, status) => {
    const payload = {
      user_id: id,
      role: status ? "manager" : "user",
    };

    if (!!status) {
      makeManager(payload);
    } else {
      removePrivilege(payload);
    }
  };

  const makeManager = (payload) => {
    confirm({
      title: "Confirm Update",
      icon: <ExclamationCircleOutlined />,
      content: "This action will make this user a manager. Continue?",
      async onOk() {
        try {
          setPageLoading(true);

          await dispatch(await actionUpdateRoleStatus({ payload }))
            .then((response) => {
              setPageLoading(false);
              getAllUsersData();
              notification.success({
                message: "Manager Role",
                description: "Manager role updated successfully",
              });
            })
            .catch((error) => {
              setPageLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() {},
    });
  };

  const removePrivilege = (payload) => {
    confirm({
      title: "Confirm Downgrade",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to remove this user's manager privilege?",
      async onOk() {
        try {
          setPageLoading(true);
          await dispatch(await actionRemovePrivilege({ payload }))
            .then((response) => {
              setPageLoading(false);
              notification.success({
                message: "Manager Privilege",
                description: "Privilege removed successfully",
              });
              getAllUsersData();
            })
            .catch((error) => {
              setPageLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() {},
    });
  };

  useAsync(async () => {
    await getAllUsersData();
  }, []);

  const confirmRemove = () => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to remove this user",
      async onOk() {},
      onCancel() {},
    });
  };

  const getRole = (user) => {
    const role = [];
    if (!!user.partner) {
      role.push("Partner");
    }
    if (!!user.customer) {
      role.push("Customer");
    }
    if (!!user.admin) {
      role.push("Admin");
    }

    return role.join();
  };

  return (
    <>
      <Head title="All Users" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Users" page="users">
            {/* <Link href="/users/add-user">
              <a>
                <Button type="primary">Add User</Button>
              </a>
            </Link> */}
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
              <div className="table-responsive">
                <table className="table align-middle table-nowrap mb-0">
                  <thead className="table-light">
                    <tr>
                      {/* <th className="align-middle">S/N</th> */}
                      <th className="align-middle">Date added</th>
                      <th className="align-middle">Full name</th>
                      <th className="align-middle">Role</th>
                      <th className="align-middle">Email addres</th>
                      <th className="align-middle">Mobile number</th>
                      <th className="align-middle">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {users.map((user, index) => (
                      <tr key={index}>
                        {/* <td>{index + 1}</td> */}
                        <td>
                          <span className="text-small">
                            {moment(user.created_at).format("ddd, MMM Do YYYY")}
                          </span>
                        </td>
                        <td>{user.name}</td>
                        <td>{getRole(user)}</td>
                        <td>{user.email}</td>
                        <td>{user.phone ?? "None yet"}</td>
                        <td>
                          <Switch
                            style={{ marginLeft: "10px" }}
                            checkedChildren="Admin"
                            unCheckedChildren="User"
                            defaultChecked={
                              user?.admin?.role === "manager" ||
                              user?.admin?.role === "super_admin"
                                ? true
                                : false
                            }
                            onChange={(value) => {
                              statusRoleChange(user.id, value);
                            }}
                          />

                          {/* &nbsp;
                          
                          <Tooltip title="Remove user">
                            <Button
                              onClick={() => {
                                confirmRemove(manager.id);
                              }}
                              type="danger"
                              icon={<CloseCircleOutlined />}
                            />
                          </Tooltip> */}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </Spin>
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
