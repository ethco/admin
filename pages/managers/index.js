import { useState } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  getAllManagers,
  actionUpdateStatus,
  actionUpdateRoleStatus,
  actionRemovePrivilege,
} from "../../helper/redux/actions/app";
import { useAsync } from "react-use";
import Router from "next/router";
import {
  notification,
  Avatar,
  Button,
  Modal,
  Badge,
  Switch,
  Spin,
  Tooltip,
} from "antd";

export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [managers, setManagers] = useState([]);
  const [pageLoading, setPageLoading] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const { confirm } = Modal;

  const getAdminRole = (role) => {
    return role.replace(/_/g, " ");
  };

  const getAllManagersData = async () => {
    try {
      setLoading(true);
      await dispatch(await getAllManagers())
        .then((response) => {
          setManagers(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  };

  console.log(managers)
  const statusRoleChange = async (id, status) => {
    try {
      setPageLoading(true);
      const payload = {
        user_id: id,
        role: status ? "super_admin" : "manager",
      };
      await dispatch(await actionUpdateRoleStatus({ payload }))
        .then((response) => {
          setPageLoading(false);
          getAllManagersData();
          notification.success({
            message: "Manager Role",
            description: "Manager role updated successfully",
          });
        })
        .catch((error) => {
          setPageLoading(false);
        });
    } finally {
      setLoading(false);
    }
  };

  const removeManager = (user_id) => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to remove this privilege?",
      async onOk() {
        try {
          setPageLoading(true);
          const payload = { user_id };

          await dispatch(await actionRemovePrivilege({ payload }))
            .then((response) => {
              setPageLoading(false);
              notification.success({
                message: "Manage Privilege",
                description: "Privilege removed successfully",
              });
              getAllManagersData();
            })
            .catch((error) => {
              setPageLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() {},
    });
  };

  useAsync(async () => {
    await getAllManagersData()
  }, []);

  return (
    <>
      <Head title="Manager" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Managers" page="managers"></PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
              <table className="table align-middle table-nowrap mb-0">
                <thead className="table-light">
                  <tr>
                    <th className="align-middle">Date</th>
                    <th className="align-middle">Name</th>
                    <th className="align-middle">Status</th>
                    <th className="align-middle">Role</th>
                    <th className="align-middle">Contact</th>
                    <th className="align-middle">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {managers.map((manager, index) => (
                    <tr key={index}>
                      <td>
                        <span className="text-small">
                          {moment(manager.created_at).format(
                            "ddd, MMM Do YYYY"
                          )}
                        </span>
                      </td>
                      <td>
                        <p className="mb-1">{manager.name}</p>
                      </td>
                      <td>
                        <Badge
                          color={
                            manager.admin && manager.admin.status === "active"
                              ? "green"
                              : "red"
                          }
                          text={manager.admin && manager.admin.status}
                        />
                      </td>
                      <td className="text-capitalize">
                        {manager.admin && getAdminRole(manager.admin.role)}
                      </td>

                      <td>
                        <p className="text-small mb-1">
                          <a href={`mailto:${manager.email}`}>
                            {manager.email}
                          </a>
                        </p>
                        <p className="text-small">
                          <a href={`tel:${manager.phone}`}>{manager.phone}</a>
                        </p>
                      </td>
                      <td>
                        {/* <Button
                          type="primary"
                          onClick={() => {
                            Router.push(`/partners/${manager.id}`);
                          }}
                        >
                          View Partner
                        </Button> */}

                        <Tooltip title="Remove privilege">
                          <Button
                            onClick={() => {
                              removeManager(manager.id);
                            }}
                            type="danger"
                            icon={<CloseCircleOutlined />}
                          />
                        </Tooltip>

                        <Switch
                          style={{ marginLeft: "10px" }}
                          checkedChildren="Admin"
                          unCheckedChildren="Manager"
                          checked={
                            manager.admin.role === "super_admin" ? true : false
                          }
                          onChange={(value) => {
                            statusRoleChange(manager.id, value);
                          }}
                        />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </Spin>
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
