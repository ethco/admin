import { useState, useEffect } from "react";
import DashboardLayout from "../../../layouts/DashboardLayout";
import Head from "../../../components/Head";
import Link from "next/link";
import { Modal, Button, Input } from "antd";
import PageHeader from "../../../components/PageHeader";
import PageLoading from "../../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
    getProductByName,
} from "../../../helper/redux/actions/app";

const AddPartner = ({ id }) => {
  const [loading, setLoading] = useState(true);
  const [product, setProduct] = useState({});
  const dispatch = useDispatch();




  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getProductByName(id))
        .then((response) => {
          setProduct(response.data);
          console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    console.log(product)
    }, [])
  return (
    <>
      <Head title={product.title || ""} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader  title="All Orders" page="Orders" page1="Order" page2="Product info" pageLink="orders">
          
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <div className="row">
              <div className="col-md-12">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title mb-4">Product Information</h4>
                    <div className="table-responsive">
                      <table className="table  table-nowrap mb-0">
                        <tbody>
                          <tr>
                            <th className="row">Date Created</th>
                            <td>
                              {moment(product.created_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                          </tr>
                          <tr>
                            <th className="row">Product Name</th>
                            <td>{product.title}</td>
                          </tr>
                          <tr>
                            <th className="row">Product ID</th>
                            <td>{product.product_id}</td>
                          </tr>
                          <tr>
                            <th className="row">About Description</th>
                            <td>{product.meta_data}</td>
                          </tr>
                          <tr>
                            <th className="row">Price</th>
                            <td>{`£${product.price}`}</td>
                          </tr>
                          <tr>
                            <th className="row">Quantity</th>
                            <td>{product.quantity}</td>
                          </tr>
                          <tr>
                            <th className="row">Unit</th>
                            <td>{product.unit}</td>
                          </tr>
                          {/* Display status */}
                          {product.status && product.status.status === "cancelled" ?
                          <>
                          <tr>
                            <th className="row">Status</th>
                            <td>{product.status.status}</td>
                          </tr>
                          <tr>
                          <th className="row">Status</th>
                          <td>{product.status}</td>
                        </tr>
                        </> :
                        <tr>
                            <th className="row">Status</th>
                            <td>{product.status.status}</td>
                          </tr>
                              }
                          <tr>
                            <th className="row">Date Updates</th>
                            <td>
                              {moment(product.updated_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </DashboardLayout>
    </>
  );
};

AddPartner.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default AddPartner;
