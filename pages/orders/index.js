import { useState, useEffect } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  getAllOrders,
  // getOrderStatus,
} from "../../helper/redux/actions/app";
import { useAsync } from "react-use";
import Router from "next/router";
import { notification,  Button,  Badge,Tabs, Table} from "antd";
import { render } from "react-dom";

const { TabPane } = Tabs;

export default function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [orders, setOrders] = useState([]);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);
  // const [pageLoading, setPageLoading] = useState(false);

  
  
  // Same Function with getAllStoreData()
  
    const getAllStoreData = async () =>{
    try {
      setLoading(true);
      await dispatch(await getAllOrders())
        .then((response) => {
          setOrders(response.data);
          console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get all orders, try again",
          });
          console.log(orders)
        });
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getAllStoreData()
  },[])
  console.log(orders)
// Setting the Table Columns


  const columns = [
    {
      key: 1,
      title: 'Date',
      dataIndex: 'created_at',
      render:  (created_at)=> (<span className="text-small">
        {moment(created_at).format(
              "ddd, MMM Do YYYY"
                      )}
                      </span>)
    },
    {
      key: 1,
      title: 'Name',
      dataIndex: 'pickup',
      render: (pickup)=> pickup.name
    },
    {
      key: 2,
      title: 'Status',
      dataIndex: 'status',
      render: (status => <Badge
      color={status.status === "dispatched" ? "blue" : 
      status.status === "new" ? "green" : 
      status.status === 'processed' ? "green" : 
      status.status === "processing" ? "yellow" 
      : "red"}
      text={status.status.toUpperCase()}
    />),
    filters:[
      {text:"NEW", value:"new"},
      {text:"PROCESSING", value:"processing"},
      {text:"PROCESSED", value:"processed"},
      {text:"DISPATCHED", value:"dispatched"},
      {text:"CANCELLED", value:"cancelled"}
    ],
    onFilter:(filterStatus, record)=>{
      console.log(record.status.status)
      console.log(filterStatus)
      return record.status.status === filterStatus
    }
    },
    {
      key: 1,
      title: 'Reference',
      dataIndex: 'reference'
    },
    {
      key: 1,
      title: 'Address',
      dataIndex: 'pickup',
      render: (pickup)=> pickup.address
    },
    {
      key: 3,
      title: 'Email',
      dataIndex: 'pickup',
      render: (pickup)=> pickup.email
    },
    {
      title: "View Order",
      key: "action",
      dataSource: "user",
      render: (record) => (
        <Button type="primary" onClick={() => Router.push(`/orders/${record._id}`)}>
          View Order
          
        </Button>
      )
    }
  ];

  return (
    <>
      <Head title="Orders" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Orders" page="orders">
          
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Table
            loading={loading}
            columns={columns}
            dataSource={orders}
            pagination={{
              current: page,
              pageSize: pageSize,
              total: orders.length,
              onChange: (page, pageSize)=>{
                setPage(page);
                setPageSize(pageSize);
              }
            }
            }
            />   

                   
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
