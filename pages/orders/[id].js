import { useState, useEffect } from "react";
import DashboardLayout from "../../layouts/DashboardLayout";
import Head from "../../components/Head";
import Link from "next/link";
import { Modal, Button, Avatar, Input, Select, InputNumber, Form, notification } from "antd";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
  getAllOrdersByName,
  updateDeliveryAddress,
updateOrderStat,
  actionCancelOrder,
  actionRefundAmount,
  pushToStore,
  getAllStores,
  getUsers,
  getOrderHistory,
  trackOrder,
  changeDriver,
  assignDriverToOrder,
  getDrivers,
  getDriverAssignment,
} from "../../helper/redux/actions/app";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { assign } from "lodash";

const EditOrder = ({id}) => {
  const [loading, setLoading] = useState(true);
  const [cancelLoading, setCancelLoading] = useState(false);
  const [addressLoading, setAddressLoading] = useState(false)
  const [updateOrderLoading, setUpdateOrderLoading] = useState(false)
  
  const [storeLoading, setStoreLoading] = useState(false)
  const [assignDriver, setAssignDriver] = useState(false)
  const [reassignDriver, setReassignDriver] = useState(false)
  const [orderRefund, setOrderRefund] = useState(false)
  const [totalAmount, setTotalAmount] = useState('')
  const [open, setOpen] = useState(false);

  const [orderData, setOrderData] = useState({});
  const [storeList, setStoreList] = useState([]);
  const [tracker, setTracker] = useState([]);
  const [users, setUsers] = useState([]);
  const [submitDecline, setSubmitDecline] = useState(false);
  const [reason, setReason] = useState("");
  const [onEdit, setOnEdit] = useState(false);
  const [onUpdateOrder, setOnUpdateOrder] = useState(false);
  const [changeStore, setChangeStore] = useState(false);
  const [activeButton, setActiveButton] = useState(false);
  
  const [refundAmount, setRefundAmount] = useState(0);
  const [refund, setRefund] = useState(0);
  const [reference, setReference] = useState("");

  // DRIVERS STATES
  const [driverList, setDriverList] = useState([]);
  const [driverInfo, setDriverInfo] = useState({});
  const [assignmentHistory, setAssignmentHistory] = useState({});

  useAsync(async () => {
    await getAllOrdersData();
    await getAllDrivers()
    await getAssignedDriver()
    await getAssignmentHistory()
  }, []);



  const dispatch = useDispatch();
  const { confirm, warning } = Modal;
  const { Option } = Select;
  const { TextArea } = Input;
  const {form, reset} = Form.useForm();
  // const {reset, control} = useForm();


  //Change Order Status - Cancel
  useEffect(() => {
    if (!!submitDecline) {
      if (!!reason) {
        statusChange("cancelled", reason);
      } else {
        requireReason();
      }
    }
  }, [submitDecline]);
  const onChangeData = (reason) => {
    setReason(reason);
  };
  const cancelOrder = () => {
    confirm({
      title: "Confirm Cancel",
      icon: <ExclamationCircleOutlined />,
      content: (
        <div>
          <br />
          <TextArea
            rows={4}
            name="reason"
            onChange={(text) => onChangeData(text.target.value)}
            placeholder="Include a reason to cancel this order"
            required={true}
          />
          <br />
        </div>
      ),
      async onOk() {
        setOrderRefund(true)
        // setSubmitDecline(true);
      },
      onCancel() {},
    });
  };
  function requireReason() {
    setSubmitDecline(false);
    warning({
      title: "Reason is required",
      content: "Kindly include your reason for disapproval.",
    });
  }
  const statusChange = async (status, reason = "") => {
    try {
      setCancelLoading(true);
      const payload = {
        order_item_id: id,
        data: {
          status,
          reason,
        },
      };
      await dispatch(await actionCancelOrder({ payload }))
      .then((response) => {
        setCancelLoading(false);
        getAllOrdersData();
        // location.href = `/orders`
        notification.success({
          message: "Partner Status",
          description: "Order cancelled successfully",
        });
      })
      .catch((error) => {
        setCancelLoading(false);
      });
    } finally {
      setLoading(false);
    }
  };

  // New Codes.
const checkForm = (value) => {
  // e.status === "full_refund" && (e.amount = orderData.sub_total)  
  console.log(value)
  // form.resetFields();
  // {confirmRefund}
  // setOrderRefund(false)
}

const updateOrderStatus = async (value) => {
  try {
    setUpdateOrderLoading(true);
    const payload = {
      order_id: id,
      data: {
        status: value,
        reason: ""
      },
    };
  console.log(value)
    await dispatch(await updateOrderStat({ payload }))
      .then((response) => {
        setUpdateOrderLoading(false);
        setOnUpdateOrder(false)
        getAllOrdersData()
        notification.success({
          message: "Order",
          description: "Order Updated successfully",
        });
      })
      .catch((error) => {
        setAddressLoading(false);
      });
  } finally {
    setLoading(false);
  }
}


   // Make refund
   
   const handleRefund = e => {
  e === "full_refund" ?
  setRefund(orderData.sub_total) : setRefund(0)
  console.log(refund + "refund")
}
useEffect(() => {
  const getRefundAmount = () =>{
    return setRefundAmount(refund)
  }
  getRefundAmount()
}, [handleRefund]);

// POP UP ON CANCEL ORDER
  const confirmRefund = async (value) => {
    value.status === "full_refund" && (value.amount = orderData.sub_total)  
    try {
      // setRefundLoading(true);
      const payload = {
        order_id: orderData.id,
        data: {
          type: value.type,
          total_amount: orderData.sub_total,
          status: value.payment_status,
          refund: {
            status: value.status,
            amount: value.amount ? value.amount : totalAmount
          },
        }
      };
      await dispatch(await actionRefundAmount({ payload }))
        .then((response) => {
          // setRefundLoading(false);
          setOrderRefund(false)
          setSubmitDecline(true);
          getAllOrdersData()
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { 
        });
    } finally {
      setLoading(false);
    }
  }

  // console.log(orderData.sub_total)

// Change Address
  const onEditAddress = async (value) => {
    try {
      setAddressLoading(true);
      const payload = {
        order_id: id,
        data: {
          name: orderData.pickup.name,
          address: value.address,
          phone_number: orderData.pickup.phone_number,
          email: orderData.pickup.email

        },
      };
      await dispatch(await updateDeliveryAddress({ payload }))
        .then((response) => {
          setAddressLoading(false);
          setOnEdit(false)
          location.href = `/orders`
          getAllOrdersData()
          notification.success({
            message: "Address",
            description: "Address Changed successfully",
          });
        })
        .catch((error) => {
          setAddressLoading(false);
        });
    } finally {
      setLoading(false);
    }
  }
  
// Push to another store
 const pushOrder = async (value) => {
  try {
    setStoreLoading(true);
    const payload = {
      order_id: id,
      data: {
        store_id: value.store_id
      },
    };
    await dispatch(await pushToStore({ payload }))
    .then((response) => {
      setStoreLoading(false);
      setChangeStore(false)
      getAllOrdersData()
      notification.success({
        message: "Store",
        description: "Store Changed successfully",
      });
    })
    .catch((error) => {
      setStoreLoading(false);
    });
  } finally {
  setLoading(false);
}
}

// Push to another store
const orderDriver = async (value) => {
  try {
    setStoreLoading(true);
    const payload = {
      assignment_id: id,
      data: {
        driver_id: value.driver_id
      },
    };
    await dispatch(await changeDriver({ payload }))
    .then((response) => {
      setStoreLoading(false);
      setChangeDriver(false)
      getAllOrdersData()
      notification.success({
        message: "Store",
        description: "Driver assigned successfully",
      });
    })
    .catch((error) => {
      setStoreLoading(false);
      notification.error({
        message: "Oops!",
        description: "Unable assign drivers, try again",
      });
    });
  } finally {
  setLoading(false);
}
}

// FUNCTIONS FOR DRIVERS

const getAllDrivers = async () =>{
  try {
    setLoading(true);
    await dispatch(await getDrivers())
      .then((response) => {
        setDriverList(response.data);
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to get all drivers, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}

const getAssignedDriver = async () =>{
  try {
    setLoading(true);
    // await dispatch(await getOrderHistory(id))
    await dispatch(await getDriverAssignment(id))
      .then((response) => {
        setDriverInfo(response.data);
        console.log(response.data)
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to get driver assignment, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}

const getAssignmentHistory = async () =>{
  const payload = {
      order_id: id
  };
  console.log(payload)

  try {
    setLoading(true);
    await dispatch(await getOrderHistory({payload}))
    // await dispatch(await getOrderHistory(payload))
      .then((response) => {
        setAssignmentHistory(response.data);
        console.log(response.data)
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to get all drivers, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}


// Assign Driver to order
const assignADriver = async (value) => {
  try {
    setStoreLoading(true);
    const payload = {
      data: {
        driver_id: value.driver_id,
        order_id: id
      },
    };
    await dispatch(await assignDriverToOrder({ payload }))
    .then((response) => {
      setStoreLoading(false);
      setAssignDriver(false)
      getAllOrdersData()
      getAssignedDriver()
      notification.success({
        message: "Store",
        description: "Store Changed successfully",
      });
    })
    .catch((error) => {
      setStoreLoading(false);
    });
  } finally {
  setLoading(false);
}
}
// Re Assign Driver to order
const reassignADriver = async (value) => {
  try {
    setStoreLoading(true);
    const payload = {
      assign_id: id,
      data: {
        driver_id: value.driver_id
      },
    };
    await dispatch(await pushToStore({ payload }))
    .then((response) => {
      setStoreLoading(false);
      setReassignDriver(false)
      getAllOrdersData()
      notification.success({
        message: "Store",
        description: "Store Changed successfully",
      });
    })
    .catch((error) => {
      setStoreLoading(false);
    });
  } finally {
  setLoading(false);
}
}
  // GET ALL AVAILABLE STORES TO PUSH TO A NEW STORE
  useEffect(async () => {
    try {
      setLoading(true);
      await dispatch(await getAllStores())
        .then((response) => {
          setStoreList(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to Change store, try again",
          });
        });
      } finally {
        setLoading(false);
      }
    },[])


// Code to Get All Order Data
  const getAllOrdersData = async () => {
    try {
      setLoading(true);
      await dispatch(await getAllOrdersByName(id))
        .then((response) => {
          setOrderData(response.data);
               response.data.status?.status !== "new" && setActiveButton(true)
               setReference(response.data.reference)
          setTotalAmount(response.data.sub_total)
          // console.log(response.data.payment_details.total_amount)
          
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get order details, try again",
          });
        });
      } finally {
        setLoading(false);
      }
    };
    
    // Tracker not working
    
    const getOrderTrack = async () => {
      console.log(reference)
      try {
        // setLoading(true);
        await dispatch(await trackOrder(reference))
          .then((response) => {
            console.log(response.data)
            setTracker(response.data)
          })
          .catch((error) => {
            notification.error({
              message: "Oops!",
              description: "Unable to get order details, try again",
            });
          });
        } finally {
          // setLoading(false);
        }
      }




 
  return (
   
    <>
      <Head title={orderData.billing && orderData.billing.name || ""} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Single Order" page="Orders" page1="Order">
            <>
          <Button className="mx-3" type="primary" onClick={() => setOpen(true)}>
                  Open Tracker
                </Button>
                {/* {orderData.status && orderData.status?.status !== "new" && setActiveButton(true)} */}
                {orderData.status && orderData.status?.status !== "cancelled" &&
                <>
                 <Button className="mx-3" type="primary" loading={storeLoading} onClick={() => setChangeStore(true)}>
                  Change Store
                </Button>
                <Button className="mx-3" type="primary" loading={addressLoading} onClick={() => setOnEdit(true)}>
                  Change Address
                </Button>

                {orderData.status && orderData.status.status === "new" ?
                  <Button className="mx-3" type="primary" loading={updateOrderLoading} onClick={() => (
                     updateOrderStatus('processing')
                     // setOnUpdateOrder(true)
                  )}>
                 Process Order
               </Button> : (
              //  setActiveButton(true),
               orderData.status?.status === "processing" ?
              <Button className="mx-3" type="primary" loading={updateOrderLoading} onClick={() =>( 
              updateOrderStatus('processed')
           ) }>
             Order Processed</Button> : 
             orderData.status?.status === "processed" &&
             <Button className="mx-3" type="primary" loading={updateOrderLoading} onClick={() => (
              updateOrderStatus('dispatched')
             )}>Dispatch Order</Button>)
             }
              {orderData.status && orderData.status?.status !== "dispatched" &&
                <Button className="mx-3" type="danger" loading={cancelLoading} onClick={cancelOrder}>
                  Cancel Order
                </Button>
                }
                   
                    {orderData.status && orderData.status?.status === "processed" &&
                <Button className="mx-3" primary loading={assignDriver} onClick={() => setAssignDriver(true)}>
                  Assign Driver
                </Button>}              
          {assignmentHistory && assignmentHistory.status === "processing" &&
              <Button className="mx-3" primary loading={reassignDriver} onClick={() => setReassignDriver(true)}>
                Reassign Driver
              </Button>
            }
            </>}
            </>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <div className="row">
            {orderData.delivery_option === "delivery" ?
             <div className="col-md-3">
             <div className="card card-bordered">
               <div className="card-body text-center">
                 <div className="mx-auto mb-4">
                   <Avatar
                     style={{
                       backgroundColor: "#e09335",
                       verticalAlign: "middle",
                     }}
                     src={orderData.logo}
                     size={80}
                   >
                     
                       <span>
                         {orderData.delivery?.name.charAt(0).toUpperCase()}
                       </span>
                     
                   </Avatar>
                 </div>
                 <h5 className="font-size-15 mb-1 text-dark">
                   {orderData.delivery?.name}
                 </h5>
                 <p className="text-muted">{orderData.delivery?.email}</p>
                 <p className="text-small">{orderData.delivery?.phone_number}</p>
                 <p className="text-small">{orderData.delivery?.address}</p>
                 <p className="text-small">Post code: {orderData.delivery?.postcode}</p>
                 

               </div>
             </div>
           </div> :
              <div className="col-md-3">
                <div className="card card-bordered">
                  <div className="card-body text-center">
                    <div className="mx-auto mb-4">
                      <Avatar
                        style={{
                          backgroundColor: "#e09335",
                          verticalAlign: "middle",
                        }}
                        src={orderData.logo}
                        size={80}
                      >
                        
                          <span>
                            {orderData.pickup?.name.charAt(0).toUpperCase()}
                          </span>
                        
                      </Avatar>
                    </div>
                    <h5 className="font-size-15 mb-1 text-dark">
                      {orderData.pickup?.name}
                    </h5>
                    <p className="text-muted">{orderData.pickup?.email}</p>
                    <p className="text-small">{orderData.pickup?.phone_number}</p>
                    <p className="text-small">{orderData.pickup?.address}</p>
                    

                  </div>
                </div>
              </div> 
              }
              
              
              {orderData.payment_details &&
              <div className="col-md-9">
              <div className="row">
              <div className="col-md-4">
                  <div className="card mini-stats-wid">
                    <div className="card-body">
                      <div className="d-flex">
                         <div className="flex-grow-1">
                          <p className="text-muted fw-medium">Delivery option</p>
                          <h5 className="mb-0 text-capitalize">{orderData.delivery_option}</h5> 
                        </div> 

                        <div className="flex-shrink-0 align-self-center">
                          <div className="mini-stat-icon avatar-sm rounded-circle bg-primary">
                            <span className="avatar-title">
                              <i className="bx bx-copy-alt font-size-24"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div className="col-md-4">
                  <div className="card mini-stats-wid">
                    <div className="card-body">
                      <div className="d-flex">
                      {orderData.status && orderData.status?.status === "cancelled" ?
                        <div className="flex-grow-1">
                          <p className="text-muted fw-medium">Refund</p>
                          <h5 className="mb-0 text-capitalize">{`£${orderData.payment_details.refund.amount} ${orderData.payment_details.refund.status}`}</h5>
                        </div> :
                         <div className="flex-grow-1">
                         <p className="text-muted fw-medium">Amount</p>
                         <h5 className="mb-0 text-capitalize">{`£${orderData.payment_details.total_amount} ${orderData.payment_details.status}`}</h5>
                       </div>
                      }

                        <div className="flex-shrink-0 align-self-center">
                          <div className="avatar-sm rounded-circle bg-primary mini-stat-icon">
                            <span className="avatar-title rounded-circle bg-primary">
                              <i className="bx bx-purchase-tag font-size-24"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="card mini-stats-wid">
                    <div className="card-body">
                      <div className="d-flex">
                        <div className="flex-grow-1">
                          <p className="text-muted fw-medium">Payment method</p>
                          <h5 className="mb-0  text-capitalize">{orderData.payment_details.type}</h5>
                        </div>

                        <div className="flex-shrink-0 align-self-center">
                          <div className="avatar-sm rounded-circle bg-primary mini-stat-icon">
                            <span className="avatar-title rounded-circle bg-primary">
                              <i className="bx bx-briefcase-alt-2 font-size-24"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                
              </div>
                    <div className="row">

                      <div className="col-md-4">
                        <div className="card mini-stats-wid">
                          <div className="card-body">
                            <div className="d-flex">
                              <div className="flex-grow-1">
                                <p className="text-muted fw-medium">Reference number</p>
                                <h5 className="mb-0">{orderData.reference}</h5>
                              </div>

                              <div className="flex-shrink-0 align-self-center">
                                <div className="avatar-sm rounded-circle bg-primary mini-stat-icon">
                                  <span className="avatar-title rounded-circle bg-primary">
                                    <i className="bx bx-purchase-tag-alt font-size-24"></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-5">

                        <div className="card mini-stats-wid"
                          onClick={() => {
                            Router.push(`/stores/store/${orderData.store._id}`);
                          }}>
                          <div className="card-body cursor-pointer">
                            <div className="d-flex">
                              <div className="flex-grow-1">
                                <p className="text-muted fw-medium">Store</p>
                                <h5 className="mb-0  text-capitalize text-primary cursor-pointer"><u>{orderData.store.name}</u></h5>
                              </div>

                              <div className="flex-shrink-0 align-self-center">
                                <div className="avatar-sm rounded-circle bg-primary mini-stat-icon">
                                  <span className="avatar-title rounded-circle bg-primary">
                                    <i className="bx bx-link font-size-24"></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="card mini-stats-wid">
                          <div className="card-body">
                            <div className="d-flex">
                              <div className="flex-grow-1">
                                <p className="text-muted fw-medium">Order status</p>
                                <h5 className="mb-0 text-capitalize">{orderData.status.status}</h5>
                              </div>

                              <div className="flex-shrink-0 align-self-center">
                                <div className="mini-stat-icon avatar-sm rounded-circle bg-primary">
                                  <span className="avatar-title">
                                    <i className="bx bx-credit-card font-size-24"></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              }

                         
              <div className="col-md-12">
                <div className="table-responsive">
                  <table className="table align-middle table-nowrap mb-0">
                    <thead className="table-light">
                      <tr>
                      <th className="align-middle">S/N</th>
                        <th className="align-middle">Date</th>
                        <th className="align-middle">Product</th>
                        <th className="align-middle">Price</th>
                        <th className="align-middle">Quantity</th>
                        <th className="align-middle">Update</th>
                      </tr>
                    </thead>
                    <tbody>
                      {orderData.orders &&
                        orderData.orders.map((order, index) => (
                          <tr key={index}>
                            <td>
                              <a
                                href="javascript: void(0);"
                                className="text-body fw-bold"
                              >
                                {index + 1}
                              </a>
                            </td>
                            <td>
                              {moment(order.created_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                            <td>{order.title}</td>
                            <td>{`£${order.price}`}</td>
                            <td>{order.quantity}</td>

                            <td>
                              <Button
                              disabled = {activeButton ? true : false}
                          type="primary"
                          onClick={() => {
                            Router.push(`/orders/product/${order._id}`);
                          }}
                        >
                         Update order item
                        </Button>
                          </td>
                          
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
                
              
              </div>

              {/* Make REFUND */}
                <Modal
                  title="Refund "
                  centered
                  visible={orderRefund}
                  width={500}
                  onCancel={() => setOrderRefund(false)}
                  footer={null}
                >
                  <p>Total Amount : 
                  {` £${orderData.sub_total}`}
                  </p>
                  <Form
                    form={form}
                    size="large"
                    name="refund"
                    onFinish={confirmRefund}
                    // onFinish={checkForm}
                    layout="vertical"
                  >
                    <Form.Item name="type" 
                      rules={[{ required: true, message: 'Please select a payment method' }]}
                    label="Payment type">
                      <Select
                        placeholder="Payment method"
                      >
                        <Option value="card">Card</Option>
                        <Option value="cash">Cash</Option>
                      </Select>
                    </Form.Item>
                    <Form.Item name="payment_status"
                       rules={[{ required: true, message: "What's the payment status" }]}
                     label="Status">
                      <Select
                        placeholder="Payment status"
                      >
                        <Option value="pending">Pending</Option>
                        <Option value="paid">Paid</Option>
                      </Select>
                    </Form.Item>
                    <Form.Item name="status" 
                      rules={[{ required: true, message: 'Please select a payment type' }]}
                    label="Status">
                      <Select
                        placeholder="Payment type"
                        onChange={handleRefund}
                      >
                        <Option value="full_refund">Full Refund</Option>
                        <Option value="part_refund">Part Refund</Option>
                        <Option value="no_refund">No Refund</Option>
                      </Select>
                    </Form.Item>
                    <Form.Item
                      name="amount"
                      // rules={[{ required: true, message: 'Please select a payment type' }]}
                     
                      label="Refund Amount">
                      <InputNumber
                        className="inputWidthFull"
                        // placeholder= {refundAmount}
                        defaultValue={ totalAmount  }
                        // required
                      />
                    </Form.Item>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      // onClick={() => setOrderRefund(false)}
                      >
                        Refund
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => {
                          setOrderRefund (false);
                      }
                      }
                      >
                        Cancel
                      </Button>
                    </Form.Item>
                    </Form>
                </Modal>

                {/* Update Order */}

                <Modal
                  title="Update order status"
                  centered
                  visible={onUpdateOrder}
                  onCancel={() => setOnUpdateOrder(false)}
                  width={500}
                  footer={null}
                >
                  <Form
                    form={form}
                    size="large"
                    name="address"
                    onFinish={updateOrderStatus}
                    layout="vertical"
                  >
                   <Form.Item name="status"
                      label="Update status"
                      // rules={[{ required: true, message: 'Please select an store' }]}
                      >
                   <Select
                        placeholder="Update status"
                        // required
                        style={{
                          width: 450
                        }}
                      >
                          <Option value="processing"></Option>
                          <Option value="processed"></Option>
                          <Option value="dispatched"></Option>
                       
                      </Select>
                   </Form.Item>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      // onClick={() => setOnEdit(false)}
                      >
                       Update status
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => setOnUpdateOrder(false)}
                      >
                        Cancel
                      </Button>
                    </Form.Item>
                  </Form>
                </Modal>

              {/* Change Delivery Address */}
                <Modal
                  title="Change Delivery Address"
                  centered
                  visible={onEdit}
                  onCancel={() => setOnEdit(false)}
                  width={500}
                  footer={null}
                >
                  <Form
                    form={form}
                    size="large"
                    name="address"
                    onFinish={onEditAddress}
                    layout="vertical"
                  >
                    <Form.Item name="address"
                      label="New Address"
                      rules={[{ required: true, message: 'Please select an address' }]}>
                      <Input
                        className="inputWidthFull"
                        label="Change address"
                        placeholder="Enter address"
                        required
                      />
                    </Form.Item>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      // onClick={() => setOnEdit(false)}
                      >
                        Change Address
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => setOnEdit(false)}
                      >
                        Cancel
                      </Button>
                    </Form.Item>
                  </Form>
                </Modal>

              {/* Push Order to another store */}
                <Modal
                  title="Change Store"
                  centered
                  visible={changeStore}
                  onCancel={() => setChangeStore(false)}
                  width={500}
                  footer={null}
                >
                  <Form
                    form={form}
                    size="large"
                    name="store_id"
                    onFinish={pushOrder}
                    layout="vertical"
                  >
                    <Form.Item name="store_id"
                      label="Choose store"
                      rules={[{ required: true, message: 'Please select an store' }]}>
                      <Select
                        placeholder="Choose Store"
                        required
                        style={{
                          width: 450
                        }}
                      >
                        {storeList && storeList.map((storeName, index) => (
                          <Option key={index} value={storeName._id}>{storeName.name}</Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      >
                        Change Store
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => setChangeStore(false)}
                      >
                        Cancel
                      </Button>
                    </Form.Item>
                  </Form>
                </Modal>

              {/* Tracker Modal */}
                <Modal
                  title="Track Order"
                  centered
                  visible={open}
                  onCancel={() => setOpen(false)}
                  // width={1000}
                  footer={null}
                >

                  <table className="table align-middle table-nowrap mb-0">
                    <thead className="table-light">
                      <tr>
                        <th className="align-middle">Date</th>
                        <th className="align-middle">Created By</th>
                        <th className="align-middle">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      {tracker &&
                        tracker.map((track, index) => (
                          <tr key={index}>
                            <td>
                              {moment(track.created_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                            <td>
                              {users.map((user) => {
                                if (user.id === track.created_by) {
                                  return (
                                    <div>{user.name}</div>
                                  )
                                }
                              })
                              }
                            </td>
                            <td>{track.status.status.toUpperCase()}</td>
                          </tr>
                        ))}
                    </tbody>
                  </table>

                  <Form.Item>
                    <Button
                      className="mt-2"
                      type="primary"
                      onClick={() => setOpen(false)}
                    >
                      Close
                    </Button>
                  </Form.Item>
                </Modal>


                {/* Assign Driver */}
                <Modal
                  title="Assign A Driver"
                  centered
                  visible={assignDriver}
                  onCancel={() => setAssignDriver(false)}
                  width={500}
                  footer={null}
                >
                  <Form
                    form={form}
                    size="large"
                    name="store_id"
                    onFinish={assignADriver}
                    layout="vertical"
                  >
                    <Form.Item name="driver_id"
                      label="Choose driver"
                      rules={[{ required: true, message: 'Please select an store' }]}>
                      <Select
                        placeholder="Choose Driver"
                        required
                        style={{
                          width: 450
                        }}
                      >
                        {driverList && driverList.map((driver, index) => (
                          driver.status !== "busy" &&
                            <Option key={index} value={driver.id}>{driver.name}</Option>
                          
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      >
                        Assign Driver
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => setAssignDriver(false)}
                      >
                        Cancel
                      </Button>
                    </Form.Item>
                  </Form>
                </Modal>

                {/* Assign Driver */}
                <Modal
                  title="Assign A Driver"
                  centered
                  visible={reassignDriver}
                  onCancel={() => setReassignDriver(false)}
                  width={500}
                  footer={null}
                >
                  <Form
                    form={form}
                    size="large"
                    name="store_id"
                    onFinish={reassignADriver}
                    layout="vertical"
                  >
                    <Form.Item name="driver_id"
                      label="Choose driver"
                      rules={[{ required: true, message: 'Please select an store' }]}>
                      <Select
                        placeholder="Choose Driver"
                        required
                        style={{
                          width: 450
                        }}
                      >
                        {driverList && driverList.map((driver, index) => (
                          driver.status !== "busy" &&
                            <Option key={index} value={driver.id}>{driver.name}</Option>
                          
                        ))}
                      </Select>
                    </Form.Item>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      >
                        Assign Driver
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => setReassignDriver(false)}
                      >
                        Cancel
                      </Button>
                    </Form.Item>
                  </Form>
                </Modal>



            </div>
          )}
        </div>
      </DashboardLayout>

      

     

    </>
  );
};

EditOrder.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default EditOrder;
