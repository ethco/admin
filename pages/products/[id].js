import { useState, useEffect } from "react";
import DashboardLayout from "../../layouts/DashboardLayout";
import Head from "../../components/Head";
import Link from "next/link";
import { Modal, Button, Input, Form, InputNumber, Select, Upload } from "antd";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
  actionEditProduct,
    getProduct,
} from "../../helper/redux/actions/app";
import {
  UploadOutlined,
} from "@ant-design/icons";
const SingleProduct = ({ id }) => {
  const [loading, setLoading] = useState(true);
  const [editProduct, setEditProduct] = useState(false);
  const [product, setProduct] = useState({});
  const [categoryList, setCategoryList] = useState([]);
  const dispatch = useDispatch();

  
  const [ form ] = Form.useForm();
  const { Option } = Select;
  const { TextArea } = Input;

 // get all categories 
 useAsync(async () => {
  try {
    setLoading(true);
    await dispatch(await getCategories())
      .then((response) => {
        setCategoryList(response.data);
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to send response, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}, [])

// Get Products Details
const productDetails = async (value) => {
    try {
      setLoading(true);
      await dispatch(await getProduct(id))
        .then((response) => {
          setProduct(response.data);
          console.log(response.data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "try again",
          });
        });
    } finally {
      setLoading(false);
    }
    }

   // update Store Subscription
   const productEdit = async (value) => {
    try {
      setLoading(true);
      const images1 = value.image > 0 ? value.image.map(e, i => {
        return e[i].thumbUrl
      }) : value.image === 0 ? [value.image[0].thumbUrl] : null
      const payload = {
        product_id: id,
        data: {
          title: value.title ? value.title : product.title,
          sku: value.sku  ? value.sku : product.sku,
          description: value.description  ? value.description : product.description,
          price: value.price ? value.price : product.price,
          image: images1 ? images1 : product.image,
          categories: value.category ? value.category : product.categories,
          quantity: value.quantity ? value.quantity : product.quantity,
          unit: "kg",
        }
      };
      await dispatch(await actionEditProduct({ payload }))
        .then((response) => {
          setLoading(false)
          getProduct()
          setEditProduct(false)
          onReset
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => {
          setEditProduct(false)
        });
    } finally {
      setLoading(false);
    }
  };
  const addProductImage = (e) => {
    console.log('Upload event:', e);

    if (Array.isArray(e)) {
      return e;
    }

    return e?.fileList;
  };

  
  const onReset = () => {
    form.resetFields();
  };

  useAsync(async () =>{
   await productDetails()
  }, [])
  useEffect(() => {
    console.log(product)
    }, [])
  return (
    <>
      <Head title={product.title || ""} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader  title="Product info" page="products">
          <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setEditProduct(true)}
            >
             Update Product
            </Button>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <div className="row">
              <div className="col-md-12">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title mb-4">Product Information</h4>
                    <div className="table-responsive">
                      <table className="table table-nowrap mb-0">
                        <tbody>
                          <tr>
                            <th className="row">Date Created</th>
                            <td>
                              {moment(product.created_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                          </tr>
                          <tr>
                            <th className="row">Product Name</th>
                            <td>{product.title}</td>
                          </tr>
                          <tr>
                            <th className="row">Product ID</th>
                            <td>{product.product_code}</td>
                          </tr>
                          <tr>
                            <th className="row">About Description</th>
                            <td>{product.description}</td>
                          </tr>
                          <tr>
                            <th className="row">Price</th>
                            <td>{`£${product.price}`}</td>
                          </tr>
                          <tr>
                            <th className="row">Quantity</th>
                            <td>{product.quantity}</td>
                          </tr>
                          <tr>
                            <th className="row">Unit</th>
                            <td>{product.unit}</td>
                          </tr>
                          {/* Display status */}
                          {product.status && product.status === "unpublished" ?
                          <tr>
                          <th className="row">Status</th>
                          <td className="text-capitalize badge-soft-danger">{product.status}</td>
                        </tr> :
                        <tr>
                            <th className="row">Status</th>
                            <td className="text-capitalize badge-soft-success">{product.status}</td>
                          </tr>
                              }
                          <tr>
                            <th className="row">Date Updates</th>
                            <td>
                              {moment(product.updated_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

                {/* UPDATE STORE ADDRESS */}
                <Modal
                  title="Change store address"
                  centered
                  visible={editProduct}
                  width={500}
                  onCancel={() => setEditProduct(false)}
                  footer={null}
                >
                  <Form
                        form={form}
                        name="add_product"
                        onFinish={productEdit}
                        // onFinish={() =>{
                        //   onReset()
                        //   productEdit
                        //   setEditProduct(false)
                        // }}
                        // closable={true}
                        layout="vertical"
                      >
                        <Form.Item  name="title" label="Product title">
                          <Input
                          
                            className="inputWidthFull"
                            label="Product title"
                           
                          /></Form.Item>

                        <Form.Item name="sku" label="SKU">
                          <Input 
                          
                            className="inputWidthFull"
                            label="SKU"
                           
                          />
                        </Form.Item>

                        <Form.Item name="price" label="Price">
                          <InputNumber
                          name="price"
                            className="inputWidthFull"
                            label="price"
                          />
                        </Form.Item>

                        <Form.Item name="quantity" label="Quantity">
                          <InputNumber
                            className="inputWidthFull"
                            label="quantity"
                          />
                        </Form.Item>

                        <Form.Item name="category" label="Categories">
                          <Select
                            mode="multiple"
                            placeholder="Choose category"
                          >
                            {categoryList && categoryList.map((category, index) => (
                              <Option key={index} value={category.id}>{category.name}</Option>
                            ))}
                          </Select>
                        </Form.Item>

                        <Form.Item name="image" valuePropName="fileList"
                          getValueFromEvent={addProductImage}
                        >
                          <Upload name="image"
                            listType="picture"
                            multiple>
                            <Button icon={<UploadOutlined />}>Click to upload</Button>
                          </Upload>
                        </Form.Item>
                        
                        <Form.Item name="description">
                          <TextArea rows={4}
                            placeholder="Product description"
                            // className="inputWidthFull"
                            label="Description"
                          // maxLength={6}
                          />

                        </Form.Item>

                        <Form.Item>
                          <Button
                            loading={loading}
                            htmlType="submit"
                            className="mt-2"
                            type="primary"
                            
                          // onClick={() =>{onReset}}
                          >
                            Update Product
                          </Button>
                          <Button
                            className="mt-2 mx-3"
                            type="primary"
                            onClick={() =>{
                              setEditProduct(false)
                              onReset()
                            }}
                          >
                            Cancel
                          </Button>
                        </Form.Item >
                      </Form>
                </Modal>
            </div>
          )}
        </div>
      </DashboardLayout>
    </>
  );
};

SingleProduct.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default SingleProduct;
