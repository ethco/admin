import { useEffect, useState } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Randomstring from "randomstring";
import Router from "next/router";
// import SearchSelect from "../../components/SearchSelect";
import Link from "next/link";
import {
  UploadOutlined,
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  Select, Modal, Spin, Switch,
  Form, InputNumber, Input, Space,
  Upload, Button, Avatar
} from "antd";
import {
  getProducts,
  getAllStores,
  getCategories,
  AddProductToStore,
  updatePublishStatus,
  getStoreCategory,
} from "../../helper/redux/actions/app";
import { useDispatch } from "react-redux";
import { useAsync } from "react-use";
import moment from "moment";

export default function Index() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
   const [productCategory, setProductCategory] = useState("");
   const [productCategoryID, setProductCategoryID] = useState("");
  const [products, setProducts] = useState([]);
  const [storeList, setStoreList] = useState([]);
  const [categoryList, setCategoryList] = useState([]);
  const [categories, setCategories] = useState([]);
  const [store, setStore] = useState([]);
  const [pageLoading, setPageLoading] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [addNewProduct, setAddNewProduct] = useState(false);
  const [results, setResults] = useState([]);

  const { confirm } = Modal;
  const { Option } = Select;
  const { TextArea } = Input;
  const [form] = Form.useForm();

  // Same Function with getStoreList()

  const onReset = () => {
    form.resetFields();
  };

  const getAllProducts = async (value) => {
    try {
      setLoading(true);
      await dispatch(await getProducts(value))
        .then((response) => {
          setLoading(false);
          setProducts(response.data);
        })
        .catch((error) => {
          setLoading(false);
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  };

  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getAllStores())
        .then((response) => {
          setStoreList(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, [])

  const checkForm = (value) => {
    console.log(value)
    const images1 = value.image
    console.log(`title: ${value.title},
  store_id: ${value.store_id}
  sku: ${value.sku},
  description: ${value.description},
  price: ${value.price},
  image:(${images1}),
  categories: ${value.category},
  quantity: ${value.quantity}`)
    // setStoreInfo(false)
  }

  const publishStatus = async (id, status) => {
    try {
      setPageLoading(true);
      const payload = {
        product_id: id,
        data: {
          status: status ? "published" : "unpublished"
        }
      };
      await dispatch(await updatePublishStatus({ payload }))
        .then((response) => {
          getAllProducts();
          notification.success({
            message: "Manager Role",
            description: "Manager role updated successfully",
          });
        })
        .catch((error) => {
          setPageLoading(false);
        });
    } finally {
      setLoading(false);
    }
  };

  // get all categories 
  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getCategories())
        .then((response) => {
          setCategoryList(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, [])

  // const merge = []
  useEffect(() => {
    setResults([...products])
     console.log(...products)
  }, [products])

// Add product to a store
  const addProduct = async (value) => {
    try {
      setLoading(true);
      const images1 = value.image > 0 ? value.image.map(e, i => {
        return e[i].thumbUrl
      }) : [value.image[0].thumbUrl]
      // console.log(images1)
      const payload = {
        store_id: value.store_id,
        data: {
          title: value.title,
          sku: value.sku,
          description: value.description,
          price: value.price,
          image: images1,
          categories: value.category,
          quantity: value.quantity,
          unit: "kg",
          product_code: Randomstring.generate(7)
        }
      };
      await dispatch(await AddProductToStore({ payload }))
        .then((response) => {
          getProducts()

          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };
  const addProductImage = (e) => {
    console.log('Upload event:', e);

    if (Array.isArray(e)) {
      return e;
    }

    return e?.fileList;
  };
// get product Category
const getProductCategory = async () => {
  try {
    setLoading(true);
    // Get plan ID to replace the data
    await dispatch(await getStoreCategory())
      .then((response) => {
        setProductCategory(response.data.name);
        console.log(response.data.name)
      })
      .catch((error) => {
        notification.error({
          message: "Oops!",
          description: "Unable to get store category, try again",
        });
      });
  } finally {
    setLoading(false);
  }
}

  useAsync(async () => {
    await getProductCategory()
    await getAllProducts()
  }, []);


  return (
    <>
      <Head title="All Products" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Products" page="products">
            <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setAddNewProduct(true)}
            >
              Add new product
            </Button>
            <Select
              mode="multiple"
              maxTagCount={3}
              onChange={(value) => {
                console.log(value.length)
                value.length > 0 ? value.map(e =>{
                  getAllProducts(e) 
                }) : getAllProducts(value)
              }}
              style={{ width: "250px" }}
              placeholder="Search product by store"
            >
              {storeList && storeList.map((store, index) => (
                <Option key={index} value={store._id}>{store.name}</Option>
              ))}
            </Select>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
               <Modal
                  title="Add a new product"
                  centered
                  visible={addNewProduct}
                  onCancel={() => setAddNewProduct(false)}
                  width={"800px"}
                  footer={null}
                >
                   <Form
                        form={form}
                        name="add_product"
                        onFinish={addProduct}
                        // onFinish={checkForm}
                        // closable={true}
                        layout="vertical"
                        // layout="inline" 
                      >
                        <Form.Item  name="title" label="Product title">
                          <Input
                          
                            className="inputWidthFull"
                            label="Product title"
                           
                          /></Form.Item>

                        <Form.Item name="sku" label="SKU">
                          <Input 
                          
                            className="inputWidthFull"
                            label="SKU"
                           
                          />
                        </Form.Item>

                        <Form.Item name="price" label="Price">
                          <InputNumber
                          name="price"
                            className="inputWidthFull"
                            label="price"
                          />
                        </Form.Item>

                        <Form.Item name="quantity" label="Quantity">
                          <InputNumber
                            className="inputWidthFull"
                            label="quantity"
                          />
                        </Form.Item>

                        <Form.Item name="store_id" label="Store">
                          <Select
                            placeholder="Select store"
                          >
                            {storeList && storeList.map((store, index) => (
                              <Option key={index} value={store._id}>{store.name}</Option>
                            ))}
                          </Select>
                        </Form.Item>

                        <Form.Item name="category" label="Categories">
                          <Select
                            mode="multiple"
                            // onChange={(value) => {
                            //   setCategories(value)
                            // }}
                            placeholder="Choose category"
                          >
                            {categoryList && categoryList.map((category, index) => (
                              <Option key={index} value={category.id}>{category.name}</Option>
                            ))}
                          </Select>
                        </Form.Item>

                        <Form.Item
                          name="image"
                          valuePropName="fileList"
                          getValueFromEvent={addProductImage}
                        >
                          <Upload name="image"
                            listType="picture"
                            multiple>
                            <Button icon={<UploadOutlined />}>Click to upload</Button>
                          </Upload>
                        </Form.Item>
                        
                        <Form.Item name="description">
                          <TextArea rows={4}
                            placeholder="Product description"
                            // className="inputWidthFull"
                            label="Description"
                          // maxLength={6}
                          />

                        </Form.Item>

                        <Form.Item>
                          <Button
                            loading={loading}
                            htmlType="submit"
                            className="mt-2"
                            type="primary"
                            
                          onClick={() =>{
                            onReset
                            setAddNewProduct
                          }}
                          >
                            Add Product
                          </Button>
                          <Button
                            className="mt-2 mx-3"
                            type="primary"
                            onClick={onReset}
                          >
                            Reset
                          </Button>
                        </Form.Item >
                      </Form>
                </Modal>

              <div className="table-responsive">
                <table className="table align-middle table-nowrap mb-0">
                  <thead className="table-light">
                    <tr>
                      {/* <th className="align-middle">S/N</th> */}
                      <th className="align-middle">Date added</th>
                      <th className="align-middle">Product name</th>
                      <th className="align-middle">Price</th>
                      <th className="align-middle">Quantity</th>
                      <th className="align-middle">categories</th>
                      <th className="align-middle">Status</th>
                      <th className="align-middle">Details</th>
                    </tr>
                  </thead>
                  <tbody>
                    {results.map((product, index) => (
                      <tr key={index}>
                        {/* <td>{index + 1}</td> */}
                        <td>
                          <span className="text-small">
                            {moment(product.created_at).format("ddd, MMM Do YYYY")}
                          </span>
                        </td>
                        <td><Avatar
                          style={{
                            backgroundColor: "#e09335",
                            verticalAlign: "middle",
                          }}
                          src={product.image[0]}
                          size={30}
                        >
                        </Avatar><span className="mx-1">{product.title}</span></td>
                        <td>{product.price}</td>
                        <td>{product.quantity}</td>
                        <td>{product.categories && product.categories.map(category =>{
                          // setProductCategoryID(category)
                          console.log(category)
                        })}<h6>{productCategory}</h6></td>
                        <td>
                          <Switch
                            style={{ marginLeft: "10px" }}
                            checkedChildren="Published"
                            unCheckedChildren="Unpublished"
                            defaultChecked={
                              product.status === "published" ? true
                                : false
                            }
                            onChange={(value) => {
                              publishStatus(product.id, value);
                              
                            }}
                          />
                        </td>
                        <td><Button type="primary"
                          primary
                          onClick={() => {
                            Router.push(`products/${product.id}`);
                            console.log(product.id)
                          }}
                        >
                          Details
                        </Button></td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </Spin>
          )}
        </div>
      </DashboardLayout>
    </>
  );
}
