import { useState } from "react";
import DashboardLayout from "../../layouts/DashboardLayout";
import Head from "../../components/Head";
import Link from "next/link";
import { Modal, Button, Avatar, Form, Input, InputNumber, Select, Upload } from "antd";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import { useDispatch } from "react-redux";
import Router from "next/router";
import { useAsync } from "react-use";
import moment from "moment";
import {
  addAddress,
  getAddress,
  addPartnerStore,
  getAllPartnerByName,
  getAllPartnerStore,
  actionDeletePartner,
  getCategories,
  getAddresses,
  actionUpdatePartner,
  updatePartnerLogo
} from "../../helper/redux/actions/app";
import { ExclamationCircleOutlined, UploadOutlined } from "@ant-design/icons";
import { SizeContextProvider } from "antd/lib/config-provider/SizeContext";

const Partner = ({ id }) => {

  const { TextArea } = Input;
  const [loading, setLoading] = useState(true);
  const [deleteLoading, setDeleteLoading] = useState(false);
  const [addStore, setAddStore] = useState(false);
  const [partnerData, setPartnerData] = useState({});
  const [partnerID, setPartnerID] = useState('');
  const [partnerInfo, setPartnerInfo] = useState(false);
  const [logoUpdate, setLogoUpdate] = useState(false);

  const [partnerStores, setPartnerStores] = useState({});
  const [postcode, setPostcode] = useState('');
  const [coord, SetCoord] = useState({});
  const [categories, setCategories] = useState([]);
  const dispatch = useDispatch();
  const { confirm } = Modal;
  const [form] = Form.useForm();
  const { Option } = Select;



  const onReset = () => {
    form.resetFields();
  };


  const confirmDelete = () => {
    confirm({
      title: "Confirm delete",
      icon: <ExclamationCircleOutlined />,
      content: "Are you sure to delete this partner?",
      async onOk() {
        try {
          setDeleteLoading(true);
          const id = partnerData.id;

          await dispatch(await actionDeletePartner({ id }))
            .then((response) => {
              setDeleteLoading(false);
              notification.success({
                message: "Partner",
                description: "Partner added successfully",
              });
              setTimeout(() => {
                Router.push("/partners");
              }, 1500);
            })
            .catch((error) => {
              setDeleteLoading(false);
            });
        } finally {
          setLoading(false);
        }
      },
      onCancel() { },
    });
  };

  useAsync(async () => {
    await getPartnerInfo()
    // await getPartnerData()
  }, []);


  const getPartnerInfo = async () => {
    try {
      setLoading(true);
      await dispatch(await getAllPartnerByName(id))
        .then((response) => {
          setPartnerData(response.data);
          setPartnerID(response.data.id);


        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get partner details, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }


  // Change Store Information
  const updatePartnerInfo = async (value) => {
    try {
      setLoading(true);
      const payload = {
        id: id,
        data: {
          business_name: value.name ? value.name : partnerData.business_name,
          description: value.description ? value.description : partnerData.description,
          phone: value.phone ? value.phone : partnerData.phone,
        }
      };
      console.log(value.street)
      await dispatch(await actionUpdatePartner({ payload }))
        .then((response) => {
          setLoading(false)
          onReset()
          setPartnerInfo(false)
          getPartnerInfo()
          console.log("Update Success")
          notification.success({
            message: "Partner",
            description: "Partner updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };
  // Update Logo
    // Add product to a store
    const updateLogo = async (value) => {
      try {
        setLoading(true);
        const images1 = value.image[0].thumbUrl
        console.log(images1)
        const payload = {
          id: partnerID,
          data: {
            logo: images1 ? images1 : partnerData.logo
          }
        };
        await dispatch(await updatePartnerLogo({ payload }))
          .then((response) => {
            setLogoUpdate(false)
            onReset()
            getPartnerInfo()
            notification.success({
              message: "Logo",
              description: "Logo updated",
            });
          })
          .catch((error) => { });
      } finally {
        setLoading(false);
      }
    };

  const addCategoryImage = (e) => {
    console.log('Upload event:', e);
  
    if (Array.isArray(e)) {
      return e;
    }
  
    return e?.fileList;
  };


  const actionAddStore = async (value) => {
    try {
      setLoading(true);
      const payload = {
        partner_id: partnerID,
        data: {
          name: value.name,
          street: value.street,
          postcode: value.postcode,
          latitude: coord.latitude ? coord.latitude : 52.762297,
          longitude: coord.longitude ? coord.longitude : -1.202675,
          mile_radius: value.mile_radius,
          category: value.category,
        }
      };
      await dispatch(await addPartnerStore({ payload }))
        .then((response) => {
          onReset()
          setAddStore(false)
          setLoading(false);
          // getPartnerData()
          getPartnerInfo()
          console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };


  //******************************** */ ADDRESS 

  const actionAddAddress = async (value) => {
    try {
      setLoading(true);
      const payload = {
        data: {
          postcode: value
        }
      };
      await dispatch(await addAddress({ payload }))
        .then((response) => {
          // console.log("Update Success")
          notification.success({
            message: "Store",
            description: "Store updated successfully",
          });
        })
        .catch((error) => { });
    } finally {
      setLoading(false);
    }
  };
  // Get Address with Post-code
  const getCoordinates = async (value) => {
    try {
      await dispatch(await getAddress(value))
        .then((response) => {
          SetCoord(response.data)
          console.log(response.data.latitude, response.data.longitude)
          notification.success({
            message: "Category",
            description: "Charges updated successfully",
          });
        })
        .catch((error) => {
          setLoading(false);
        });
    } finally {
      setLoading(false);
    }
  }

  // Get Address with Post-code
  const getJustAddress = async (value) => {
    try {
      await dispatch(await getAddresses(value))
        .then((response) => {
          const data = response.data
          console.log(data)
          SetCoord(data)
          // console.log(response.data.latitude , response.data.longitude)
          notification.success({
            message: "Category",
            description: "Charges updated successfully",
          });
        })
        .catch((error) => {
          setLoading(false);
        });
    } finally {
      setLoading(false);
    }
  }

  // ****************************** */ CATEGORIES
  // Get All Categories 
  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getCategories())
        .then((response) => {
          const data = (response.data);
          setCategories(data)
          console.log(data)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to get Categories, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, [])








  return (
    <>
      <Head title={partnerData.name || ""} />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Partners" page="partners" page1="partner"
          // title={partnerData.name || ""}
          // page={partnerData.name || ""}
          >

            <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setPartnerInfo(true)}
            >
              Update Information
            </Button>

            <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setAddStore(true)}
            >
              Add Store Branch
            </Button>

          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <div className="row">
              <div className="col-md-3">
                <div className="card card-bordered">
                  <div className="card-body text-center">
                    <div className="mx-auto mb-4">
                      <Avatar
                        style={{
                          // backgroundColor: "#e09335",
                          verticalAlign: "middle",
                        }}
                        src={partnerData.logo}
                        size={80}
                      >
                        {partnerData.business_name && (
                          <span>
                            {partnerData.business_name.charAt(0).toUpperCase()}
                          </span>
                        )}
                      </Avatar>
                    </div>
                    <h5 className="font-size-15 mb-1 text-dark">
                      {partnerData.business_name}
                    </h5>
                    <p className="text-muted">{partnerData.email}</p>
                    <p className="text-muted">{partnerData.phone}</p>
                    <p className="text-small">{partnerData.description}</p>
                    <Button
              type="primary"
              className="mx-3"
              primary
              onClick={() => setLogoUpdate(true)}
            >
              Update Logo
            </Button>
                  </div>
                </div>
              </div>
              <div className="col-md-9">
                <div className="table-responsive">
                  <table className="table align-middle table-nowrap mb-0">
                    <thead className="table-light">
                      <tr>
                        <th className="align-middle">S/N</th>
                        <th className="align-middle">Date</th>
                        <th className="align-middle">Address Title</th>
                        <th className="align-middle">Location</th>
                        <th className="align-middle">Post Code</th>
                        <th className="align-middle">Town</th>
                        <th className="align-middle">View Store</th>
                      </tr>
                    </thead>
                    <tbody>
                      {partnerData.stores &&
                        partnerData.stores.map((location, index) => (
                          <tr key={index}>
                            <td>
                              <a
                                href="javascript: void(0);"
                                className="text-body fw-bold"
                              >
                                {index + 1}
                              </a>
                            </td>
                            <td>
                              {moment(location.created_at).format(
                                "ddd, MMM Do YYYY"
                              )}
                            </td>
                            <td>{location.name}</td>
                            <td>{location.street}</td>
                            <td>{location.postcode}</td>
                            <td>{location.town}</td>
                            <td>
                              <Button
                                type="primary"
                                onClick={() => {
                                  Router.push(`/stores/store/${location.id}`);
                                  console.log(location.id)
                                }}
                              >
                                View Store
                              </Button>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>


              </div>

            </div>
          )}
        </div>
      </DashboardLayout>

      {/* ADD STORE TO PARTNER */}
      <Modal
        title="Add a new store"
        centered
        visible={addStore}
        onCancel={() => setAddStore(false)}
        width={"800px"}
        footer={null}
      >
        <Form
          form={form}
          name="add_store"
          onFinish={actionAddStore}
          layout="vertical"
        >
          <Form.Item name="name" label="Store name"
            rules={[{ required: true, message: 'Please input the store name' }]}
          >
            <Input
              className="inputWidthFull"
              label="Store name"
            />
          </Form.Item>

          <Form.Item name="street" label="Street address"
            rules={[{ required: true, message: 'Please input the street address' }]}
          >
            <Input
              className="inputWidthFull"
              label="Street address"
            />
          </Form.Item>

          <Form.Item name="postcode" label="Post code"
            rules={[{ required: true, message: 'Please input the post code' }]}
            onChange={e => {
              getCoordinates(e.target.value)
              getJustAddress(e.target.value)
              actionAddAddress(e.target.value)
            }}
          >
            <Input
              className="inputWidthFull"
              label="Post code"
            />
          </Form.Item>

          <Form.Item name="mile_radius" label="Miles"
            rules={[{ required: true, message: 'Please input the miles' }]}
          >
            <Input
              name="mile_radius"
              className="inputWidthFull"
              label="Miles"
            />
          </Form.Item>

          <Form.Item name="category" label="Category"
            rules={[{ required: true, message: 'Please select a pproduct category' }]}
          >
            <Select
              placeholder="Choose a category"
            >
              {categories && categories.map((category, index) => (
                <Option key={index} value={category.id}>{category.name}</Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"

            // onClick={() =>{
            //   onReset
            //   setAddStore
            // }}
            >
              Add Store
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => {
                onReset
                setAddStore(false)
              }}
            >
              Reset
            </Button>
          </Form.Item >
        </Form>
      </Modal>


      {/* Change Partner Info Modal*/}
      <Modal
        title="Update partner Info"
        centered
        visible={partnerInfo}
        onCancel={() => setPartnerInfo(false)}
        footer={null}
      >

        <Form
          form={form}
          name="update_partner"

          onFinish={updatePartnerInfo}
          closable={true}
          layout="vertical"
        >
          <Form.Item
            name="name"
            label="Business name">
            <Input
              // defaultValue={storeData.name}
              placeholder={partnerData.business_name}
              className="inputWidthFull"
              label="Business name"
            />
          </Form.Item>
          <Form.Item
            name="phone"
            label="Phone">
            <InputNumber
              placeholder={partnerData.phone}
              className="inputWidthFull"
              label="Phone"

            />
          </Form.Item>

          <Form.Item
            name="description"
            label="Description">

            <TextArea
              rows={4}
              name="description"
              placeholder={partnerData.description}
            />

          </Form.Item>

          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"
            >
              Save
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => {
                setPartnerInfo(false)
                onReset()
              }}
            >
              Cancel
            </Button>
          </Form.Item >
        </Form>
      </Modal>


       {/* Edit Category */}
      <Modal
        title="Update logo"
        centered
        visible={logoUpdate}
        width={500}
        onCancel={() => {
          setLogoUpdate(false)
          onReset()
        }}

        onOk={() => {
          setIsEditing(false)
        }}
        footer={null}
      >
        <Form
          form={form}
          name="update_logo"
          onFinish={updateLogo}
          layout="vertical"
        >
          <Form.Item name="image" valuePropName="fileList"
            getValueFromEvent={addCategoryImage}>
            <Upload name="image"
              listType="picture"
            >
              <Button icon={<UploadOutlined />}>Click to upload</Button>
            </Upload>

          </Form.Item>
          <Form.Item>
            <Button
              loading={loading}
              htmlType="submit"
              className="mt-2"
              type="primary"
            >
              Upload logo
            </Button>
            <Button
              className="mt-2 mx-3"
              type="primary"
              onClick={() => {
                setLogoUpdate(false)
                onReset()
              }}
            >
              Cancel
            </Button>
          </Form.Item >
        </Form>
      </Modal>
    </>
  );
};

Partner.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default Partner;
