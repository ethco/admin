import { useState } from "react";
import DashboardLayout from "../../../layouts/DashboardLayout";
import Head from "../../../components/Head";
import Link from "next/link";
import { Form, Button, Input, notification, InputNumber } from "antd";
import PageHeader from "../../../components/PageHeader";
import { actionAddLocation } from "../../../helper/redux/actions/app";
import { useDispatch } from "react-redux";

const AddLocation = ({ id }) => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const submitForm = async (values) => {
    try {
      setLoading(true);
      const data = {
        payload: values,
        partner: id,
      };
      await dispatch(await actionAddLocation({ data }))
        .then((response) => {
          form.resetFields();
          notification.success({
            message: "Location",
            description: "Location added successfully",
          });
        })
        .catch((error) => {});
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      <Head title="Add Location" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Add Store Branch" page="location">
            <Link href={`/partners/${id}`}>
              <a>
                <Button type="border">View partner</Button>
              </a>
            </Link>
          </PageHeader>
          <div className="row">
            <div className="col-md-7">
              <div className="card card-bordered">
                <div className="card-body">
                  <p>Fill out all information</p>

                  <Form
                    form={form}
                    size="large"
                    name="add_partner"
                    onFinish={submitForm}
                    layout="vertical"
                  >
                    <div className="row">
                      <div className="col-md-6">
                        <Form.Item
                          name="name"
                          label="Branch name"
                          rules={[
                            {
                              required: true,
                              message: "Please input the branch name!",
                            },
                          ]}
                        >
                          <Input label="Branch name" />
                        </Form.Item>
                      </div>

                      <div className="col-md-6">
                        <Form.Item
                          name="town"
                          label="Town"
                          rules={[
                            {
                              required: true,
                              message: "Please input address town!",
                            },
                          ]}
                        >
                          <Input className="inputWidthFull" label="Town" />
                        </Form.Item>
                      </div>

                      <div className="col-md-6">
                        <Form.Item
                          name="postcode"
                          label="Postcode"
                          rules={[
                            {
                              required: true,
                              message: "Please input address postcode!",
                            },
                          ]}
                        >
                          <Input
                            className="inputWidthFull"
                            label="Postcode"
                          />
                        </Form.Item>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-3">
                        <Form.Item
                          name="mile_radius"
                          label="Mile radius"
                          rules={[
                            {
                              required: true,
                              message: "Please input mile radius!",
                            },
                          ]}
                        >
                          <InputNumber
                            className="inputWidthFull"
                            label="Mileradius"
                          />
                        </Form.Item>
                      </div>
                      <div className="col-md-9">
                        <Form.Item
                          name="street"
                          label="Street Address"
                          rules={[
                            {
                              required: true,
                              message: "Please input the street!",
                            },
                          ]}
                        >
                          <Input label="Street" />
                        </Form.Item>
                      </div>
                    </div>
                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      >
                        Add Location
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    </>
  );
};

AddLocation.getInitialProps = async (ctx) => {
  let { id } = ctx.query;
  return { id };
};

export default AddLocation;
