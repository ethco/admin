import { useState } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import PageLoading from "../../components/PageLoading";
import Link from "next/link";
import { useDispatch } from "react-redux";
import moment from "moment";
import {
  getAllPartners,
  actionUpdateStatus,
} from "../../helper/redux/actions/app";
import { useAsync } from "react-use";
import Router from "next/router";
import { notification, Input,  Avatar, Button, Modal, Badge,Table, Switch, Spin, Form } from "antd";

import { ExclamationCircleOutlined } from "@ant-design/icons";
import { useEffect } from "react";

export default function Home() {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const [loading, setLoading] = useState(true);
  const [partners, setPartners] = useState([]);
  const [pageLoading, setPageLoading] = useState(false);
  const [popReason, setPopReason] = useState(false);
  const [reason, setReason] = useState("");
  const [status, setStatus] = useState("");
  const [partnerID, setPartnerID] = useState("");
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);


  const getAllPartnersData = async () => {
    try {
      setLoading(true);
      await dispatch(await getAllPartners())
        .then((response) => {
          setPartners(response.data);
          
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  };


  // Same Function with getAllPartnersData()
  useAsync(async () => {
    try {
      setLoading(true);
      await dispatch(await getAllPartners())
        .then((response) => {
          setPartners(response.data);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  }, []);
  
/* Partner Status Change*/
  const statusChange = async (id, status, reason) => {
    try {
      setPageLoading(true);
      const payload = {
        id,
        data: {
          status: status ? "active" : "decline",
          reason: reason
        },
      };
      console.log(id, status)
      await dispatch(await actionUpdateStatus({ payload }))
        .then((response) => {
          setPageLoading(false);
          getAllPartnersData();
          notification.success({
            message: "Partner Status",
            description: "Partner status updated successfully",
          });
        })
        .catch((error) => {
          setPageLoading(false);
        });
    } finally {
      setLoading(false);
    }
  };

  const onReset = () => {
    form.resetFields();
  };

  const checkForm = (val) => {
    console.log(partnerID, status, reason)
    statusChange(partnerID, status, reason)
  }
  

    //  Partner Table
    const partnerColumns = [
      {
        key: 1,
        title: 'Date',
        dataIndex: 'created_at',
        render:  (created_at)=> (<span className="text-small">
          {moment(created_at).format(
                "ddd, MMM Do YYYY"
                        )}
                        </span>)
      },
      {
        key: 2,
        title: 'Business name',
        dataIndex: ['name', 'id'],
        render: (name, partner) => (
          <Link href={`/partners/${partner.id}`}>
            <a>
              {partner.business_name}
            </a>
          </Link>
        )
      },
      {
        key: 3,
        title: 'Status',
        dataIndex: ['status', 'id'],
        render:(status, partner) =>(
          <Switch
            style={{ marginLeft: "10px" }}
            checkedChildren="Active"
            unCheckedChildren="Inactive"
            checked={
              partner.status.status === "active" ? true : false
            }
            onChange={(value) => {
              setPopReason(true)
              setPartnerID(partner.id)
              setStatus(value)
            }
            }
          />
        )
      },
      {
        key: 4,
        title: 'Description',
        dataIndex: 'description',
      },
      {
        key: 5,
        title: 'Stores',
        dataIndex: 'stores',
        render: (stores) => (
          <p className="text-small">
            {stores && stores.length} Stores
          </p>
        )

      },
      {
        key: 6,
        title: 'Contact',
        dataIndex: ['email', 'id'],
        render: (email, partner) => (
          <>
            <p className="text-small mb-1">
              <a href={`mailto:${partner.email}`}>
                {partner.email}
              </a>
            </p>
            <p className="text-small">
              <a href={`tel:${partner.phone}`}>{partner.phone}</a>
            </p>
          </>
        )
      },
      {
        title: "Action",
        key: 7,
        dataSource:  ['email', 'id'],
        render: (text,record) => (
          <Button type="primary" onClick={() => Router.push(`/partners/${record.id}`)}>
            View Store          
          </Button>
        )
      }
    ];


  return (
    <>
      <Head title="Partners" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Partners" page="partner">
            <Link href="/partners/add-partner">
              <a>
                <Button type="primary">Add Partner</Button>
              </a>
            </Link>
          </PageHeader>
          {loading ? (
            <PageLoading />
          ) : (
            <Spin spinning={pageLoading}>
                <div className="row">
                 <div className="col-md-12">
              {/* <table className="table align-middle  mb-0">
                <thead className="table-light">
                  <tr>
                    <th className="align-middle">Date</th>
                    <th className="align-middle">Business name</th>
                    <th className="align-middle">Status</th>
                    <th className="align-middle">Description</th>
                    <th className="align-middle">Stores</th>
                    <th className="align-middle">Contact</th>
                    <th className="align-middle">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {partners.map((partner, index) => (
                    <tr key={index}>
                    
                      <td>
                        <span className="text-small">
                          {moment(partner.created_at).format(
                            "ddd, MMM Do YYYY"
                          )}
                        </span>
                      </td>
                      <td>
                        <p className="mb-1">
                          <Link href={`/partners/${partner.id}`}>
                            <a>
                              {partner.business_name}
                              </a>
                          </Link>
                        </p>
                      </td>
                      <td>
                      <Switch
                          style={{ marginLeft: "10px" }}
                          checkedChildren="Active"
                          unCheckedChildren="Inactive"
                          checked={
                            partner.status.status === "active" ? true : false
                          }
                          onChange={(value) => {
                            setPopReason(true) 
                            setPartnerID(partner.id)
                            setStatus(value)
                          }
                          }
                        />
                      </td>
                      <td>{partner.description}</td>
                      <td>
                        {" "}
                        <p className="text-small">
                          {partner.stores && partner.stores.length} Stores
                        </p>
                      </td>
                      <td>
                        <p className="text-small mb-1">
                          <a href={`mailto:${partner.email}`}>
                            {partner.email}
                          </a>
                        </p>
                        <p className="text-small">
                          <a href={`tel:${partner.phone}`}>{partner.phone}</a>
                        </p>
                      </td>
                      <td>
                        <Button
                          type="primary"
                          onClick={() => {
                            Router.push(`/partners/${partner.id}`);
                          }}
                        >
                          View Partner
                        </Button>
                       
                       
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table> */}
                     
                     <Table
            loading={loading}
            columns={partnerColumns}
            dataSource={partners}
            pagination={{
              current: page,
              pageSize: pageSize,
              total: partners.length,
              onChange: (page, pageSize)=>{
                setPage(page);
                setPageSize(pageSize);
              }
            }
            }
            /> 
              </div>
              </div>
            </Spin>

            
          )}


          {/* Enter Reason for status update */}
          <Modal
                  title="Change Delivery Address"
                  centered
                  visible={popReason}
                  onCancel={() => setPopReason(false)}
                  // onOk={()=> statusChange()}
                  width={500}
                  footer={null}
                >
                  <Form
               form={form}
               name="add_category"
               onFinish={checkForm}
               layout="vertical"
             >
              <Form.Item>
                <TextArea
            rows={4}
            name="reason"
            onChange={(text) => setReason(text.target.value)}
            placeholder="Include a reason to cancel this order"
            required={true}
            />
            </Form.Item>
            <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"

                        onClick={() => {
                          onReset()
                          setPopReason(false)
                        }}
                      >
                        Save
                      </Button>
                      <Button
                        className="mt-2 mx-3"
                        type="primary"
                        onClick={() => {
                          onReset()
                          setPopReason(false)
                        }}
                      >
                        Close
                      </Button>
                    </Form.Item >
          </Form>
                </Modal>
        </div>

        
      </DashboardLayout>
    </>
  );
}
