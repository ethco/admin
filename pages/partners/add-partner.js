import React, { useState } from "react";
import DashboardLayout from "../../layouts/DashboardLayout";
import Head from "../../components/Head";
import Link from "next/link";
import {
  Form,
  Button,
  Input,
  notification,
  Select,
  InputNumber,
  Spin,
  message,
} from "antd";
import PageHeader from "../../components/PageHeader";
import { actionAddPartner } from "../../helper/redux/actions/app";
import { useDispatch } from "react-redux";
/*import debounce from "lodash/debounce";*/

// function DebounceSelect({ fetchOptions, debounceTimeout = 800, ...props }) {
//   const [fetching, setFetching] = React.useState(false);
//   const [options, setOptions] = React.useState([]);
//   const fetchRef = React.useRef(0);
//   const debounceFetcher = React.useMemo(() => {
//     const loadOptions = (value) => {
//       fetchRef.current += 1;
//       const fetchId = fetchRef.current;
//       setOptions([]);
//       setFetching(true);
//       fetchOptions(value).then((newOptions) => {
//         if (fetchId !== fetchRef.current) {
//           // for fetch callback order
//           return;
//         }

//         setOptions(newOptions);
//         setFetching(false);
//       });
//     };

//     return debounce(loadOptions, debounceTimeout);
//   }, [fetchOptions, debounceTimeout]);
//   return (
//     <Select
//       labelInValue
//       filterOption={false}
//       onSearch={debounceFetcher}
//       notFoundContent={fetching ? <Spin size="small" /> : null}
//       {...props}
//       options={options}
//     />
//   );
// } // Usage of DebounceSelect

// async function fetchUserList(username) {
//   console.log("fetching user", username);
//   return await fetch("https://randomuser.me/api/?results=5")
//     .then((response) => response.json())
//     .then((body) =>
//       body.results.map((user) => ({
//         label: `${user.name.first} ${user.name.last}`,
//         value: user.login.username,
//       }))
//     );
// }

const AddPartner = (props) => {
  const [loading, setLoading] = useState(false);
  const [streetLoading, setStreetLoading] = useState(false);
  const [allLocations, setAllLocations] = useState([]);
  const [value, setValue] = useState([]);
  const [locationData, setLocationData] = useState({
    street: "",
    postcode: "",
    town_or_city: "",
  });
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const { Option } = Select;

  // function onChange(value) {
  //   fetch(
  //     `https://api.getAddress.io/get/${value}?api-key=WGdwrcuwoEyBbBfsSu5DyA34752`
  //   )
  //     .then((response) => response.json())
  //     .then((data) => {
  //       setStreetLoading(false);
  //       setLocationData(data);
  //     })
  //     .catch((err) => {
  //       setStreetLoading(false);
  //     });
  // }

  // const onSearch = async (val) => {
  //   console.log("search:", val);
  //   setStreetLoading(true);
  //   // fetch api for address validation
  //   if (val.length > 0) {
  //     fetch(
  //       `https://api.getAddress.io/autocomplete/${val}?api-key=WGdwrcuwoEyBbBfsSu5DyA34752`
  //     )
  //       .then((response) => response.json())
  //       .then((data) => {
  //         const { suggestions } = data || [];
  //         setStreetLoading(false);
  //         setAllLocations(suggestions);
  //       })
  //       .catch((err) => {
  //         setStreetLoading(false);
  //       });
  //     // await fetch(
  //     //   `https://api.getAddress.io/autocomplete/${val}?api-key=WGdwrcuwoEyBbBfsSu5DyA34752`
  //     // )
  //     //   .then((res) => {
  //     //     console.log("data:", res.json);
  //     //     setStreetLoading(false);
  //     //     // setAllLocations(data.result);
  //     //   })
  //     //   .catch((err) => {
  //     //     console.log(err);
  //     //     setStreetLoading(false);
  //     //   });
  //   }
  // };

  // Add Parters Function
  const submitForm = async (values) => {
    try {
      setLoading(true);
      const data = {
        ...values,
      };
      await dispatch(await actionAddPartner({ data }))
        .then((response) => {
          form.resetFields();
          notification.success({
            message: "Partner",
            description: "Partner added successfully",
          });
        })
        .catch((error) => {});
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      <Head title="Add Partner" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="Add Partner" page="partner">
            <Link href="/partners">
              <a>
                <Button type="border">View all</Button>
              </a>
            </Link>
          </PageHeader>
          <div className="row">
            <div className="col-md-7">
              <div className="card card-bordered">
                <div className="card-body">
                  <p>Fill out all information</p>

                  <Form
                    form={form}
                    size="large"
                    name="add_partner"
                    onFinish={submitForm}
                    layout="vertical"
                  >
                    <Form.Item
                      name="name"
                      label="Name"
                      rules={[
                        {
                          required: true,
                          message: "Please input the Contact name!",
                        },
                      ]}
                    >
                      <Input label="Store name" />
                    </Form.Item>

                    <Form.Item
                      name="email"
                      label="Contact person email"
                      rules={[
                        {
                          type: "email",
                          message: "The input is not valid E-mail!",
                        },
                        {
                          required: true,
                          message: "Please input partner email address!",
                        },
                      ]}
                    >
                      <Input label="Contact email" type="email" />
                    </Form.Item>

                    <Form.Item
                      name="business_name"
                      label="Business Name"
                      rules={[
                        {
                          required: true,
                          message: "Please input the Business name!",
                        },
                      ]}
                    >
                      <Input label="Business name" />
                    </Form.Item>

                    <Form.Item
                      name="description"
                      label="Business Description"
                      rules={[
                        {
                          required: true,
                          message: "Please input the Business Description!",
                        },
                      ]}
                    >
                      <Input.TextArea label="Business Description" />
                    </Form.Item>

                    <Form.Item>
                      <Button
                        loading={loading}
                        htmlType="submit"
                        className="mt-2"
                        type="primary"
                      >
                        Add Partner
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    </>
  );
};

export default AddPartner;
