import { useState, useEffect } from "react";
import Head from "../../components/Head";
import DashboardLayout from "../../layouts/DashboardLayout";
import PageHeader from "../../components/PageHeader";
import Link from "next/link";
import {
  ExclamationCircleOutlined,
  CheckCircleOutlined,
} from "@ant-design/icons";
import { Badge, Button, Modal, Tabs, Input } from "antd";
import {
  actionUpdateStatus,
  getAllPartners,
  getDeclinedPartners,
  getPendingPartners,
} from "../../helper/redux/actions/app";
import { useAsync } from "react-use";
import { useDispatch } from "react-redux";
import moment from "moment";

export default function Index() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [pendingPartners, setPendingPartners] = useState([]);
  const [activatedPartners, setActivatedPartners] = useState([]);
  const [declinedPartners, setDeclinedPartners] = useState([]);
  const [reason, setReason] = useState("");
  const [partnerID, setPartnerID] = useState("");
  const [submitDecline, setSubmitDecline] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);

  const { confirm, warning } = Modal;
  const { TabPane } = Tabs;
  const { TextArea } = Input;

  useEffect(() => {
    if (!!submitDecline) {
      if (!!reason) {
        statusChange(partnerID, "decline", reason);
      } else {
        requireReason();
      }
    }
  }, [submitDecline]);

  const onChangeData = (reason, partner_id) => {
    setReason(reason);
    setPartnerID(partner_id);
  };

  const confirmDecline = (partner_id) => {
    confirm({
      title: "Confirm Disapproval",
      icon: <ExclamationCircleOutlined />,
      content: (
        <div>
          <br />
          <TextArea
            rows={4}
            name="reason"
            onChange={(text) => onChangeData(text.target.value, partner_id)}
            placeholder="Include a reason to disapprove this partner"
            required={true}
          />
          <br />
        </div>
      ),
      async onOk() {
        setSubmitDecline(true);
      },
      onCancel() {},
    });
  };

  function requireReason() {
    setSubmitDecline(false);
    warning({
      title: "Reason is required",
      content: "Kindly include your reason for disapproval.",
    });
  }

  const confirmApprove = (partner_id) => {
    confirm({
      title: "Confirm Disapprove",
      icon: <CheckCircleOutlined />,
      content: "Are you sure to approve this partner",
      async onOk() {
        statusChange(partner_id, "active");
        getAllPartnersData()
      },
      onCancel() {},
    });
  };

  const confirmPending = (partner_id) => {
    confirm({
      title: "Confirm Status",
      icon: <CheckCircleOutlined />,
      content: "Are you sure to update this partner to pending",
      async onOk() {
        statusChange(partner_id, "pending");
      },
      onCancel() {},
    });
  };







  
  const getAllPartnersData = async () => {
    try {
      setLoading(true);
      await dispatch(await getAllPartners())
        .then((response) => {
          setActivatedPartners(response.data);
          console.log(activatedPartners)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
     
      await dispatch(await getPendingPartners())
        .then((response) => {
          setPendingPartners(response.data);
          console.log(pendingPartners)
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });

      await dispatch(await getDeclinedPartners())
        .then((response) => {
          setDeclinedPartners(response.data);
          console.log(declinedPartners);
        })
        .catch((error) => {
          notification.error({
            message: "Oops!",
            description: "Unable to send response, try again",
          });
        });
    } finally {
      setLoading(false);
    }
  };

  useAsync(async () => {
    await getAllPartnersData();
  }, []);

  const statusChange = async (id, status, reason = "") => {
    try {
      setPageLoading(true);
      const payload = {
        id,
        data: {
          status,
          reason,
        },
      };
      await dispatch(await actionUpdateStatus({ payload }))
      .then((response) => {
        setPageLoading(false);
        getAllPartnersData();
        notification.success({
          message: "Partner Status",
          description: "Partner status updated successfully",
        });
      })
      .catch((error) => {
        setPageLoading(false);
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <Head title="All Signup Requests" />
      <DashboardLayout>
        <div className="container-fluid">
          <PageHeader title="All Signup Requests" page="signup-requests">
            <Link href="/partners">
              <a>
                <Button type="primary">Add partners</Button>
              </a>
            </Link>
          </PageHeader>

          <div>
            <div className="row">
              <div className="col-md-4">
                <div className="card mini-stats-wid">
                  <div className="card-body">
                    <div className="d-flex">
                      <div className="flex-grow-1">
                        <p className="text-muted fw-medium">Pending Requests</p>
                        <h4 className="mb-0">{pendingPartners.length}</h4>
                      </div>

                      <div className="flex-shrink-0 align-self-center">
                        <div className="mini-stat-icon avatar-sm rounded-circle bg-primary">
                          <span className="avatar-title">
                            <i className="bx bx-copy-alt font-size-24"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className="card mini-stats-wid">
                  <div className="card-body">
                    <div className="d-flex">
                      <div className="flex-grow-1">
                        <p className="text-muted fw-medium">Approved</p>
                        <h4 className="mb-0">{activatedPartners.length}</h4>
                      </div>

                      <div className="flex-shrink-0 align-self-center">
                        <div className="avatar-sm rounded-circle bg-warning mini-stat-icon">
                          <span className="avatar-title rounded-circle bg-warning">
                            <i className="bx bx-user-check font-size-24"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className="card mini-stats-wid">
                  <div className="card-body">
                    <div className="d-flex">
                      <div className="flex-grow-1">
                        <p className="text-muted fw-medium">Disapproved</p>
                        <h4 className="mb-0">{declinedPartners.length}</h4>
                      </div>

                      <div className="flex-shrink-0 align-self-center">
                        <div className="avatar-sm rounded-circle bg-danger mini-stat-icon">
                          <span className="avatar-title rounded-circle bg-danger">
                            <i className="bx bx-purchase-tag-alt font-size-24"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="card">
            <div className="card-body">
              <Tabs defaultActiveKey="pending">
                <TabPane tab="Pending Requests" key="pending">
                  <div className="table-responsive">
                    <table className="table align-middle table-nowrap mb-0">
                      <thead className="table-light">
                        <tr>
                          <th className="align-middle">Date</th>
                          <th className="align-middle">Store name</th>
                          {/* <th className="align-middle">Store Category</th> */}
                          {/* <th className="align-middle">Product type</th> */}
                          <th className="align-middle">Contact Name</th>
                          <th className="align-middle">Contact Email</th>
                          <th className="align-middle">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {pendingPartners.map((partner, index) => (
                          <tr key={index}>
                            <td>
                              <span className="text-small">
                                {moment(partner.created_at).format(
                                  "ddd, MMM Do YYYY"
                                )}
                              </span>
                            </td>
                            <td>
                              <p className="mb-1">
                                <Link href={`/signup-requests/${partner.id}`}>
                                  <a>{partner.business_name}</a>
                                </Link>
                              </p>
                              <p className="text-small">
                                {partner.stores.length} Branches
                              </p>
                            </td>
                            {/* <td>{partner.category}</td> */}
                            {/* <td>Groceries</td> */}
                            <td>{partner.manager}</td>
                            <td>
                              <p className="text-small mb-1">
                                <a href={`mailto:${partner.email}`}>
                                  {partner.email}
                                </a>
                              </p>
                              <p className="text-small">
                                <a href="tel:+44922839283">
                                  {partner?.phone ?? "None Yet"}
                                </a>
                              </p>
                            </td>
                            <td>
                              <Button
                                type="primary"
                                onClick={() => confirmApprove(partner.id)}
                              >
                                Approve
                              </Button>
                              <Button
                                style={{ marginLeft: "10px" }}
                                onClick={() => confirmDecline(partner.id)}
                                danger
                              >
                                Disapprove
                              </Button>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </TabPane>
                <TabPane tab="Disapproved Requests" key="disapproved">
                  <div className="table-responsive">
                    <table className="table align-middle table-nowrap mb-0">
                      <thead className="table-light">
                        <tr>
                          <th className="align-middle">Date</th>
                          <th className="align-middle">Store name</th>
                          {/* <th className="align-middle">Store Category</th> */}
                          {/* <th className="align-middle">Product type</th> */}
                          <th className="align-middle">Contact Name</th>
                          <th className="align-middle">Contact Email</th>
                          <th className="align-middle">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {declinedPartners.map((partner, index) => (
                          <tr key={index}>
                            <td>
                              <span className="text-small">
                                {moment(partner.created_at).format(
                                  "ddd, MMM Do YYYY"
                                )}
                              </span>
                            </td>
                            <td>
                              <p className="mb-1">
                                <Link href={`/signup-requests/${partner.id}`}>
                                  <a>{partner.business_name}</a>
                                </Link>
                              </p>
                              <p className="text-small">
                                {partner.stores.length} Branches
                              </p>
                            </td>
                            {/* <td>{partner.category}</td> */}
                            {/* <td>Groceries</td> */}
                            <td>{partner.manager}</td>
                            <td>
                              <p className="text-small mb-1">
                                <a href={`mailto:${partner.email}`}>
                                  {partner.email}
                                </a>
                              </p>
                              <p className="text-small">
                                <a href="tel:+44922839283">
                                  {partner?.phone ?? "None Yet"}
                                </a>
                              </p>
                            </td>
                            <td>
                              <Button
                                type="primary"
                                onClick={() => confirmApprove(partner.id)}
                              >
                                Approve
                              </Button>
                              <Button
                                style={{ marginLeft: "10px" }}
                                onClick={() => confirmPending(partner.id)}
                                danger
                              >
                                Pend
                              </Button>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </TabPane>
              </Tabs>
            </div>
          </div>
        </div>
      </DashboardLayout>
    </>
  );
}
