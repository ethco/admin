import { useEffect, useState } from "react";
import Script from "next/script";
import VendorScript from "../components/VendorScript";
import wrapperStore from "../helper/redux";
// import DashboardLayout from "../layouts/DashboardLayout"
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import cookie from "react-cookies";
import { useAsync } from "react-use";
import AuthStorage from "../helper/utils/auth-storage";
import Loading from "../components/Loading";
import "antd/dist/antd.css";
require("../styles/variables.less");
import "../styles/scss/app.scss";
import "../styles/css/icons.min.css";
import "../styles/scss/bootstrap.scss";
import "../styles/scss/custom.scss";
import "../styles/scss/icons.scss";

const urlsIgnore = [
  "/auth/forgot-password",
  "/auth/reset-password",
  "/auth/login",
  "/auth/set-new-password.js",
  "/auth/signup",
  "/auth/verify-email",
];

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const [awaitLoading, setAwaitLoading] = useState(true);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    isRouteChanging: false,
    loadingKey: 0,
  });

  useEffect(() => {
    const handleRouteChangeStart = () => {
      setState((prevState) => ({
        ...prevState,
        isRouteChanging: true,
        loadingKey: prevState.loadingKey ^ 1,
      }));
    };

    const handleRouteChangeEnd = () => {
      setState((prevState) => ({
        ...prevState,
        isRouteChanging: false,
      }));
    };

    router.events.on("routeChangeStart", handleRouteChangeStart);
    router.events.on("routeChangeComplete", handleRouteChangeEnd);
    router.events.on("routeChangeError", handleRouteChangeEnd);
    return () => {
      router.events.off("routeChangeStart", handleRouteChangeStart);
      router.events.off("routeChangeComplete", handleRouteChangeEnd);
      router.events.off("routeChangeError", handleRouteChangeEnd);
    };
  }, [router.events]);

  useAsync(async () => {
    if (
      !AuthStorage.loggedIn &&
      typeof window !== "undefined" &&
      !urlsIgnore.includes(router.pathname)
    ) {
      router.push("/auth/login");
    }
  }, [router.pathname]);

  return (
    <>
      <Loading isRouteChanging={state.isRouteChanging} key={state.loadingKey} />
      {/* <DashboardLayout> */}
      <Component {...pageProps} />
      {/* </DashboardLayout> */}
      {/* <VendorScript />
      <Script type="text/javascript" src="/js/app.js" /> */}
    </>
  );
}

export default wrapperStore.withRedux(MyApp);
