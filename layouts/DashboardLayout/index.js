import { useState, useCallback } from "react";
import Link from "next/link";
import { StylesWrapper } from "./styles";
import Modals from "../../components/Modals";
import AuthStorage from "../../helper/utils/auth-storage";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Modal, Dropdown, Menu, Avatar, AutoComplete } from "antd";
import Router from "next/router";


const AuthLayout = ({ children }) => {

  const [value, setValue] = useState('');
  const [options, setOptions] = useState([]);

  const onSearch = (searchText) => {
    setOptions(
      !searchText ? [] : [mockVal(searchText), mockVal(searchText, 2), mockVal(searchText, 3)],
    );
  };
  const onSelect = (data) => {
    console.log('onSelect', data);
  };
  const onChange = (data) => {
    setValue(data);
  };






  const [user, setUser] = useState(AuthStorage.user || {});
  const handleLogout = useCallback(async () => {
    Modal.confirm({
      title: "Are you sure?",
      icon: <ExclamationCircleOutlined />,
      onOk: async () => {
        sessionStorage.clear();
        AuthStorage.destroy(() => {
          Router.push("/auth/login");
        });
      },
      onCancel() {},
    });
  }, []);

  return (
    <StylesWrapper>
      <div id="layout-wrapper">
        <header id="page-topbar">
          <div className="navbar-header">
            <div className="d-flex">
              <div className="navbar-brand-box">
                <a href="index.html" className="logo logo-dark">
                  <span className="logo-sm">
                    <img src="/images/logo.svg" alt="" height="22" />
                  </span>
                  <span className="logo-lg">
                    <img src="/images/logo-dark.png" alt="" height="17" />
                  </span>
                </a>

                <a
                  href="index.html"
                  className="logo logo-light"
                  style={{ textAlign: "left" }}
                >
                  <span className="logo-sm">
                    <img src="/images/logo-light.svg" alt="" height="22" />
                  </span>
                  <span className="logo-lg">
                    <img src="/images/logo-light.png" alt="" height="30" />
                  </span>
                </a>
              </div>

              <form className="app-search  px-3 d-none d-lg-block">
                <div className="position-relative">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search..."
                  />
                  <span className="bx bx-search-alt"></span>
                  
                </div>
              </form>

            </div>

            <div className="d-flex">
              <div className="dropdown d-inline-block d-lg-none ms-2">
                <button
                  type="button"
                  className="btn header-item noti-icon waves-effect"
                  id="page-header-search-dropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i className="mdi mdi-magnify"></i>
                </button>
                <div
                  className="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                  aria-labelledby="page-header-search-dropdown"
                >
                  <form className="p-3">
                    <div className="form-group m-0">
                      <div className="input-group">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Search ..."
                          aria-label="Recipient's username"
                        />
                        <div className="input-group-append">
                          <button className="btn btn-primary" type="submit">
                            <i className="mdi mdi-magnify"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

              <div className="dropdown d-inline-block">
                <button
                  type="button"
                  className="btn header-item noti-icon waves-effect"
                  id="page-header-notifications-dropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i className="bx bx-bell bx-tada"></i>
                  <span className="badge bg-danger rounded-pill">1</span>
                </button>
                <div
                  className="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                  aria-labelledby="page-header-notifications-dropdown"
                >
                  <div className="p-3">
                    <div className="row align-items-center">
                      <div className="col">
                        <h6 className="m-0" key="t-notifications">
                          Notifications
                        </h6>
                      </div>
                      <div className="col-auto">
                        <a href="#!" className="small" key="t-view-all">
                          {" "}
                          View All
                        </a>
                      </div>
                    </div>
                  </div>
                  <div data-simplebar style={{ maxHeight: "230px" }}>
                    <a
                      href="javascript: void(0);"
                      className="text-reset notification-item"
                    >
                      <div className="d-flex">
                        <div className="avatar-xs me-3">
                          <span className="avatar-title bg-primary rounded-circle font-size-16">
                            <i className="bx bx-cart"></i>
                          </span>
                        </div>
                        <div className="flex-grow-1">
                          <h6 className="mb-1" key="t-your-order">
                            Your order is placed
                          </h6>
                          <div className="font-size-12 text-muted">
                            <p className="mb-1" key="t-grammer">
                              If several languages coalesce the grammar
                            </p>
                            <p className="mb-0">
                              <i className="mdi mdi-clock-outline"></i>
                              <span key="t-min-ago">3 min ago</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>
                    <a
                      href="javascript: void(0);"
                      className="text-reset notification-item"
                    >
                      <div className="d-flex">
                        <img
                          src="/images/users/avatar-3.jpg"
                          className="me-3 rounded-circle avatar-xs"
                          alt="user-pic"
                        />
                        <div className="flex-grow-1">
                          <h6 className="mb-1">James Lemire</h6>
                          <div className="font-size-12 text-muted">
                            <p className="mb-1" key="t-simplified">
                              It will seem like simplified English.
                            </p>
                            <p className="mb-0">
                              <i className="mdi mdi-clock-outline"></i>
                              <span key="t-hours-ago">1 hours ago</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>
                    <a
                      href="javascript: void(0);"
                      className="text-reset notification-item"
                    >
                      <div className="d-flex">
                        <div className="avatar-xs me-3">
                          <span className="avatar-title bg-success rounded-circle font-size-16">
                            <i className="bx bx-badge-check"></i>
                          </span>
                        </div>
                        <div className="flex-grow-1">
                          <h6 className="mb-1" key="t-shipped">
                            Your item is shipped
                          </h6>
                          <div className="font-size-12 text-muted">
                            <p className="mb-1" key="t-grammer">
                              If several languages coalesce the grammar
                            </p>
                            <p className="mb-0">
                              <i className="mdi mdi-clock-outline"></i>
                              <span key="t-min-ago">3 min ago</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>

                    <a
                      href="javascript: void(0);"
                      className="text-reset notification-item"
                    >
                      <div className="d-flex">
                        <img
                          src="/images/users/avatar-4.jpg"
                          className="me-3 rounded-circle avatar-xs"
                          alt="user-pic"
                        />
                        <div className="flex-grow-1">
                          <h6 className="mb-1">Salena Layfield</h6>
                          <div className="font-size-12 text-muted">
                            <p className="mb-1" key="t-occidental">
                              As a skeptical Cambridge friend of mine
                              occidental.
                            </p>
                            <p className="mb-0">
                              <i className="mdi mdi-clock-outline"></i>
                              <span key="t-hours-ago">1 hours ago</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                  <div className="p-2 border-top d-grid">
                    <a
                      className="btn btn-sm btn-link font-size-14 text-center"
                      href="javascript:void(0)"
                    >
                      <i className="mdi mdi-arrow-right-circle me-1"></i>
                      <span key="t-view-more">View More..</span>
                    </a>
                  </div>
                </div>
              </div>
              <Dropdown
                overlay={
                  <Menu className="profile-dropdown">
                    <Menu.Item
                      onClick={() => {
                        Router.push("/profile");
                      }}
                    >
                      <span className="px-4 py-3">View Profile</span>
                    </Menu.Item>
                    <Menu.Item onClick={() => Router.push("/more")}>
                      <span className="px-4 py-3">Settings</span>
                    </Menu.Item>
                    <Menu.Item onClick={handleLogout}>
                      <span className="px-4 py-3">Logout</span>
                    </Menu.Item>
                  </Menu>
                }
                placement="bottomRight"
                arrow
              >
                {user && (
                  <div
                    className="header-item cursor-pointer"
                    style={{ display: "flex", alignItems: "center" }}
                  >
                    <img
                      className="rounded-circle header-profile-user"
                      src={
                        user.picture ||
                        `https://ui-avatars.com/api/?name=${user.name}&background=3c29b3&color=fff&size=128`
                      }
                      alt="Header Avatar"
                    />
                    <span
                      className="d-none d-xl-inline-block ms-1"
                      key="t-henry"
                    >
                      {user && user.name}
                    </span>
                    <i className="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                  </div>
                )}
              </Dropdown>
            </div>
          </div>
        </header>

        <div className="vertical-menu">
          <div data-simplebar className="h-100">
            <div id="sidebar-menu">
              <ul className="metismenu list-unstyled" id="side-menu">
                <li>
                  <Link href="/">
                    <a className="waves-effect">
                      <i className="bx bx-home-circle"></i>
                      <span key="t-starter-page">Dashboard</span>
                    </a>
                  </Link>
                </li>

                <li>
                  <Link href="/orders">
                    <a className="waves-effect">
                      <i className="bx bx-cart"></i>
                      <span key="t-horizontal">Orders</span>
                    </a>
                  </Link>
                </li>
{/* 
                <li>
                  <Link href="/analytics">
                    <a className="waves-effect">
                      <i className="bx bx-bar-chart-alt"></i>
                      <span key="t-horizontal">Analytics</span>
                    </a>
                  </Link>
                </li> */}

                <li>
                  <Link href="/partners">
                    <a className="waves-effect">
                      <i className="bx bx-user-pin"></i>
                      <span key="t-horizontal">Partners</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/stores">
                    <a className="waves-effect">
                      <i className="bx bx-store-alt"></i>
                      <span key="t-horizontal">Stores</span>
                    </a>
                  </Link>
                </li>

                {/* <li>
                  <Link href="/coupon">
                    <a className="waves-effect">
                      <i className="bx bxs-coupon"></i>
                      <span key="t-horizontal">Coupon</span>
                    </a>
                  </Link>
                </li> */}
                
                <li>
                  <Link href="/category">
                    <a className="waves-effect">
                      <i className="bx bx-user-pin"></i>
                      <span key="t-horizontal">Category</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/plan">
                    <a className="waves-effect">
                      <i className="bx bx-bar-chart-alt"></i>
                      <span key="t-horizontal">Plans</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/coupon">
                    <a className="waves-effect">
                      <i className="bx  bxs-coupon"></i>
                      <span key="t-horizontal">Coupon</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/customers">
                    <a className="waves-effect">
                      <i className="bx bx-user-pin"></i>
                      <span key="t-horizontal">Customers</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/driver">
                    <a className="waves-effect">
                      <i className="bx bx-user-pin"></i>
                      <span key="t-horizontal">Drivers</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/managers">
                    <a className="waves-effect">
                      <i className="bx bx-user-plus"></i>
                      <span key="t-horizontal">Managers</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/signup-requests">
                    <a className="waves-effect">
                      <i className="bx bx-list-plus"></i>
                      <span key="t-horizontal">Store Signup Requests</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/users">
                    <a className="waves-effect">
                      <i className="bx bx-user-pin"></i>
                      <span key="t-horizontal">Users</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/profile">
                    <a className="waves-effect">
                      <i className="bx bx-user"></i>
                      <span key="t-horizontal">Profile</span>
                    </a>
                  </Link>
                </li>

                
              </ul>
            </div>
          </div>
        </div>

        <div className="main-content">
          <div className="page-content">{children}</div>

          <Modals
          // data={drawer.drawerState.data}
          // type={drawer.drawerState.type}
          // visible={drawer.drawerState.visible}
          />

          <footer className="footer">
            <div className="container-fluid">
              <div className="row">
                <div className="col-sm-6">
                  {new Date().getFullYear()} © Ethco.
                </div>
                <div className="col-sm-6">
                  <div className="text-sm-end d-none d-sm-block">
                    Design & Develop by Ethco Team
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </StylesWrapper>
  );
};

export default AuthLayout;
