import React from "react";
import { StylesWrapper } from "./styles";

const AuthLayout = ({ children }) => {
  return (
    <StylesWrapper>
      <div>{children}</div>
    </StylesWrapper>
  );
};

export default AuthLayout;
