import Link from "next/link";
import { Button } from "antd";
import Router from "next/router";
export default function index({ title, page, children, pageLink, page1, page2 }) {
  return (
    <div className="row">
      <div className="col-12">
        <div className="page-title-box d-sm-flex align-items-center justify-content-between">
          <div>
            <h4 className="mb-sm-0 font-size-18">{title}</h4>
            <ol className="breadcrumb m-0">
              <li className="breadcrumb-item">
                <Link href="/">
                  <a>Dashboard</a>
                </Link>
              </li>
              {/* {(() => {
                if (page2) {
                  return (
                    <>
                    <li className="breadcrumb-item pe-auto" onClick={() => Router.push(`/${pageLink}`)}><a>{page}</a></li>
              <li className="breadcrumb-item active" success>{page2}</li>
                    <li className="breadcrumb-item pe-auto" onClick={() => Router.back()}><a>Go back</a></li>
              </> )
            } else if (page1) {
              return (
                <>
                <li className="breadcrumb-item active">{page1}</li>
                <li className="breadcrumb-item pe-auto" onClick={() => Router.back()}><a>Go back</a></li>
          </> 
              )
            } else if (page) {
              return (
                <> */}
                <li className="breadcrumb-item pe-auto" onClick={() => Router.back()}><a>Go back</a></li>
                {/* <li className="breadcrumb-item active">{page}</li> */}
{/*                 
          </> 
              )
            }

          })()} */}
            </ol>
          </div>

          <div className="page-title-right">{children}</div>
        </div>
      </div>
    </div>
  );
}
