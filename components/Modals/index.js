import React, { useContext, useState } from "react";
import { Modal } from "antd";
import AddPartnerModal from "./AddPartnerModal";

// import { GlobalContext } from "../../helper/context/Provider";
// import { UPDATE_DRAWER } from "../../helper/context/actionTypes";

const HaloDrawer = ({ visible, type, data, placement }) => {
  //   const { dispatch } = useContext(GlobalContext);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const itemViewer = () => {
    switch (type) {
      case "add_partner":
        return <AddPartnerModal />;
    }
  };

  //   const showModal = ({ isModalVisible }) => {
  //     setIsModalVisible(true);
  //   };

  //   const handleOk = () => {
  //     setIsModalVisible(false);
  //   };

  //   const handleCancel = () => {
  //     setIsModalVisible(false);
  //   };

  return (
    <>
      <Modal
        title="Basic Modal"
        visible={isModalVisible}
        //   onOk={handleOk}
        //   onCancel={handleCancel}
      >
        <h1>Hello World</h1>
        {itemViewer()}
      </Modal>
      {/* <Drawer
        title=""
        className="halo-drawer"
        placement={placement || "right"}
        onClose={() => {
          sessionStorage.clear();
          dispatch({
            type: UPDATE_DRAWER,
            payload: {
              visible: false,
              type: "",
            },
          });
        }}
        width={480}
        closable={false}
        visible={visible}
      >
        {itemViewer()}
      </Drawer> */}
    </>
  );
};

export default HaloDrawer;
