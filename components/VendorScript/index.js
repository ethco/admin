import Script from "next/script";

export default function index() {
  return (
    <>
      <Script type="text/javascript" src="/libs/jquery/jquery.min.js" />
      <Script
        type="text/javascript"
        src="/libs/bootstrap/js/bootstrap.bundle.min.js"
      />
      {/* <Script type="text/javascript" src="/libs/metismenu/metisMenu.min.js" /> */}
      <Script type="text/javascript" src="/libs/simplebar/simplebar.min.js" />
      <Script type="text/javascript" src="/libs/node-waves/waves.min.js" />
    </>
  );
}
